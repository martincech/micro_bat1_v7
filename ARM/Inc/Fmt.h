//*****************************************************************************
//
//    Fmt.h        Format specifications
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __Fmt_H__
   #define __Fmt_H__

#define FMT_MINUS          0x80000000       // zaporne znamenko pro BCD float

// priznaky vlozene do parametru <width> :
#define FMT_NEGATIVE       0x8000           // zaporne znamenko
#define FMT_UNSIGNED       0x4000           // dekadicke cislo bez znamenka
#define FMT_LEADING_0      0x2000           // priznak vedoucich 0
#define FMT_PLUS           0x1000           // povinne + u kladnych cisel
#define FMT_WIDTH          0x000F           // maska - pocet znaku cisla
#define FMT_DECIMALS       0x00F0           // maska - pocet cislic za carkou
#define FMT_DECIMALS_SHIFT 4                // posun cislic za carkou


// standardni formatovani printf <Total> je celkovy pocet cislic + 2 (znamenko a tecka),
// <Decimal> je pocet mist za teckou

#define FmtPrecision( Total, Decimal) (((Total) & FMT_WIDTH)  | \
                                      (((Decimal) << FMT_DECIMALS_SHIFT) & FMT_DECIMALS))

#define FmtSetWidth(    Width)    ((Width) & FMT_WIDTH)
#define FmtSetDecimals( Decimal)  (((Decimal) << FMT_DECIMALS_SHIFT) & FMT_DECIMALS)

#define FmtGetWidth( w)           ((w) & FMT_WIDTH)
#define FmtGetDecimals( w)        (((w) & FMT_DECIMALS) >> FMT_DECIMALS_SHIFT)

#endif
