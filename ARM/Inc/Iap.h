//*****************************************************************************
//
//    Iap.h        In application programming services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Iap_H__
   #define __Iap_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define IAP_SECTOR_SIZE 8192       // Flash sector size

//------------------------------------------------------------------------------
//   Functions
//------------------------------------------------------------------------------

void IapSetup( void);
// Prepare CPU for IAP

void IapCleanup( void);
// Cleanup CPU after IAP

TYesNo IapErase( void *From, void *To);
// Erase flash <From> address <To> (inclusive) address
// If <To> == 0, erases sector containing <From> address

TYesNo IapWrite( void *Destination, void *Source, native Size);
// Write <Source> data into Flash address <Destination> in <Size> length
// Warning : <Destination> align and <Size> depend on CPU model

TYesNo IapCompare( void *Destination, void *Source, native Size);
// Compare memory area <Destination>/<Source> in <Size> length

#endif
