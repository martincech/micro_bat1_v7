//*****************************************************************************
//
//    xprint.h - formatting basics template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __xprint_H__
   #define __xprint_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __Fmt_H__
   #include "Fmt.h"
#endif

#ifndef __PutcharDef_H__
   #include "PutcharDef.h"
#endif

#include <stdarg.h>

//-----------------------------------------------------------------------------

void xputs( TPutchar *xputchar, const char *string);
// vystup retezce <string>

void xputsn(  TPutchar *xputchar, const char *string);
// vystup retezce <string> az po LF

void xprinthex( TPutchar *xputchar, dword x, dword flags);
// vystup hexa cisla <x>, <flags> koduje sirku

void xprintdec( TPutchar *xputchar, int32 x, dword flags);
// vystup dekadickeho cisla <x>, <flags> koduje sirku

void xfloat( TPutchar *xputchar, dword x, int w, int d, dword flags);
// Tiskne binarni cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. <flags> jsou doplnujici atributy
// (FMT_PLUS)

void xprintf( TPutchar *xputchar, const char *Format, ...);
// jednoduchy formatovany vystup

void xvprintf( TPutchar *xputchar, const char *Format, va_list Arg);
// jednoduchy formatovany vystup

#endif
