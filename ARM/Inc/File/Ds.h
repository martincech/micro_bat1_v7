//******************************************************************************
//
//   Ds.h          Data Set utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Ds_H__
   #define __Ds_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Db_H__
   #include "Db.h"
#endif

#ifndef __DsDef_H__
   #include "DsDef.h"                  // data set definition
#endif

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Dataset utility
//------------------------------------------------------------------------------

void DsClear( TFDataSet DataSet);
// Clear dataset

void DsSelectAll( TFDataSet DataSet);
// Select all files to dataset

void DsCopy( TFDataSet Dst, TFDataSet Src);
// Copy dataset

void DsAdd( TFDataSet DataSet, TFdirHandle Handle);
// Add file to dataset

void DsRemove( TFDataSet DataSet, TFdirHandle Handle);
// Remove file from dataset

TYesNo DsContains( TFDataSet DataSet, TFdirHandle Handle);
// Returns YES if <DataSet> contains <Handle>

TYesNo DsIsEmpty( TFDataSet DataSet);
// Empty dataset

//------------------------------------------------------------------------------
// Working data set
//------------------------------------------------------------------------------

void DsSetCurrent( void);
// Set dataset to currently opened file

void DsSet( TFDataSet DataSet);
// Set <DataSet> as current

//------------------------------------------------------------------------------
// Location in working data set
//------------------------------------------------------------------------------

void DsBegin( void);
// Set before start of the first table

TYesNo DsNext( void *Record);
// Get next record

#ifdef __cplusplus
}
#endif

#endif

