//******************************************************************************
//                                                                            
//  DTime.h        Display time
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DTime_H__
   #define __DTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DtDef_H__
   #include "../DtDef.h"
#endif

void DTime( TTimestamp Now);
// Display time only

void DTimeShort( TTimestamp Now);
// Display time without seconds

void DDate( TTimestamp Now);
// Display date only

void DDateTime( TTimestamp Now);
// Display date and time

void DDateTimeShort( TTimestamp Now);
// Display date and time without seconds

#endif
