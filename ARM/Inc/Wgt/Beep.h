//******************************************************************************
//                                                                            
//  Beep.h         Sound processing
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Beep_H__
   #define __Beep_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void BeepKey( void);
// Key press beep

void BeepError( void);
// Error key press beep

void BeepTimeout( void);
// User inactivity beep

#endif
