//******************************************************************************
//                                                                            
//  DHistogram.h   Display histogram
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DHistogram_H__
   #define __DHistogram_H__

#ifndef __Histogram_H__
   #include "../Histogram.h"
#endif

TYesNo DHistogram( THistogram *Histogram,  int LowLimit, int HighLimit);
// Display histogram. Returns YES on Enter

void DHistogramIcon( THistogram *Histogram, int x, int y);
// Display histogram icon at <x,y>

#endif
