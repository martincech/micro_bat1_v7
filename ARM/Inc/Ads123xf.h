//*****************************************************************************
//
//    Ads123xf.h  -  A/D convertor ADS1230/ADS1232 with filter services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads123xf_H__
   #define __Ads123xf_H__

#ifndef __Filter_H__
   #include "Filter.h"
#endif

#define ADC_SAMPLES_PER_S     10       // samples per second
#define ADC_SAMPLING_PERIOD   100      // samples per second in [ms]

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

void AdcStart( void);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

void AdcRestart( void);
// Restart filtrace

TRawWeight AdcRawRead( void);
// Cteni okamzite hodnoty prevodu

TRawWeight AdcLowPassRead( void);
// Cteni filtrovane hodnoty prevodu

TYesNo AdcRead( TRawWeight *Weight);
// Cteni ustalene hodnoty

//-----------------------------------------------------------------------------

#endif
