//******************************************************************************
//
//   Dt.c         Data/Time utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../inc/Dt.h"

// helper macros :
#define MonthDay( m)      _MDay[ (m) - 1]        // days of month
#define YearDay( m)       _YDay[ (m) - 1]        // days from begin of year
#define IsLeapYear( y)    (!((y) & 0x03))        // leap year

// constants :
#define DOW_1_1_2000      DT_SATURDAY            // dow for 1.1.2000
#define FOUR_YEAR_DAYS    1461                   // days of four years

// date internal representation :
typedef word TDtDate;                            // days from 1.1.2000

// local data :

// month days :
static const byte _MDay[12] = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

// month days from start of year :
static const word _YDay[12] = {
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};

#ifdef DT_ENABLE_DST

typedef struct {
   byte Month;
   byte Week;
   byte Dow;
   byte Hour;
   byte Min;
} TTransitionDay;

typedef struct {
   TTransitionDay Start;
   TTransitionDay End;
} TDstTransition;

static const TDstTransition DstTransition[ _DT_DST_TYPE_COUNT - 1] = {
// EU
{
   { DT_MARCH,   5, DT_SUNDAY, 2, 0},
   { DT_OCTOBER, 5, DT_SUNDAY, 2, 0}
},
// US
{
   { DT_MARCH,    2, DT_SUNDAY, 2, 0},
   { DT_NOVEMBER, 1, DT_SUNDAY, 2, 0}
}
};

#endif

// Local functions :

static TDtDate EncodeDate( byte Year, byte Month, byte Day);
// Encode <Local> to internal date representation

static void DecodeDate( TDtDate Date, byte *Year, byte *Month, byte *Day);
// Decode internal <Date> to split representation

static TTimestamp ComposeDateTime( TDtDate Date, TLocalTime *Time);
// Compose  <Date> with <Time>

static TDtDate DecomposeTime( TTimestamp Timestamp, TLocalTime *Time);
// Decode time from <Timestamp>, returns date

static int GetDow( TDtDate Date);
// Returns dow code for <Date>

static int GetMonthDays( byte Year, byte Month);
// Returns month days

#ifdef DT_ENABLE_DST

static TYesNo EnableDst( void);
// Returns Yes if DST is enabled

static TYesNo IsDst( TLocalTime *Local);
// Returns YES for summer time. Input values are winter time

static byte GetTransitionDay( byte Year, const TTransitionDay *Transition);
// Returns transition day of month

#endif // DT_ENABLE_DST

//------------------------------------------------------------------------------
// Compose
//------------------------------------------------------------------------------

TTimestamp DtCompose( TLocalTime *Local)
// Compose date/time <Local> to internal representation
{
TDtDate Date;

   Date = EncodeDate( Local->Year, Local->Month, Local->Day);
   return( ComposeDateTime( Date, Local));
} // DtCompose

//------------------------------------------------------------------------------
// Split
//------------------------------------------------------------------------------

void DtSplit( TTimestamp Timestamp, TLocalTime *Local)
// Split date/time from <Timestamp> to <Local>
{
TDtDate Date;

   Date = DecomposeTime( Timestamp, Local);
   DecodeDate( Date, &Local->Year, &Local->Month, &Local->Day);
} // StSplit

//------------------------------------------------------------------------------
// Encode
//------------------------------------------------------------------------------

TTimestamp DtEncode( TLocalTime *Local)
// Encode date/time <Local> to internal representation
{
TDtDate Date;

   Date = EncodeDate( Local->Year, Local->Month, Local->Day);
#ifdef DT_ENABLE_DST
   if( EnableDst() && IsDst( Local)){
      // is summertime
      if( Local->Hour != 0){
         Local->Hour--;
      } else {
         // 00h - one day back
         Local->Hour = 23;
         Date--;
      }
   }
#endif // DT_ENABLE_DST
   return( ComposeDateTime( Date, Local));
} // DtEncode

//------------------------------------------------------------------------------
// Decode
//------------------------------------------------------------------------------

void DtDecode( TTimestamp Timestamp, TLocalTime *Local)
// Decode date/time from <Timestamp> to <Local>
{
TDtDate Date;

   Date = DecomposeTime( Timestamp, Local);
   DecodeDate( Date, &Local->Year, &Local->Month, &Local->Day);
#ifdef DT_ENABLE_DST
   if( EnableDst() && IsDst( Local)){
      // is summertime, add one hour
      Local->Hour++;
      if( Local->Hour == 24){
         // next day, compute date again
         Local->Hour = 0;
         DecodeDate( Date + 1,  &Local->Year, &Local->Month, &Local->Day);
      }
   }
#endif // DT_ENABLE_DST
} // DtDecode

//------------------------------------------------------------------------------
// Day of week
//------------------------------------------------------------------------------

int DtDow( TTimestamp Timestamp)
// Returns day of week
{
   Timestamp /= DT_DAY;                // remove time
   return( GetDow( (TDtDate)Timestamp));
} // DtDow

//------------------------------------------------------------------------------
// Time Validity
//------------------------------------------------------------------------------

int DtValidTime( TLocalTime *Local)
// Check time validity. Returns 0 if OK, else wrong field index
{
   if( Local->Sec > 59){
      return( DTE_SEC);
   }
   if( Local->Min > 59){
      return( DTE_MIN);
   }
   if( Local->Hour > 23){
      return( DTE_HOUR);
   }
   return( DTE_OK);
} // DtValidTime

//------------------------------------------------------------------------------
// Validity
//------------------------------------------------------------------------------

int DtValid( TLocalTime *Local)
// Check date/time validity. Returns 0 if OK, else wrong field index
{
int Result;
int LastDay;

   // check for time :
   Result = DtValidTime( Local);
   if( Result != DTE_OK){
      return( Result);
   }
   // check for date :
   if( Local->Month < DT_JANUARY || Local->Month > DT_DECEMBER){
      return( DTE_MONTH);
   }
   if( Local->Year > 67){
      return( DTE_YEAR);
   }
   LastDay = GetMonthDays( Local->Year, Local->Month);
   if( Local->Day < 1 || Local->Day > LastDay){
      return( DTE_DAY);
   }
   return( DTE_OK);
} // DtValid

//******************************************************************************

//------------------------------------------------------------------------------
// Encode date
//------------------------------------------------------------------------------

static TDtDate EncodeDate( byte Year, byte Month, byte Day)
// Encode <Local> to internal date representation
{
int Leaps;                  // count of leap days
int Tmp;

   Leaps = (Year + 4) / 4;             // 2000 (0) is first leap
   if( IsLeapYear( Year) && Month < DT_MARCH){
      Leaps--;                         // this year is leap, but low date
   }
   Tmp = Leaps + --Day;                // day of month startig at 0
   return( Tmp + (TDtDate)Year * 365 +  YearDay( Month));
} // EncodeDate

//******************************************************************************
// Na Datum
//******************************************************************************

static void DecodeDate( TDtDate Date, byte *Year, byte *Month, byte *Day)
// Decode internal <Date> to split representation
{
int Tmp;
int Y, M, D;
int YDay;
int YDays; // days of year

   Tmp  = Date / FOUR_YEAR_DAYS;     // count of four years
   YDay = Date % FOUR_YEAR_DAYS;     // offset by begin of four years
   Y    = Tmp * 4;                   // near low start of four years
   // correct year :
   while( 1){
      YDays = IsLeapYear( Y) ? 366 : 365;
      if( YDay < YDays){
         break;                      // ofset inside of year
      }
      Y++;                           // next year
      YDay -= YDays;
   }
   *Year = Y;
   // YDay is day of year, convert to MM:DD
   YDay++;                           // date starts at 1
   if( IsLeapYear( Y)){
      if( YDay > (31 + 29)){
         YDay--;                     // short by leap day
      } else if( YDay == (31 + 29)){
         *Day   = 29;                // is leap day now
         *Month = DT_FEBRUARY;
         return;
      }
   }
   M = DT_JANUARY;
   while( 1){
      D = MonthDay( M);              // day of month
      if( D >= YDay){
         break;
      }
      YDay -= D;                     // substract day of month
      M++;                           // next month
   }
   *Month = M;
   *Day   = (byte)YDay;
} // DecodeDate

//******************************************************************************
// Compose date & time
//******************************************************************************

static TTimestamp ComposeDateTime( TDtDate Date, TLocalTime *Time)
// Compose  <Date> with <Time>
{
TTimestamp Timestamp;

   Timestamp  = Time->Sec  * DT_SEC;
   Timestamp += Time->Min  * DT_MIN;
   Timestamp += Time->Hour * DT_HOUR;
   Timestamp += (TTimestamp)Date * DT_DAY;
   return( Timestamp);
} // ComposeDateTime

//******************************************************************************
// Decompose time
//******************************************************************************

static TDtDate DecomposeTime( TTimestamp Timestamp, TLocalTime *Time)
// Decode time from <Timestamp>, returns date
{
   Time->Sec  = Timestamp % 60;       // seconds
   Timestamp  /= 60;                   // remove seconds
   Time->Min  = Timestamp % 60;       // minutes
   Timestamp  /= 60;                   // remove minutes
   Time->Hour = Timestamp % 24;       // hours
   Timestamp  /= 24;                   // remove hours
   return( (TDtDate)Timestamp);
} // DecomposeTime

//******************************************************************************
// Day of week
//******************************************************************************

static int GetDow( TDtDate Date)
// Returns dow code for <Date>
{
   return( (Date + DOW_1_1_2000) % 7);
} // GetDow

//******************************************************************************
// Month days
//******************************************************************************

static int GetMonthDays( byte Year, byte Month)
// Returns month days
{
   if( Month == DT_FEBRUARY && IsLeapYear( Year)){
      // february & leap year
      return( 29);
   } // other months, inclusive normal february
   return( MonthDay( Month));
} // GetMonthDays

#ifdef DT_ENABLE_DST

//******************************************************************************
// Enable
//******************************************************************************

static TYesNo EnableDst( void)
// Returns Yes if DST is enabled
{
   return( DtGetDstType() != DT_DST_TYPE_OFF);
} // EnableDst

//******************************************************************************
// Letni cas
//******************************************************************************

static TYesNo IsDst( TLocalTime *Local)
// Returns YES for summer time. Input values are winter time
{
int StartDay;
const TTransitionDay *Start, *End;

   Start = &DstTransition[ DtGetDstType() - 1].Start;
   End   = &DstTransition[ DtGetDstType() - 1].End;
   if( Local->Month < Start->Month || Local->Month > End->Month){
      return( NO);       // out of summer time
   }
   if( Local->Month > Start->Month && Local->Month < End->Month){
      return( YES);      // surely summer time
   }
   if( Local->Month == Start->Month){
      // start of summer time
      StartDay = GetTransitionDay( Local->Year, Start);
      if( Local->Day > StartDay){
         return( YES);   // day after start
      }
      if( Local->Day < StartDay){
         return( NO);    // day before start
      }
      // transition day :
      if( Local->Hour > Start->Hour){
         return( YES);   // hour after transition
      }
      if( Local->Hour < Start->Hour){
         return( NO);
      }
      // transition hour :
      if( Local->Min  >= Start->Min){
         return( YES);   // min after transition
      } else {
         return( NO);    // min before transition
      }
   } else { // Month == DST_END_MONTH
      // end of summertime
      StartDay = GetTransitionDay( Local->Year, End);
      if( Local->Day > StartDay){
         return( NO);    // day after end
      }
      if( Local->Day < StartDay){
         return( YES);   // day before end
      }
      // transition day :
      if( Local->Hour > End->Hour){
         return( NO);    // hour after transition
      }
      if( Local->Hour < End->Hour){
         return( YES);   // hour before transition
      }
      // transition hour :
      if( Local->Min >= End->Min){
         return( NO);    // min after transition
      } else {
         return( YES);   // min before transition
      }
   }
} // IsDst

//******************************************************************************
// Transition Day
//******************************************************************************

static byte GetTransitionDay( byte Year, const TTransitionDay *Transition)
// Returns transition day of month
{
byte    LastDay;
byte    Day;
TDtDate Date;
byte    Dow;

   LastDay = GetMonthDays( Year, Transition->Month);
   Day   = Transition->Week * _DT_DOW_COUNT;            // get end of the week date
   if( Day > LastDay){
      Day = LastDay;                                    // month days saturation
   }
   Date  = EncodeDate( Year, Transition->Month, Day);   // internal representation
   Dow   = GetDow( Date);                   // day of week
   if( Dow < Transition->Dow){
      Dow += _DT_DOW_COUNT;                 // transition day is before Dow - rool back one week
   }
   Day  -= Dow;                             // monday
   Day  += Transition->Dow;                 // this day of week
   return( Day);
} // GetTransitionDay

#endif // DT_ENABLE_DST
