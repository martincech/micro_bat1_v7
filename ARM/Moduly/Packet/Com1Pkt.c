//*****************************************************************************
//
//   Com1Pkt.c    COM packet interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "../../inc/Com1Pkt.h"
#include "../../inc/CPktDef.h"            // format paketu

#ifdef PACKET1_TX_BLOCK
   #define PACKET_TX_BLOCK
#endif
#ifdef PACKET1_RX_BLOCK
   #define PACKET_RX_BLOCK
#endif
#define PACKET_RX_TIMEOUT   PACKET1_RX_TIMEOUT
#define PACKET_MAX_DATA     PACKET1_MAX_DATA

#define __COM1__
#include "../../inc/Com.h"

#define COMRxPacket     Com1RxPacket
#define COMTxPacket     Com1TxPacket         
#define COMTxBlockStart Com1TxBlockStart
#define COMTxBlockByte  Com1TxBlockByte
#define COMTxBlockEnd   Com1TxBlockEnd
#define COMRxData       Com1RxData

#include "ComPkt.c"
