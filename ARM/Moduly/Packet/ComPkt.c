//*****************************************************************************
//
//   ComPkt.c    COM packet interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

//-----------------------------------------------------------------------------
// Prijem
//-----------------------------------------------------------------------------

TYesNo COMRxPacket( int *cmd, int *arg)
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel
{
TDataConvertor dc;
byte     b;
unsigned crc;

   // s uvodnim cekanim :
   if( !ComRxWait( PACKET_RX_TIMEOUT)){
      return( NO);                     // timeout
   }
   ComRxChar( &b);
#ifdef PACKET_RX_BLOCK
   if( b == CPKT_DATA_START){
      *cmd = PACKET_DATA;
      *arg = 0;
      return( YES);                    // zbytek docteme v COMRxData
   }
#endif   
   if( b != CPKT_SHORT_START){
      ComFlushChars();                 // prijato smeti, cekat na dobeh vsech znaku
      return( NO);
   }
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc  = b;
   *cmd = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 0] = b;                   // LSB
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 1] = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 2] = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 3] = b;                   // MSB
   *arg = dc.dw;
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != (byte)~crc){
      return( NO);                     // chyba zabezpeceni
   }
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_SHORT_END){
      return( NO);                     // neplatne zakonceni
   }
   return( YES);
} // COMRxPacket

//-----------------------------------------------------------------------------
// Vyslani
//-----------------------------------------------------------------------------

void COMTxPacket( int cmd, int arg)
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>
{
TDataConvertor dc;
unsigned       crc;

   dc.dw = arg;
   ComTxChar( CPKT_SHORT_START);
   crc    = cmd;
   crc   += dc.array[ 0];
   crc   += dc.array[ 1];
   crc   += dc.array[ 2];
   crc   += dc.array[ 3];
   ComTxChar( cmd);
   ComTxChar( dc.array[ 0]);           // LSB
   ComTxChar( dc.array[ 1]);
   ComTxChar( dc.array[ 2]);
   ComTxChar( dc.array[ 3]);           // MSB
   ComTxChar( ~crc);
   ComTxChar( CPKT_SHORT_END);
} // COMTxPacket

#ifdef PACKET_TX_BLOCK

   static native _com_data_crc;                 // pro interni pouziti

//-----------------------------------------------------------------------------
// Datovy blok - start
//-----------------------------------------------------------------------------

void COMTxBlockStart( int size)
// Vysle zahlavi paketu pro data o velikosti <size>
{
   ComTxChar( CPKT_DATA_START);
   ComTxChar( size & 0xFF);            // Size1 LSB
   ComTxChar( size >> 8);              // MSB
   ComTxChar( size & 0xFF);            // Size2 LSB
   ComTxChar( size >> 8);              // MSB
   ComTxChar( CPKT_DATA_START);
   _com_data_crc = 0;
} // COMTxBlockStart

void COMTxBlockByte( byte b)
// Vysle byte <b> bloku dat
{
   ComTxChar( b);
   _com_data_crc += b;
} // COMTxBlockByte


//-----------------------------------------------------------------------------
// Datovy blok - konec
//-----------------------------------------------------------------------------

void COMTxBlockEnd( void)
// Uzavre datovy blok
{
   ComTxChar( (byte)~_com_data_crc);
   ComTxChar( CPKT_DATA_END);
} // COMTxBlockEnd
#endif // PACKET_TX_BLOCK


#ifdef PACKET_RX_BLOCK
//-----------------------------------------------------------------------------
// Prijem datoveho bloku
//-----------------------------------------------------------------------------

TYesNo COMRxData( void *buffer, int *size)
// Prijem datoveho bloku
{
byte     b, bb;
int      data_size;
unsigned crc;
int      i;
byte    *p;

   // v COMRxPacket bylo jiz prijato CPKT_DATA_START
   // Size1 :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( !ComRxChar( &bb)){
      return( NO);
   }
   data_size = ((native)bb << 8) | b;
   // Size2 :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( !ComRxChar( &bb)){
      return( NO);
   }
   if( data_size != (((int)bb << 8) | b)){
      return( NO);
   }
   if( data_size > PACKET_MAX_DATA){
      return( NO);                     // delka paketu presahuje buffer
   }
   *size = data_size;                  // navrat delky
   // Header :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_DATA_START){
      return( NO);
   }
   // Data :
   crc = 0;
   p   = (byte *)buffer;
   for( i = 0; i < data_size; i++){
      if( !ComRxChar( &b)){
         return( NO);
      }
      *p = b;
      p++;
      crc += b;
   }
   // Crc :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != (byte)~crc){
      return( NO);                     // chyba zabezpeceni
   }
   // End :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_DATA_END){
      return( NO);                     // neplatne zakonceni
   }
   return( YES);
} // COMRxData

#endif // PACKET_RX_BLOCK
