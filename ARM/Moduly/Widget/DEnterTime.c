//******************************************************************************
//                                                                            
//   DEnterTime.c   Display enter date & time
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "../../inc/Bcd.h"
#include "../../inc/conio.h"
#include "../../inc/Dt.h"
#include "../../inc/Country.h"
#include "../../inc/xTime.h"

// Local functions :

static unsigned TimeToBcd( TLocalTime *Local);
// Returns BCD time

static void BcdToTime( TLocalTime *Local, unsigned Bcd); 
// Converts <Bcd> to <Local>

static int TimeMaxField( void);
// Returns highest possible field index

static int TimeGetOffset( int Field);
// Returns character offset of the field

static int TimeGetNumber( int Field);
// Returns limit on <Field>

static void TimePrint( int Value);
// Print BCD <Value> time

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

// time fields 24hours (12 hours) :
typedef enum {
   FLD_1S,         // PM
   FLD_10S,        // 1s
   FLD_1MIN,       // 10s
   FLD_10MIN,      // 1min
   FLD_1HOUR,      // 10min
   FLD_10HOUR,     // 1h
   FLD_PM,         // 10h
   _FLD_LAST
} TTimeField;

TYesNo DEnterTime( TLocalTime *Local, int x, int y)
// Enter time
{
int    Field;         // active field
int    FieldMax;      // max field
int    Value;
int    OldValue;
TYesNo ShowCursor;    // show/hide cursor
TYesNo FlashCursor;   // draw/skip cursor
int    TmpValue;

   if( DtValidTime( Local) != DTE_OK){
      // repair garbage
      Local->Hour = 0;
      Local->Min  = 0;
      Local->Sec  = 0;
   }
   Field       = TimeMaxField();       // edit MSB
   FieldMax    = Field;                // max field
   Value       = TimeToBcd( Local);
   OldValue    = -1;
   ShowCursor  = YES;                  // show cursor
   FlashCursor = NO;                   // no cursor
   forever {
      if( Value != OldValue){          // value changed - redraw
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  DEnterTimeWidth(), DENTER_H);
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GTextAt( x, y);
         GSetFixedPitch();             // set nonproportional font
         TimePrint( Value);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || FlashCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            GBox( x + TimeGetOffset( Field) * GCharWidth(), y, GCharWidth() - 1, DENTER_H);
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP | K_REPEAT :
         case K_UP :
            ShowCursor = NO;                               // disable cursor
            TmpValue = BcdGetDigit( Value, Field);         // select digit
            if( TmpValue == TimeGetNumber( Field)){
               TmpValue = 0;
            } else {
               TmpValue++;
            }
            BeepKey();
            Value = BcdSetDigit( Value, Field, TmpValue);  // compose digit
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            ShowCursor = NO;                               // disable cursor
            TmpValue = BcdGetDigit( Value, Field);         // select digit
            if( TmpValue == 0){
               TmpValue = TimeGetNumber( Field);
            } else {
               TmpValue--;
            }
            BeepKey();
            Value = BcdSetDigit( Value, Field, TmpValue);  // compose digit
            break;

         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            ShowCursor = NO;                               // disable cursor
            if( Field == 0){
               BeepError();
               break;
            }
            BeepKey();
            Field--;
            OldValue   = -1;        // force redraw
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            ShowCursor = NO;                               // disable cursor
            if( Field == FieldMax){
               BeepError();
               break;
            }
            BeepKey();
            Field++;
            OldValue   = -1;        // force redraw
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            OldValue   = -1;        // force redraw
            break;

         case K_FLASH1 :
            FlashCursor = YES;
            OldValue    = -1;       // force redraw
            break;

         case K_FLASH2 :
            FlashCursor = NO;
            OldValue    = -1;       // force redraw
            break;

         case K_ENTER :
            BcdToTime( Local, Value);
            if( CountryGetTimeFormat() == TIME_FORMAT_12 &&
                 (Value & 0xF000000) && 
                ((Value & 0x0F00000) > 0x0200000)){
               // > 12:xx:xx
               Field   = FLD_10HOUR + 1;
               BeepError();
               OldValue   = -1;           // force redraw       
               break;
            }
            switch( DtValidTime( Local)){
               case DTE_OK :
                  BeepKey();
                  return( YES);
               case DTE_SEC :
                  Field   = FLD_10S;
                  break;
               case DTE_MIN :
                  Field   = FLD_10MIN;
                  break;
               case DTE_HOUR :
                  Field   = FLD_10HOUR;
                  break;
            }
            if( CountryGetTimeFormat() == TIME_FORMAT_12){
               Field++;                // shift because of AM/PM field
            }
            BeepError();
            OldValue   = -1;           // force redraw       
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEnterTime

//------------------------------------------------------------------------------
//  Time width
//------------------------------------------------------------------------------

int DEnterTimeWidth( void)
// Returns width of time field
{
   return( xTimeWidth() * GCharWidth());
} // DEnterTimeWidth

//******************************************************************************

//------------------------------------------------------------------------------
//  BCD time
//------------------------------------------------------------------------------

static unsigned TimeToBcd( TLocalTime *Local)
// Returns BCD time
{
unsigned Bcd;
int      Pm;

   if( CountryGetTimeFormat() == TIME_FORMAT_24){
      return( dbin2bcd( Local->Hour * 10000 + Local->Min * 100 + Local->Sec));
   }
   // TIME_FORMAT_12
   Pm = 0;
   if( Local->Hour >= 12){
      Pm = 1;
   }
   if( Local->Hour == 0){
      // 00:00 .. 00:59      
      Local->Hour = 12;                // 12:xx am
   } else if( Local->Hour > 12){
      // 13:00 .. 23:59
      Local->Hour -= 12;
   } // else 01:00 .. 12:59
   Bcd = dbin2bcd( Local->Hour * 100000 + Local->Min * 1000 + Local->Sec * 10);
   if( Pm){
      Bcd++;                           // add PM flag to LSB
   }
   return( Bcd);
} // TimeToBcd

static void BcdToTime( TLocalTime *Local, unsigned Bcd)
// Converts <Bcd> to <Local>
{
int Pm;

   if( CountryGetTimeFormat() == TIME_FORMAT_12){
      Pm  = Bcd & 0x0F;
      Bcd = dbcd2bin( Bcd >> 4);
   } else {
      Pm  = 0;
      Bcd = dbcd2bin( Bcd);
   }
   Local->Sec  = Bcd % 100;
   Bcd /= 100;
   Local->Min  = Bcd % 100;
   Bcd /= 100;
   Local->Hour = Bcd;
   if( CountryGetTimeFormat() == TIME_FORMAT_24){
      return;
   }
   if( Pm){
      // PM
      if( Local->Hour < 12){
         Local->Hour += 12;       // 01:00 pm .. 11:59 pm
      } // else 12:xx pm or error
   } else {
      // AM
      if( Local->Hour == 12){
         Local->Hour = 0;         // 12:00 am .. 12:59 am
      } // else 01:00 am .. 11:59 am or error
   }
} // BcdToTime

//------------------------------------------------------------------------------
//  Time field
//------------------------------------------------------------------------------

static int TimeMaxField( void)
// Returns highest possible field index
{
   if( CountryGetTimeFormat() == TIME_FORMAT_24){
      return( FLD_10HOUR);
   }
   // TIME_FORMAT_12
   return( FLD_PM);
} // TimeMaxField

//------------------------------------------------------------------------------
//  Time offset
//------------------------------------------------------------------------------

// time offsets :
static const byte Offset[ _TIME_FORMAT_COUNT][ _FLD_LAST] = { 
/* 1s */   {7, 6, 4, 3, 1, 0, 0}, /* 10 hour */
/* PM */   {8, 7, 6, 4, 3, 1, 0}  /* 10 hour */
};

static int TimeGetOffset( int Field)
// Returns character offset of the field
{ 
   return( Offset[ CountryGetTimeFormat()][ Field]);
} // TimeGetOffset

//------------------------------------------------------------------------------
//  Time number
//------------------------------------------------------------------------------

// time limits :
static const byte Limit[ _TIME_FORMAT_COUNT][ _FLD_LAST] = { 
/* 1s */   {9, 5, 9, 5, 9, 2, 0}, /* 10 hour */
/* PM */   {1, 9, 5, 9, 5, 9, 1}  /* 10 hour */
};

static int TimeGetNumber( int Field)
// Returns limit on <Field>
{
   return( Limit[ CountryGetTimeFormat()][ Field]);
} // TimeCheckNumber

//------------------------------------------------------------------------------
//  Time print
//------------------------------------------------------------------------------

static void TimePrint( int Value)
// Print BCD <Value> time
{
char Separator;
int  Hour, Min, Sec;
const char *PmStr;

   Separator = CountryGetTimeSeparator();
   if( CountryGetTimeFormat() == TIME_FORMAT_12){
      PmStr   = xTimeSuffix( Value & 0x0F);
      Value >>= 4;
   } else {
      PmStr   = "";
   }
   Hour = (Value >> 16) & 0xFF;
   Min  = (Value >>  8) & 0xFF;
   Sec  =  Value & 0xFF;
   cprintf( "%02x%c%02x%c%02x%s", Hour, Separator, Min, Separator, Sec, PmStr);
} // TimePrint
