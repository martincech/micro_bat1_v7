//******************************************************************************
//                                                                            
//   DEnterText.c   Display enter text box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DInput.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "../../inc/conio.h"
#include "Str.h"                        // strings from project directory
#include <string.h>

// Letter table :
static const char Letters[] = "0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZ_+-*/=!?.,:;%#&";
#define LETTERS_COUNT   ((int)(sizeof( Letters) - 1))

// Local functions :

static int FindLetter( char Letter);
// Find letter

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DEnterText( char *Text, int Width, int x, int y)
// Display and edit <Text> with maximal <Width>
{
int    Position;      // cursor position
int    LetterIndex;   // index in the Letters array
TYesNo Redraw;        // redraw text
TYesNo ShowCursor;    // show/hide cursor
TYesNo DrawCursor;    // draw/skip cursor

   Position    = 0;                    // at first character
   Redraw      = YES;                  // first draw
   ShowCursor  = YES;                  // show cursor
   DrawCursor  = NO;                   // wait for flash
   StrSetWidth( Text, Width);          // fill with spaces
   LetterIndex = FindLetter( Text[ Position]);
   forever {
      if( Redraw){
         // update character :
         Text[ Position] = Letters[ LetterIndex];
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  DEnterTextWidth( Width), DENTER_H);
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GTextAt( x + 1, y);
         GSetFixedPitch();             // set nonproportional font
         cputs( Text);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || DrawCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            GBox( x + 1 + Position * GCharWidth(), y, GCharWidth() - 1, DENTER_H);
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         Redraw = NO;
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            BeepKey();
            ShowCursor = NO;           // disable cursor
            if( LetterIndex == LETTERS_COUNT - 1){
               LetterIndex = 0;
            } else {
               LetterIndex++;
            }
            Redraw = YES;              // redraw edit box
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            BeepKey();
            ShowCursor = NO;           // disable cursor
            if( LetterIndex == 0){
               LetterIndex = LETTERS_COUNT - 1;
            } else {
               LetterIndex--;
            }
            Redraw = YES;              // redraw edit box
            break;

         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            if( Width < 2){
               break;
            }
            ShowCursor = NO;           // disable cursor
            if( Position == Width - 1){
               BeepError();
               break;
            }
            BeepKey();
            Position++;
            LetterIndex = FindLetter( Text[ Position]);
            Redraw = YES;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            if( Width < 2){
               break;
            }
            ShowCursor = NO;           // disable cursor
            if( Position == 0){
               BeepError();
               break;
            }
            BeepKey();
            Position--;
            LetterIndex = FindLetter( Text[ Position]);
            Redraw = YES;
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            Redraw     = YES;
            break;

         case K_FLASH1 :
            DrawCursor = YES;
            Redraw     = YES;
            break;

         case K_FLASH2 :
            DrawCursor = NO;
            Redraw     = YES;
            break;

         case K_ENTER :
            BeepKey();
            return( YES);
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DInputText

//------------------------------------------------------------------------------
//  Text Width
//------------------------------------------------------------------------------

int DEnterTextWidth( int Width)
// Returns pixel width of text field
{
   return( Width * GCharWidth() + 1);
} // DEnterTextWidth

//******************************************************************************

//------------------------------------------------------------------------------
//  Find
//------------------------------------------------------------------------------

static int FindLetter( char Letter)
// Find letter
{
int  i;

   for( i = 0; i < LETTERS_COUNT; i++){
      if( Letter == Letters[ i]){
         return( i);
      }
   }
   return( 0);
} // FindLetter
