//*****************************************************************************
//
//    Exception.c   CPU exception handling
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../Inc/System.h"
#include "../Inc/Cpu.h"

void __undef undef_handler( void);
// Undefined instruction exception handler

void __swi swi_handler( void);
// SWI instruction handler

void __abort pabort_handler( void);
// Prefetch abort handler

void __abort dabort_handler( void);
// Data abort handler

//------------------------------------------------------------------------------
//  Undefined
//------------------------------------------------------------------------------

void __undef undef_handler( void)
// Undefined instruction exception handler
{
register unsigned Address;

   __asm__ __volatile__( " mov %0, lr" : "=r" (Address) : /* no inputs */  );
   SysException( EXCEPTION_UNDEFINED, Address);
} // undef_handler

//------------------------------------------------------------------------------
//  SWI
//------------------------------------------------------------------------------

void __swi swi_handler( void)
// SWI instruction handler
{
register unsigned Address;

   __asm__ __volatile__( " mov %0, lr" : "=r" (Address) : /* no inputs */  );
   SysException( EXCEPTION_SWI, Address);
} // swi_handler

//------------------------------------------------------------------------------
//  Abort prefetch
//------------------------------------------------------------------------------

void __abort pabort_handler( void)
// Prefetch abort handler
{
register unsigned Address;

   __asm__ __volatile__( " mov %0, lr" : "=r" (Address) : /* no inputs */  );
   SysException( EXCEPTION_PREFETCH_ABORT, Address);
} // pabort_handler

//------------------------------------------------------------------------------
//  Abort data
//------------------------------------------------------------------------------

void __abort dabort_handler( void)
// Data abort handler
{
register unsigned Address;

   __asm__ __volatile__( " mov %0, lr" : "=r" (Address) : /* no inputs */  );
   SysException( EXCEPTION_DATA_ABORT, Address);
} // dabort_handler
