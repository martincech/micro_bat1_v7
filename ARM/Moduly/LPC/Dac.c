//*****************************************************************************
//
//    Dac.c        Internal D/A convertor functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Dac.h"

#if  defined( __LPC213x__)
   // P0.25 - AOUT function 10
   #define PINSEL        PCB_PINSEL1        
   #define PINSEL_MASK   (3 << 18)   // PINSEL1, P0.25 dibits
   #define PINSEL_UART   (2 << 18)   // PINSEL1 function 10 on P0.25
#else
   #error "Unknown processor model"
#endif

#ifdef DAC_LOW_POWER
   #define DAC_BIAS DACR_BIAS          // 350uA, 2,5us
#else
   #define DAC_BIAS 0                  // 700uA, 1us
#endif

//------------------------------------------------------------------------------
//   Initialisation
//------------------------------------------------------------------------------

void DacInit( void)
// Inicializace prevodniku
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_UART; // select pin fuction
   DAC_DACR = DAC_BIAS;                            // BIAS & Value = 0
} //  DacInit

//------------------------------------------------------------------------------
//   Nastaveni hodnoty
//------------------------------------------------------------------------------

void DacValue( word Value)
// Nastaveni hodnoty <Value>
{
   DAC_DACR = DAC_BIAS | DACR_VALUE( Value);
} // DacValue
