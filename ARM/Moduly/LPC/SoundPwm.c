//*****************************************************************************
//
//    SoundPwm.c   Sound creation wia PWM hardware
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Sound.h"
#include "../../inc/System.h"
#include "../../inc/Cpu.h"

#if   SOUND_CHANNEL == 1
   // P0.0
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 << 0)                // PINSEL0, P0.0 dibit
   #define PINSEL_PWM    (2 << 0)                // PINSEL0 function 10 on P0.0
   #define SOUND_PIN      0
   #define PWM_MR        PWM_MR1
#elif SOUND_CHANNEL == 2
   // P0.7
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 << 14)               // PINSEL0, P0.7 dibit
   #define PINSEL_PWM    (2 << 14)               // PINSEL0 function 10 on P0.7
   #define SOUND_PIN      7
   #define PWM_MR        PWM_MR2
#elif SOUND_CHANNEL == 3
   // P0.1
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 <<  2)               // PINSEL0, P0.1 dibit
   #define PINSEL_PWM    (2 <<  2)               // PINSEL0 function 10 on P0.1
   #define SOUND_PIN      1
   #define PWM_MR        PWM_MR3
#elif SOUND_CHANNEL == 4
   // P0.8
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 << 16)               // PINSEL0, P0.8 dibit
   #define PINSEL_PWM    (2 << 16)               // PINSEL0 function 10 on P0.8
   #define SOUND_PIN      8
   #define PWM_MR        PWM_MR4
#elif SOUND_CHANNEL == 5
   // P0.21
   #define PINSEL        PCB_PINSEL1             // index : 21 - 16 = 5
   #define PINSEL_MASK   (3 << 10)               // PINSEL1, P0.21 dibit
   #define PINSEL_PWM    (1 << 10)               // PINSEL1 function 01 on P0.21
   #define SOUND_PIN     21
   #define PWM_MR        PWM_MR5
#elif SOUND_CHANNEL == 6
   // P0.9
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 << 18)               // PINSEL0, P0.9 dibit
   #define PINSEL_PWM    (2 << 18)               // PINSEL0 function 10 on P0.9
   #define SOUND_PIN      9
   #define PWM_MR        PWM_MR6
#else
   #error "Invalid SOUND_CHANNEL"
#endif

#ifdef SOUND_PINSEL_INTERRUPT
   #define SndDisableInts()   DisableInts()
   #define SndEnableInts()    EnableInts()
#else
   #define SndDisableInts()
   #define SndEnableInts()
#endif

#define SOUND_DIR     GPIO0_IODIR      // port direction
#define SOUND_CLR     GPIO0_IOCLR      // port output clear
#define SOUND_SET     GPIO0_IOSET      // port output set

volatile short _snd_duration;          // citac delky

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void SndInit( void)
// Initialisation
{
#ifdef SOUND_FORCE
   SndInitPorts();
#endif
#ifndef SOUND_ACTIVE_L
   SOUND_CLR  = (1 << SOUND_PIN);                // inactive level L
#else
   SOUND_SET  = (1 << SOUND_PIN);                // inactive level H
#endif
   SOUND_DIR |= (1 << SOUND_PIN);                // output
   PWM_PR  = 0;                                  // prescaler
   PWM_TCR = PWMTCR_RESET;                       // reset
   PWM_MCR = PWMMCR_RESET0;                      // nulovani od PWM0
   PWM_PCR = PWMPCR_PWMENA( SOUND_CHANNEL);      // povoleni PWM vystupu
} // SndInit

//-----------------------------------------------------------------------------
// Pipnuti
//-----------------------------------------------------------------------------

void SndBeep( int Note, int Volume, int Duration)
// Plays asynchronously <Note> with <Volume> and <Duration> [ms]
{
   _snd_duration = (Duration) / TIMER_PERIOD;
   SndSound( Note, Volume);
}  // SndBeep

//-----------------------------------------------------------------------------
// Synchronni pipnuti
//-----------------------------------------------------------------------------

void SndSBeep( int Note, int Volume, int Duration)
// Plays synchronously <Note> with <Volume> and <Duration> [ms]
{
   SndSound( Note, Volume);
   SysDelay( Duration);
   SndNosound();
   SysDelay(10);
} // SndSBeep

#ifdef SOUND_PINSEL_INTERRUPT
//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void SndNosound( void)
// Stops sound
{
   SndDisableInts();
   SndNosoundInt();
   SndEnableInts();
} // SndNosound

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void SndNosoundInt( void)
// Stops sound inside interrupt (WARNING - for internal use only)
{
#ifdef SOUND_FORCE
   SndSilent();
#endif
   PWM_TCR = PWMTCR_RESET;             // counter reset
   PINSEL =  PINSEL & ~PINSEL_MASK;    // deselect pin fuction
} // SndNosound

#else
//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void SndNosound( void)
// Stops sound (inside interrupt)
{
#ifdef SOUND_FORCE
   SndSilent();
#endif
   PWM_TCR = PWMTCR_RESET;             // counter reset
   PINSEL =  PINSEL & ~PINSEL_MASK;    // deselect pin fuction
} // SndNosound
#endif

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

static const word _Frequency[ 12] = {
   /* NOTE_C4   */ 20930,
   /* NOTE_CIS4 */ 22175,
   /* NOTE_D4   */ 23493,
   /* NOTE_DIS4 */ 24890,
   /* NOTE_E4   */ 26370,
   /* NOTE_F4   */ 27938,
   /* NOTE_FIS4 */ 29600,
   /* NOTE_G4   */ 31360,
   /* NOTE_GIS4 */ 33224,
   /* NOTE_A4   */ 35200,
   /* NOTE_AIS4 */ 37293,
   /* NOTE_H4   */ 39511,
};

static const word _Gain[ VOL_MAX + 1] = {
#if defined( SOUND_3DB)
  0,630,880,1250,1760,2500,3530,5000,7070,10000};    // 3dB step
#elif defined( SOUND_4DB)
  0,250,400,630,1000,1580,2510,3980,6310,10000};     // 4dB step
#elif defined( SOUND_6DB)
  0,40,80,160,310,630,1250,2500,5000,10000};          // 6dB step
#elif defined( SOUND_FORCE)
  0,100,400,1200,100,233,547,1279,2993,7000};         // two stage volume 7dB step
#else
  0,10,55,109,219,438,875,1750,3500,7000};            // 6dB step modified
#endif

#define VOL_TRESHOLD 3            // two stage volume treshold

void SndSound( int Note, int Volume)
// Starts play <Note> with <Volume>
{
int Width;
int Count;
int Octave;

   if( Volume == 0){
      SndNosound();                                             // silent
      return;
   }
   SndDisableInts();
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_PWM;               // select pin fuction
   SndEnableInts();
   Octave = Note / 12;                                          // get octave 0..OCTAVE_MAX
   Note  %= 12;                                                 // get note in octave
   if( Octave > OCTAVE_MAX){
      Count =  (FBUS * 10) / (2 * _Frequency[ Note]);           // play C5..H5 instead
   } else {
      Count = ((FBUS * 10) * (1 << (OCTAVE_MAX - Octave))) / _Frequency[ Note];
   }
   Width   = (Count * _Gain[ Volume]) / 20000;                  // pulse width
   if( Width < 1){
      Width = 1;                                                // minimal width
   }
   PWM_MR0 = Count;                                             // period
#ifndef SOUND_ACTIVE_L
   PWM_MR  = Width;                                             // width
#else
   PWM_MR  = Count - Width;                                     // inverted width
#endif
   PWM_LER = PWMLER_ENABLE0 | PWMLER_ENABLE( SOUND_CHANNEL);    // enable latch
   PWM_TCR = PWMTCR_ENABLE  | PWMTCR_PWM_ENABLE;                // enable counting and PWM
#ifdef SOUND_FORCE
   if( Volume > VOL_TRESHOLD){
      SndStrong();
   } else {
      SndSilent();
   }
#endif
} // SndSound
