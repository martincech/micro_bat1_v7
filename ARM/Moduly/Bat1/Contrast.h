//******************************************************************************
//                                                                            
//   Contrast.h     Display contrast
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Contrast_H__
   #define __Contrast_H__

void ContrastSet( void);
// Set contrast

void ContrastTest( int Contrast);
// Test contrast

#endif
