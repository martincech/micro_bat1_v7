//*****************************************************************************
//
//    Bat1.h   -  Bat1 project definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Bat1_H__
   #define __Bat1_H__

// Device data :
#define BAT1_VERSION          0x0704         // Version number
#define BAT1_BUILD            0x04           // Build number
#define BAT1_HW_VERSION       0x01           // Hardware version
#define BAT1_SCALE_NAME       "SCALE 1"      // Default scale name
#define BAT1_DEFAULT_FILE     "FILE000"      // Default file name

// Country data :
#define BAT1_COUNTRY  COUNTRY_INTERNATIONAL
#define BAT1_LANGUAGE LNG_ENGLISH

// Weighing display limits :
#define BAT1_WEIGHT_MAX        99999
#define BAT1_WEIGHT_MIN        -9999

// Units :
#define UNITS_KG_RANGE        30000         // basic range
#define UNITS_KG_EXT_RANGE    50000         // extended range
#define UNITS_KG_DECIMALS     3
#define UNITS_KG_MAX_DIVISION 100
#define UNITS_KG_DIVISION     1

#define UNITS_G_RANGE         30000
#define UNITS_G_EXT_RANGE     50000
#define UNITS_G_DECIMALS      0
#define UNITS_G_MAX_DIVISION  100
#define UNITS_G_DIVISION      1

#define UNITS_LB_RANGE        60000
#define UNITS_LB_EXT_RANGE    99999
#define UNITS_LB_DECIMALS     3
#define UNITS_LB_MAX_DIVISION 100
#define UNITS_LB_DIVISION     1

// Saving parameters :
#define SAVING_FILTER          10      // 0.1s
#define SAVING_STABILISATION   5       // 0.1s
#define SAVING_RANGE           10      // 0.1%

// Saving parameters by units :
#define SAVING_MINIMUM_KG      100
#define SAVING_MINIMUM_G       100
#define SAVING_MINIMUM_LB      200

// Number of birds :
#define NUMBER_OF_BIRDS_DEFAULT  1

// Backlight :
#define BACKLIGHT_DURATION   20         // 1s

// Timeouts :
#define KEYBOARD_TIMEOUT     180        // 1s
#define POWER_OFF_TIMEOUT    900        // 1s

// Printer defaults :
#define PRINTER_PAPER_WIDTH  32
#define PRINTER_SPEED        9600

// Statistic parameters :
#define HISTOGRAM_RANGE      30
#define HISTOGRAM_STEP       10

#endif
