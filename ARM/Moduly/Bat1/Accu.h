//*****************************************************************************
//
//    Accu.h       Accumulator utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Accu_H__
   #define __Accu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void AccuInit( void);
// Initialisation

TYesNo AccuMaitenance( void);
// Process accumulator maitenance

unsigned AccuRawVoltage( void);
// Measure accumulator voltage [LSB]

TYesNo AccuFormat( void);
// Format (charge) accu

TYesNo AccuDischarge( void);
// Discharge accu

TYesNo AccuCharge( void);
// Charge accu

void AccuSaveCurve( int SamplesCount);
// Save discharging curve data

#endif
