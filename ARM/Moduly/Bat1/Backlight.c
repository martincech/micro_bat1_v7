//*****************************************************************************
//
//    Backlight.c   Backlight functions
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Backlight.h"
#include "../inc/System.h"
#include "../inc/Pwm.h"
#include "ConfigDef.h"

#define BLT_FLAG_CHARGER   0x01         // charger mode
#define BLT_FLAG_BOOT      0x02         // boot mode

static byte BltFlag;

// Local functions :

static int PwmPercent( int Intensity);
// PWM duty cycle by backlight <Intensity>

static int PwmIntensity( void);
// PWM duty by current intensity

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void BacklightInit( void)
// Initialisation
{
   BltFlag = 0;
   PwmInit();
} // BacklightInit

//------------------------------------------------------------------------------
//  Charger
//------------------------------------------------------------------------------

void BacklightCharger( void)
// Set charger mode
{
   BltFlag = BLT_FLAG_CHARGER;
   PwmStart( PwmIntensity());
} // BacklightCharger

//------------------------------------------------------------------------------
//  Normal
//------------------------------------------------------------------------------

void BacklightNormal( void)
// Set normal mode
{
   BltFlag = 0;
   switch( Config.Display.Backlight.Mode){
      case BACKLIGHT_MODE_ON :
      case BACKLIGHT_MODE_AUTO :
         PwmStart( PwmIntensity());
         break;
      case BACKLIGHT_MODE_OFF :
         PwmStop();
         break;
   }
} // BacklightNormal

//------------------------------------------------------------------------------
//  Boot
//------------------------------------------------------------------------------

void BacklightBoot( void)
// Set backlight mode to boot
{
   BltFlag = BLT_FLAG_BOOT;
   PwmStop();
} // BacklightBoot


//------------------------------------------------------------------------------
//  On
//------------------------------------------------------------------------------

void BacklightOn( void)
// Conditionaly on
{
   if( BltFlag & BLT_FLAG_BOOT){
      return;                          // direct control
   }
   if( BltFlag & BLT_FLAG_CHARGER){
      PwmStart( PwmIntensity());
   }
   // normal mode
   if( Config.Display.Backlight.Mode != BACKLIGHT_MODE_AUTO){
      return;                          // always on/off
   }
   PwmStart( PwmIntensity());
} // BacklightOn

//------------------------------------------------------------------------------
//  Off
//------------------------------------------------------------------------------

void BacklightOff( void)
// Conditionaly off
{
   if( BltFlag & BLT_FLAG_BOOT){
      return;                          // direct control
   }
   if( BltFlag & BLT_FLAG_CHARGER){
      PwmStop();
   }
   // normal mode
   if( Config.Display.Backlight.Mode == BACKLIGHT_MODE_ON){
      return;                          // always on
   }
   PwmStop();
} // BacklightOff

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

void BacklightShutdown( void)
// Power off
{
   PwmStop();
} // BacklightShutdown

//------------------------------------------------------------------------------
//  Test
//------------------------------------------------------------------------------

void BacklightTest( int Intensity)
// Test backlight intensity
{
   PwmStart( PwmPercent( Intensity));
} // BacklightTest

//-----------------------------------------------------------------------------
//   Backlight off
//-----------------------------------------------------------------------------

void BacklightDark( void)
// Unconditionaly Backlight off
{
   BltOff();
} // BacklightDark

//-----------------------------------------------------------------------------
//   Backlight on
//-----------------------------------------------------------------------------

void BacklightLight( void)
// Unconditionaly Backlight on
{
   BltOn();
} // BacklightLight

//-----------------------------------------------------------------------------
//   Backlight flash
//-----------------------------------------------------------------------------

void BacklightFlash( void)
// Unconditionaly flash backlight
{
   BacklightLight();
   SysDelay( 1000);
   BacklightDark();
} // BacklightFlash

//*****************************************************************************

//------------------------------------------------------------------------------
//  Percent
//------------------------------------------------------------------------------

static const byte Percent[ BACKLIGHT_MAX + 1] =
{ 0,10,12,16,21,28,35,50,73,100};

static int PwmPercent( int Intensity)
// PWM duty cycle by backlight intensity
{
   return( Percent[ Intensity]);
} // PwmPercent

//------------------------------------------------------------------------------
//  Intensity
//------------------------------------------------------------------------------

static int PwmIntensity( void)
// PWM duty by current intensity
{
   return( PwmPercent( Config.Display.Backlight.Intensity));
} // PwmIntensity
