//******************************************************************************
//                                                                            
//   Main.c         Bat1 main
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Cpu.h"      // CPU startup
#include "../Inc/System.h"   // Operating system
#include "../Inc/Sound.h"    // Sound
#include "../Inc/Kbd.h"      // Keyboard
#include "../Inc/Uart1.h"    // UART1

#include "../Inc/Graphic.h"  // graphic
#include "../Inc/conio.h"    // Display

#include "../Inc/Rtc.h"      // RTC
#include "../Inc/Dt.h"       // Date & Time
#include "Cal.h"             // Calibration
#include "Config.h"          // Configuration
#include "../Inc/File/Fs.h"  // FileSystem

#include "PMan.h"            // Power management
#include "Beep.h"            // Sound
#include "Contrast.h"        // Contrast
#include "Backlight.h"       // Backlight
#include "Usb.h"             // USB interface
#include "Weighing.h"        // Weighing
#include "Screen.h"          // Basic screen
#include "Menu.h"            // Menu loop
#include "MenuBoot.h"        // Boot menu loop
#include "Password.h"        // Password checking
#include "Crash.h"           // Crash record

#define SHUTDOWN_COUNT 6     // shutdown autorepeat keys (autorepeat start + n * autorepeat speed)

// Inactivity timeout
#define SetKeyboardTimeout()  KeyboardTimeout  = Config.KeyboardTimeout;
#define SetBacklightTimeout() BacklightTimeout = Config.Display.Backlight.Duration
#define SetPowerOffTimeout()  PowerOffTimeout  = Config.PowerOffTimeout

// Local variables
static word KeyboardTimeout;       // keyboard inactivity timeout
static word BacklightTimeout;      // backlight timeout
static word PowerOffTimeout;       // power off timeout

static byte ChargerPower;          // charger connected
static byte UsbPower;              // USB connected

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   PManSetVYP();                       // hold power
   CpuInit();
   // basic peripherals init :
   KbdInit();
   SndInit();
   BacklightInit();
   Uart1Init();
   UsbInit();
   PManInit();                         // no return on accu empty
   // run-time peripherals init :
   RtcInit();
   GInit();
   // Load configuration data :
   ConfigLoad();                       // configuration
   CalLoad();                          // calibration
   ContrastSet();                      // contrast by config
   // Delayed initialisation :
   FsInit();                           // filesystem initialisation
   WeighingInit();                     // weighing initialisation
   // system run :   
   EnableInts();                       // enable interrupts
   SysStartTimer();                    // system timer/clock
   SysEnableTimeout();                 // start inactivity timers
   if( WatchDogActive()){
      CrashWatchDog();                 // Record watchdog restart
   }
   StartWatchDog();                    // start watchdog
   ChargerPower = PManCharger();       // get charger status
   UsbPower     = PManUsb();           // get USB status
   if( PManBoot()){
      // boot loop :
      ContrastTest( CONTRAST_MAX / 2); // default contrast
      BacklightCharger();              // low power backlight
      ScreenBoot();
      BeepBoot();
      KbdPowerUpRelease();             // wait for key release
      SysDelay( 1000);                 // show screen
      MenuBoot();                      // return on power off
      BeepBye();                       
      BacklightShutdown();             // backlight off
      PManPowerOff();                  // do power off - no return
   }
   // main loop :
   forever {
      BacklightShutdown();             // charge only may shutdown - backlight off
      if( PManChargeOnly()){           // no return on shutdown and no charger
         BacklightCharger();           // charger mode
         MenuCharger();                // returns on power on key
      }
      // greeting :
      BacklightNormal();               // normal mode
      ScreenLogo();
      BeepGreeting();
      KbdPowerUpRelease();             // wait for key release
      PasswordClear();                 // clear password count after power on key
      MenuExecute();                   // execute up to shutdown
   }
   return( 0);
} // main

//------------------------------------------------------------------------------
//  Yield
//------------------------------------------------------------------------------

int SysYield( void) 
// Background process & check for events
{
static byte ShutdownCounter = SHUTDOWN_COUNT;
int    Key;

   CpuIdleMode();                      // enter power saving IDLE mode
   if( PManShutdown()){
      return( K_SHUTDOWN);             // do nothing - shutdown in process
   }
   if( !PManBoot()){
      // normal mode
      if( PManAccuFail()){
         return( K_SHUTDOWN);          // do nothing - accu failure
      }
      // fast processes -----------------------------------------------------------
      PManExecute();                   // power management measurings
      if( PManUsb()){
         UsbExecute();                 // USB connected, check for data
      }
      // test for charger power :
      if( PManCharger() != ChargerPower){
         // charger power changed
         ChargerPower = PManCharger(); // rememer new value
         SysResetTimeout();            // reset timeout & backlight on
      }
      // test for USB power :
      if( PManUsb() != UsbPower){
         // USB power changed
         UsbPower = PManUsb();         // rememer new value
         SysResetTimeout();            // reset timeout & backlight on
         // set USB status :
         if( UsbPower){
            UsbConnect();              // power rise
         } else {
            UsbDisconnect();           // power fall
         }
      }
   } // else boot mode
   // slow processes -----------------------------------------------------------
   if( Sys1sExpired()){
      WatchDog();                      // refresh watchdog
      PManExecuteSlow();               // power management slow processing
      if( KeyboardTimeout){
         if( !(--KeyboardTimeout)){
            SetKeyboardTimeout();      // restart
            return( K_TIMEOUT);        // inactivity timeout
         }
         if( !(--BacklightTimeout)){
            SetBacklightTimeout();     // restart
            BacklightOff();            // backlight off
         }
      } // else inactivity timers stopped
      if( PowerOffTimeout){
         if( !(--PowerOffTimeout)){
            PManSetShutdown();         // set shutdown flag
            return( K_SHUTDOWN);       // send shutdown key            
         }
      }
      return( K_REDRAW);               // 1s redraw
   }
   // --------------------------------------------------------------------------
   // check for flash :
   if( SysIsFlash1()){
      return( K_FLASH1);
   }
   if( SysIsFlash2()){
      return( K_FLASH2);
   }
   // check for keyboard :
   Key = KbdGet();
   if( Key != K_IDLE){
      SysResetTimeout();               // reset inactivity timeout
      if( Key == (K_ESC | K_REPEAT)){
         // check for shutdown keys
         if( !(--ShutdownCounter)){
            KbdDisable();              // disable autorepeat
            PManSetShutdown();         // set shutdown flag
            return( K_SHUTDOWN);       // send shutdown key
         }
         return( Key);                 // return key only
      }
      ShutdownCounter = SHUTDOWN_COUNT;
      return( Key);
   }
   return( K_IDLE);
} // SysYield

//------------------------------------------------------------------------------
//  Timeouts
//------------------------------------------------------------------------------

void SysDisableTimeout( void)
// Disable timeout
{
   KeyboardTimeout  = 0;               // stop running
   BacklightTimeout = 0;
   PowerOffTimeout  = 0;
} // SysDisableTimeout

void SysEnableTimeout( void)
// Enable timeout
{
   SetKeyboardTimeout();               // start keyboard timeout
   SetBacklightTimeout();              // start backlight timeout
   SetPowerOffTimeout();               // start power off timeout
} // SysEnableTimeout

void SysResetTimeout( void)
// Reset timeout
{
   BacklightOn();                      // backlight on
   if( KeyboardTimeout){
      // is enabled
      SetKeyboardTimeout();            // start keyboard timeout
      SetBacklightTimeout();           // start backlight timeout
      SetPowerOffTimeout();            // start power off timeout
   }
} // SysResetTimeout

//------------------------------------------------------------------------------
//   Exception
//------------------------------------------------------------------------------

void SysException( int Type, int Address)
// CPU exception occured
{
   WatchDogNaked();                    // refresh watchdog
   CrashException( Type, Address);     // record exception
   FsClose();                          // try close file
   forever;                            // wait for WatchDog
} // SysException
