//******************************************************************************
//                                                                            
//   DEdit.h        Display edit box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DEdit_H__
   #define __DEdit_H__

#ifndef __StrDef_H__
   #include "../../Inc/Wgt/StrDef.h"
#endif

#ifndef __DtDef_H__
   #include "../../Inc/DtDef.h"
#endif

#ifndef __DCallback_H__
   #include "../../Inc/Wgt/DCallback.h"
#endif

#ifndef __DEnterList_H__
   #include "../../Inc/Wgt/DEnterList.h"  // DefList() macros
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo DEditNumber( int x, int y, int *Value, 
                    int Decimals, int LoLimit, int HiLimit, TUniStr Units);
// Edit number

TYesNo DEditEnum( int x, int y, int *Value, 
                  TUniStr Base, int EnumCount);
// Edit enum

TYesNo DEditEnumCallback( int x, int y, int *Value, 
                          TUniStr Base, int EnumCount, TAction *OnChange);
// Edit enum with <OnChange> callback

TYesNo DEditText( int x, int y, char *String, int CharCount);
// Edit text up to <Width> letters

TYesNo DEditList( int x, int y, int *Value, const TUniStr *List);
// Edit list

TYesNo DEditSpin( int x, int y, int *Value, 
                  int MinValue, int MaxValue, TAction *OnChange);
// Edit value by spinner

#endif
