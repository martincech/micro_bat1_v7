//******************************************************************************
//                                                                            
//   DInputSpin.c   Input dialog for spinner
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DInputSpin.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/Graphic.h"
#include "DLayout.h"
#include "../Bitmap.h"                 // project bitmaps
#include "../Str.h"                    // strings from project directory
#include "../Fonts.h"                  // project fonts

//------------------------------------------------------------------------------
//  Spin
//------------------------------------------------------------------------------

TYesNo DInputSpin( TUniStr Caption, TUniStr Text, int *Value, 
                   int MinValue, int MaxValue, TAction *OnChange)
// Input value by spinner
{
int x;                    // edit field position
int Width;

   SetFont( DINPUT_FONT);
   Width = DEnterSpinWidth( MaxValue);
   x = G_WIDTH / 2 - Width / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   // arrows :
   GBitmap( x + Width / 2 - 3, DINPUT_EDIT_Y - 5,                 &BmpEditUp);
   GBitmap( x + Width / 2 - 3, DINPUT_EDIT_Y + DINPUT_EDIT_H + 1, &BmpEditDown);
   GFlush();
   // edit enum :
   return( DEnterSpin( Value, MinValue, MaxValue, OnChange, x, DINPUT_EDIT_Y));
} // DInputSpin
