//******************************************************************************
//                                                                            
//  DSamples.h     Display samples database
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DData_H__
   #define __DData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FdirDef_H__
   #include "../File/FdirDef.h"
#endif

void DSamplesDisplay( TFdirHandle Handle);
// Display samples database data

#endif


