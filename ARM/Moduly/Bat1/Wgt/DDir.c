//******************************************************************************
//
//  DDir.c         Display file directory
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DDir.h"
#include "../../Inc/Wgt/DEvent.h"
#include "../../Inc/Wgt/DMsg.h"
#include "../../Inc/Wgt/DLabel.h"
#include "../../Inc/Wgt/Beep.h"
#include "../../Inc/File/Fd.h"
#include "../../Inc/File/Ds.h"
#include "../../Inc/Graphic.h"
#include "../../Inc/conio.h"
#include "../../Inc/Cpu.h"                // WatchDog
#include "DLayout.h"                      // Display layout
#include "../Str.h"                       // Project directory strings
#include "../Fonts.h"                     // Project fonts
#include "../Bitmap.h"                    // Bitmaps
#include "../Sdb.h"                       // SdbFileSize

#define DDIR_TEXT_Y        25                // top of the text area
#define DDIR_ITEM_HEIGHT   19                // item height

#define DDIR_HEIGHT  (G_HEIGHT - DDIR_TEXT_Y - DLAYOUT_STATUS_H) // items area height
#define DDIR_COUNT   (DDIR_HEIGHT / DDIR_ITEM_HEIGHT)            // items per page

// Local functions :

static int DisplaySelection( TFdirHandle Handle, int ShowSize);
// Display and edit, retuns index

static TYesNo DisplaySetSelection( TFDataSet CheckSet);
// Display and edit set. Returns NO on Esc

static void DisplayPage( TFDataSet CheckSet, int CurrentPage, int LastCursorRow, int CursorRow, int *RowCount, int ShowSize);
// Display directory set page

static void SetCursorPosition( TFdirHandle Handle, int *CurrentPage, int *CurrentItem);
// Searches for the <Handle>, returns <CurrentPage>, <CurrentItem>

static TYesNo NextPage( int *CurrentPage, int *RowCount);
// Move at next page

static TYesNo PreviousPage( int *CurrentPage, int *RowCount);
// Move at previous page

static void HandleToIndex( TFDataSet CheckSet, TFDataSet DataSet);
// recalculate data set to directory indexes

static void IndexToHandle( TFDataSet DataSet, TFDataSet CheckSet);
// recalculate data set to directory indexes

//------------------------------------------------------------------------------
//  Select file
//------------------------------------------------------------------------------

TFdirHandle DDirSelectFile( TUniStr Caption, TFdirHandle Handle)
// Select file from directory. Set cursor to <Handle>
{
int Index;

   if( !FdCount()){
      DMsgOk( STR_ERROR, STR_NO_FILES_DEFINED, 0);
      return( FDIR_INVALID);
   }
   GClear();
   DLayoutTitle( Caption);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   Index = DisplaySelection( Handle, YES);
   if( Index < 0){
      return( FDIR_INVALID);           // escape
   }
   return( FdGet( Index));
} // DDirSelectFile

//------------------------------------------------------------------------------
//  Select group
//------------------------------------------------------------------------------

TFdirHandle DDirSelectGroup( TUniStr Caption, TFdirHandle Handle)
// Select group from directory. Set cursor to <Handle>
{
int Index;

   if( !FdCount()){
      DMsgOk( STR_ERROR, STR_NO_GROUPS_DEFINED, 0);
      return( FDIR_INVALID);
   }
   GClear();
   DLayoutTitle( Caption);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   Index = DisplaySelection( Handle, NO);
   if( Index < 0){
      return( FDIR_INVALID);           // escape
   }
   return( FdGet( Index));
} // DDirSelectGroup

//------------------------------------------------------------------------------
//  Select set
//------------------------------------------------------------------------------

TYesNo DDirSelectSet( TUniStr Caption, TFDataSet DataSet)
// Select file set from directory
{
TFDataSet CheckSet;          // directory checkboxes

   if( !FdCount()){
      DMsgOk( STR_ERROR, STR_NO_FILES_DEFINED, 0);
      return( NO);
   }
   GClear();
   DLayoutTitle( Caption);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   HandleToIndex( CheckSet, DataSet);  // recalculate data set to directory indexes
   if( !DisplaySetSelection( CheckSet)){
      return( NO);                     // escape
   }
   IndexToHandle( DataSet, CheckSet);  // recalculate to handle set
   return( YES);
} // DDirSelectSet

//------------------------------------------------------------------------------
//  Select file/group
//------------------------------------------------------------------------------

static int DisplaySelection( TFdirHandle Handle, int ShowSize)
// Display and edit, retuns index
{
int RowCount;                // rows count
int CurrentItem;             // active row
int LastItem;                // last active row
int CurrentPage;             // start of page offset
int LastPage;                // last start of page offset
int Index;                   // temporary index

   CurrentItem =  0;                   // first row
   CurrentPage =  0;                   // first page
   if( Handle != FDIR_INVALID){
      // update cursor position
      SetCursorPosition( Handle, &CurrentPage, &CurrentItem);
   }
   LastItem    = -1;                   // force redraw
   LastPage    = -1;                   // force redraw
   forever {
      if( CurrentPage != LastPage || CurrentItem != LastItem){
         // redraw page
         if( CurrentPage != LastPage){
            LastItem = -1;             // redraw all menu
         }
         DisplayPage( 0, CurrentPage, LastItem, CurrentItem, &RowCount, ShowSize);
         LastPage = CurrentPage;       // remember last
         LastItem = CurrentItem;       // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CurrentItem > 0){
               BeepKey();
               CurrentItem--;
               break;
            }
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = RowCount - 1;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CurrentItem < RowCount - 1){
               BeepKey();
               CurrentItem++;
               break;
            }
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = 0;
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            if( CurrentItem >= RowCount){
               CurrentItem = RowCount - 1;       // short page - move cursor at last item
            }
            break;

         case K_ENTER :
            BeepKey();
            Index = CurrentPage + CurrentItem;   // actual index
            return( Index);                      // file selection

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( -1);
      }
   }
} // DisplaySelection

//------------------------------------------------------------------------------
//  Select file set
//------------------------------------------------------------------------------

static TYesNo DisplaySetSelection( TFDataSet CheckSet)
// Display and edit set. Returns NO on Esc
{
int RowCount;                // rows count
int CurrentItem;             // active row
int LastItem;                // last active row
int CurrentPage;             // start of page offset
int LastPage;                // last start of page offset
int Index;                   // temporary index

   CurrentItem =  0;                   // first row
   CurrentPage =  0;                   // first page
   LastItem    = -1;                   // force redraw
   LastPage    = -1;                   // force redraw
   forever {
      if( CurrentPage != LastPage || CurrentItem != LastItem){
         // redraw page
         if( CurrentPage != LastPage){
            LastItem = -1;             // redraw all menu
         }
         DisplayPage( CheckSet, CurrentPage, LastItem, CurrentItem, &RowCount, NO);
         LastPage = CurrentPage;       // remember last
         LastItem = CurrentItem;       // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CurrentItem > 0){
               BeepKey();
               CurrentItem--;
               break;
            }
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = RowCount - 1;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CurrentItem < RowCount - 1){
               BeepKey();
               CurrentItem++;
               break;
            }
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = 0;
            break;

         case K_RIGHT :
            // file set - toggle selection
            if( !CheckSet){
               break;
            }
            BeepKey();
            Index = CurrentPage + CurrentItem;   // actual index
            if( DsContains( CheckSet, Index)){
               DsRemove( CheckSet, Index);
            } else {
               DsAdd(    CheckSet, Index);
            }
            LastPage = -1;                       // force redraw
            break;

         case K_ENTER :
            BeepKey();
            Index = CurrentPage + CurrentItem;   // actual index
            return( YES);                        // set selection

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DisplaySetSelection

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

static void DisplayPage( TFDataSet CheckSet, int CurrentPage, int LastCursorRow, int CursorRow, int *RowCount, int ShowSize)
// Display directory page
{
int  Count;
TFdirHandle Fd;
char Name[ FDIR_NAME_LENGTH + 1];
int  i;
int  RowY;
int  FileSize;

   SetFont( DDIR_FONT);
   if( LastCursorRow == -1){
      // clear data area
      GSetColor( DCOLOR_BACKGROUND);
      GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
      GSetColor( DCOLOR_DEFAULT);
   }
   // draw files
   Count      = 0;
   FdMoveAt( CurrentPage);
   i = CurrentPage;
   while( (Fd = FdFindNext()) != FDIR_INVALID){
      if( Count >= DDIR_COUNT){
         // end of page
         *RowCount = Count;
         GSetColor( DCOLOR_DEFAULT);
         return;
      }
      RowY = DDIR_TEXT_Y + Count * DDIR_ITEM_HEIGHT;
      if( LastCursorRow != -1){
         // draw partialy
         if( Count != LastCursorRow && Count != CursorRow){
            // dont't redraw other rows
            i++;
            Count++;
            continue;
         }
         // erase row
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 0, RowY, G_WIDTH, DDIR_ITEM_HEIGHT);
         GSetColor( DCOLOR_DEFAULT);
      }
      // draw cursor
      if( Count == CursorRow){
         GSetColor( DCOLOR_CURSOR);
         GBoxRound( 0, RowY, G_WIDTH, DDIR_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         GSetColor( DCOLOR_BACKGROUND);
         // draw cursor arrow
         if( CheckSet){
            GBitmap( 225, RowY + 4, &BmpFileArrow);
         }
      } else {
         GSetColor( DCOLOR_DEFAULT);
      }
      // draw text
      GTextAt( 5, RowY);
      if( CheckSet){
         // display checkbox
         if( DsContains( CheckSet, i)){
            GBitmap( 5, RowY + 3, &BmpFileChecked);
         } else {
            GBitmap( 5, RowY + 3, &BmpFileUnchecked);
         }
         GTextAt( 25, RowY);
      }
      FdGetName( Fd, Name);
      cputs( Name);
      // draw file size
      if( ShowSize){
         FileSize  = SdbFileSize( FdGetSize( Fd));
         DLabelFormat( DMENU_PARAMETERS_X, RowY, "%d", FileSize);
      }
      Count++;
      i++;
   }
   *RowCount = Count;
} // DisplayPage

//------------------------------------------------------------------------------
//  Cursor position
//------------------------------------------------------------------------------

static void SetCursorPosition( TFdirHandle Handle, int *CurrentPage, int *CurrentItem)
// Searches for the <Handle>, returns <CurrentPage>, <CurrentItem>
{
int         Position;
TFdirHandle Fd;

   if( !FdCount()){
      return;                          // directory empty
   }
   Position = 0;
   FdFindBegin();
   while( (Fd = FdFindNext()) != FDIR_INVALID){
      if( Fd == Handle){
         break;
      }
      Position++;
   }
   if( Fd == FDIR_INVALID){
      return;                          // not found
   }
   // find page & cursor offset :
   *CurrentPage  = Position / DDIR_COUNT;        // page number
   *CurrentPage *= DDIR_COUNT;                   // page start row
   *CurrentItem  = Position % DDIR_COUNT;
} // SetCursorPosition

//------------------------------------------------------------------------------
//  Next page
//------------------------------------------------------------------------------

static TYesNo NextPage( int *CurrentPage, int *RowCount)
// Move at next page
{
int TotalCount;
int NextCount;

   TotalCount = FdCount();
   if( *CurrentPage + DDIR_COUNT >= TotalCount){
      return( NO);                     // last visible page
   }
   *CurrentPage += DDIR_COUNT;         // move to next page
   NextCount    = TotalCount - *CurrentPage;
   if( NextCount >= DDIR_COUNT){
      *RowCount = DDIR_COUNT;          // whole page
   } else {
      *RowCount = NextCount;           // short page
   }
   return( YES);
} // NextPage

//------------------------------------------------------------------------------
//  Previous page
//------------------------------------------------------------------------------

static TYesNo PreviousPage( int *CurrentPage, int *RowCount)
// Move at previous page
{
int TotalCount;

   if( *CurrentPage == 0){
      return( NO);
   }
   TotalCount = FdCount();
   if( *CurrentPage < DDIR_COUNT){
      *CurrentPage  = 0;
   } else {
      *CurrentPage -= DDIR_COUNT;
   }
   if( TotalCount < DDIR_COUNT){
      *RowCount = TotalCount;
   } else {
      *RowCount = DDIR_COUNT;
   }
   return( YES);
} // PreviousPage

//------------------------------------------------------------------------------
//  Handle to index
//------------------------------------------------------------------------------

static void HandleToIndex( TFDataSet CheckSet, TFDataSet DataSet)
// recalculate data set to directory indexes
{
int         i;
TFdirHandle Handle;

   DsClear( CheckSet);
   i = 0;
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( DsContains( DataSet, Handle)){
         DsAdd( CheckSet, i);
      }
      i++;
      WatchDog();
   }
} // HandleToIndex

//------------------------------------------------------------------------------
//  Index to handle
//------------------------------------------------------------------------------

static void IndexToHandle( TFDataSet DataSet, TFDataSet CheckSet)
// recalculate data set to directory indexes
{
int         i;
TFdirHandle Handle;

   DsClear( DataSet);
   i = 0;
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( DsContains( CheckSet, i)){
         DsAdd( DataSet, Handle);
      }
      i++;
      WatchDog();
   }
} // IndexToHandle
