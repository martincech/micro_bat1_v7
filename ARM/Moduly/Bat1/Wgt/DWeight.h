//******************************************************************************
//                                                                            
//   DWeight.h      Display weight
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DWeight_H__
   #define __DWeight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

#ifndef __SdbDef_H__
   #include "SdbDef.h"       // sample flag only
#endif

#ifndef __StrDef_H__
   #include "../Inc/Wgt/StrDef.h"
#endif

#define DWEIGHT_MAX_LENGTH   12      // max. size of weight string with units

void DWeight( TWeight Weight);
// Display weight

void DWeightLeft( TWeight Weight);
// Display weight left aligned

void DWeighingFlag( TSampleFlag Flag);
// Display weighing flag

void DWeighingUnits( void);
// Display current units

void DWeightWithUnits( TWeight Weight);
// Display weight with units

void DWeightWithUnitsNarrow( int x, int y, TWeight Weight);
// Display weight with units left aligned

void DWeightNarrow( int x, int y, TWeight Weight);
// Display weight with narrow decimal dot

void DWeightWithUnitsFormat( char *Buffer, TWeight Weight);
// Format <Weight> with units to <Buffer>

TYesNo DInputWeight( TUniStr Caption, TUniStr Text, int *Value, int LoLimit, int HiLimit);
// Input weight

TYesNo DEditWeight( int x, int y, int *Value, int LoLimit, int HiLimit);
// Edit weight

int DWeightWidth( void);
// Returns weight width (pixels)

int DWeightWidthNarrow( void);
// Returns weight width with narrow decimal dot (pixels)

#endif
