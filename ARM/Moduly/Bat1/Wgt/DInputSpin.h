//******************************************************************************
//                                                                            
//   DInputSpin.h   Input dialog for spinner
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DInputSpin_H__
   #define __DInputSpin_H__

#ifndef __DEnterList_H__
   #include "../../Inc/Wgt/DEnterList.h"
#endif

#ifndef __DCallback_H__
   #include "../../Inc/Wgt/DCallback.h"
#endif


TYesNo DInputSpin( TUniStr Caption, TUniStr Text, int *Value, 
                   int MinValue, int MaxValue, TAction *OnChange);
// Input value by spinner

#endif
