//*****************************************************************************
//
//    GLogo.h       Graphic EEPROM logo
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __GLogo_H__
   #define __GLogo_H__

void GLogo( void);
// Display logo by EEPROM

#endif
