//******************************************************************************
//
//  Str.c          Strings translations
//  Version 1.0    (c) Robot
//
//******************************************************************************

#include "Str.h"

//------------------------------------------------------------------------------
//   String definitions
//------------------------------------------------------------------------------

TAllStrings AllStrings = {
// system :
/* STR_NULL */
{
   "",
   "",
   "",
   "",
   "",
   "",
   "",
   "",
   "",
   "",
   "",
   "",
   ""
},


//-----------------------------------------------------------------------------
// Buttons
//-----------------------------------------------------------------------------

/* STR_BTN_OK           */ 
{
   "OK",
   "OK",
   "OK",
   "OK",
   "OK",
   "OK",
   "OK",
   "OK",
   "OK",
   "OK",
   "O\317",
   "OK",
   "TAMAM"
},
/* STR_BTN_YES          */ 
{
   "ANO",
   "JA",
   "YES",
   "KYLL\342",
   "OUI",
   "JA",
   "IGEN",
   "\221\006",
   "TAK",
   "SIM",
   "\312A",
   "S\224",
   "EVET"
},
/* STR_BTN_NO           */ 
{
   "NE",
   "NEE",
   "NO",
   "EI",
   "NON",
   "NEIN",
   "NEM",
   "\006\006\013",
   "NIE",
   "N\002O",
   "HET ",
   "NO",
   "HAYIR"
},
/* STR_BTN_CANCEL       */ 
{
   "ZRU\230IT",
   "ANNULEER",
   "CANCEL",
   "PERUUTA",
   "ANNULER",
   "ABBRUCH",
   "VISSZA",
   "\020\245\265\036\255",
   "ANULUJ",
   "CANCELAR",
   "OTMEHA",
   "CANCELAR",
   "VAZGEC"
},
/* STR_BTN_HISTOGRAM    */ 
{
   "HISTOGRAM",
   "HISTOGRAM",
   "HISTOGRAM",
   "HISTOGRAMMI",
   "HISTOGRAMME",
   "HISTOGRAMM",
   "GRAFIKON",
   "\224\034\212\023\253\242",
   "HISTOGRAM",
   "HISTOGRAMA",
   "\311PA\323\315\317",
   "HISTOGRAMA",
   "GRAFIK"
},
/* STR_BTN_STATISTICS   */ 
{
   "STATISTIKA",
   "STATISTIEKEN",
   "STATISTICS",
   "TILASTOT",
   "STATISTIQUES",
   "STATISTIK",
   "STATISZTIKA",
   "\212\010\024\006",
   "STATYSTYKI",
   "ESTAT\224STICA",
   "CTAT\315CT\315\317A ",
   "ESTAD\224STICAS",
   "ISTATISTIKLER"
},
/* STR_BTN_DELETE       */ 
{
   "SMAZAT",
   "VERWIJDEREN",
   "DELETE",
   "POISTA",
   "EFFACER",
   "L\343SCHEN",
   "T\343RL\222S",
   "\030\022\033\251",
   "USU\252",
   "DELETAR",
   "Y\312A\320\315T\332",
   "ELIMINAR",
   "SIL"
},
/* STR_BTN_EXIT         */ 
{
   "ZAV\227\224T",
   "AFSLUITEN",
   "EXIT",
   "LOPETA",
   "SORTIE",
   "VERLASSEN",
   "KIL\222P",
   "\212\033\255",
   "WYJD\254",
   "SAIR",
   "B\331XO\312",
   "SALIDA",
   "CIKIS"
},
/* STR_BTN_SELECT       */ 
{
   "VYBRAT",
   "SELECTEREN",
   "SELECT",
   "VALITSE",
   "CHOISIR",
   "AUSWAHL",
   "KIV\217LASZT",
   "\036\265\201\022\034\255",
   "WYBIERZ",
   "SELECIONAR",
   "B\331\310OP",
   "SELECCIONAR",
   "SEC"
},

//-----------------------------------------------------------------------------
// Standard title :
//-----------------------------------------------------------------------------

/* STR_ERROR            */ 
{
   "Chyba",
   "Fout",
   "Error",
   "Virhe",
   "Erreur",
   "Fehler",
   "Hiba",
   "\013\253\276",
   "B\241\236d",
   "Erro",
   "O\300\264\256\266a",
   "Error",
   "Hata"
},

//-----------------------------------------------------------------------------
// Input box :
//-----------------------------------------------------------------------------

/* STR_OUT_OF_LIMITS    */ 
{
   "Hodnota mimo rozsah",
   "Invoer te groot. Aub inkorten",
   "Value out of limits",
   "Arvo ylitt\336\336 rajat",
   "Valeur hors limite",
   "Bereichs\340berschreitung",
   "Hat\200ron k\205v\340li \203rt\203k",
   "\254\241\205\212\264\026\013\201\004\201\006",
   "Warto\243\237 poza limitem",
   "Valor fora dos limites",
   "O\260pa\271\264\277e\271\264e \257eca",
   "Valor fuera de l\205mites",
   "Deger limitler disinda"
},

//-----------------------------------------------------------------------------
// Directory box :
//-----------------------------------------------------------------------------

/* STR_NO_FILES_DEFINED */ 
{
   "Nen\205 definov\200n \216\200dn\215 soubor",
   "Geen bestanden in weegschaal",
   "No files defined",
   "Tiedostoaja ei ole m\336\336ritelty",
   "Pas de fichiers d\203finis",
   "Keine Dateien definiert",
   "Nincs adat v\200lasztva",
   "\227\003\006\255\017\210\006\021\030\256\210\006\240\036\265",
   "Brak zdefiniowanych plik\207w",
   "Nenhum arquivo selecionado",
   "He\273 \261a\271\271o\260o \275a\265\267a",
   "Archivos no especificados",
   "Tanimlanmis dosya yok"
},
/* STR_NO_GROUPS_DEFINED */ 
{
   "Nen\205 definov\200na \216\200dn\200 skupina",
   "Geen bestandsgroepen\nin weegschaal",
   "No groups defined",
   "Ryhmi\336 ei ole m\336\336ritelty",
   "Pas de groupes d\203finis",
   "Keine Gruppen definiert",
   "Nincs csoport v\200lasztva",
   "\023\255\276\231\017\210\006\021\030\256\210\006\240\036\265",
   "Brak zdefiniowanych grup",
   "Nennhum grupo selicionado",
   "He\273 \261a\271\271o\265 \260p\274\272\272",
   "Grupos no especificados",
   "Tanimlanmis grup yok"
},
/* STR_SELECT_FILE      */ 
{
   "V\215b\204r souboru",
   "Selecteer bestand",
   "Select file",
   "Valitse tiedosto",
   "Choisir un fichier",
   "Datei ausw\336hlen",
   "Adat v\200laszt\200s",
   "\227\003\006\255\264\036\265\201\022",
   "Wybierz plik",
   "Selecionar arquivo",
   "B\303\256pa\273\304 \275a\265\267",
   "Seleccionar un archivo",
   "Dosya sec"
},
/* STR_SELECT_GROUP     */ 
{
   "V\215b\204r skupiny",
   "Selecteer groep",
   "Select group",
   "Valitse ryhm\336",
   "Choisir un dossier",
   "Gruppe ausw\336hlen",
   "Csoport v\200laszt\200sa",
   "\023\255\276\231\264\036\265\201\022",
   "Wybierz grup\240",
   "Selecionar  grupo",
   "B\303\256pa\273\304 \260p\274\272\272\274",
   "Seleccionar un grupo",
   "Grup sec"
},
/* STR_SELECT_FILES     */ 
{
   "V\215b\204r soubor\214",
   "Selecteer bestanden",
   "Select files",
   "Valitse tiedostot",
   "Choisir des fichiers",
   "Dateien ausw\336hlen",
   "T\337bb adat kiv\200laszt\200sa",
   "\227\003\006\255\264\036\265\201\022",
   "Wybierz pliki",
   "Selecionar arquivos",
   "B\303\256pa\273\304 \275a\265\267\303",
   "Seleccionar archivos",
   "Dosyalari sec"
},

//-----------------------------------------------------------------------------
// Text box :
//-----------------------------------------------------------------------------

/* STR_STRING_EMPTY     */ 
{
   "Neplatn\200 hodnota",
   "Geen geldige waarde",
   "Invalid value",
   "Virheellinen arvo",
   "Valeur invalide",
   "Ung\340ltiger Wert",
   "\222rv\203nytelen mennyis\203g",
   "\242\026\010\214\034\010\203",
   "Nieprawid\241owa warto\243\237",
   "Valor inv\200lido",
   "He \261e\265c\273\257\264\273e\267\304\271\303\265 \257ec",
   "Valor inv\200lido",
   "Gecersiz deger"
},

//-----------------------------------------------------------------------------
// DSamples box :
//-----------------------------------------------------------------------------

/* STR_DELETE_SAMPLE    */ 
{
   "Smazat hmotnost",
   "Verwijder gewicht",
   "Delete weight",
   "Poista punnitus",
   "Effacer la pes\203e",
   "Gewichtswert l\337schen",
   "S\213ly t\337rl\203se",
   "\201\006\033\247\010\264\030\022\033\251",
   "Usu\242 dane wa\245e\242",
   "Deletar peso",
   "Y\261a\267\264\273\304 \257ec",
   "Eliminar el peso",
   "Tartimi sil"
},
/* STR_REALLY_DELETE_RECORD */ 
{
   "Opravdu smazat hmotnost ?",
   "Gewicht echt verwijderen?",
   "Really delete weight ?",
   "Haluatko varmasti poistaa\npunnituksen?",
   "Voulez-vous vraiment supprimer la pes\203e?",
   "Wirklich l\337schen?",
   "T\203nyleg t\337rli a s\213lyt?",
   "\235\265\212\010\215\201\006\033\247\010\n\264\030\022\033\251\032\240\034\016?",
   "Czy na pewno chcesz\nusun\236\237 dane wa\245e\242 ?",
   "Realmente deletar peso?",
   "Y\261a\267\264\273\304 \257ec ?",
   "\360Realmente eliminar el peso?",
   "Tartim gercekten silinsin mi?"
},

//-----------------------------------------------------------------------------
// Physical units :
//-----------------------------------------------------------------------------

/* STR_MINUTES          */ 
{
   "min",
   "min",
   "min",
   "min",
   "min",
   "min",
   "perc",
   "\227\265",
   "min.",
   "min",
   "M\264\271",
   "m",
   "dak."
},
/* STR_SECONDS          */ 
{
   "s",
   "s",
   "s",
   "s",
   "s",
   "s",
   "mp",
   "\225\251\010",
   "sek.",
   "s",
   "Ce\266",
   "s",
   "sn."
},

//-----------------------------------------------------------------------------
// Yes/No enum :
//-----------------------------------------------------------------------------

/* STR_NO               */ 
{
   "Ne",
   "Nee",
   "No",
   "Ei",
   "Non",
   "Nein",
   "Nem",
   "\006\006\013",
   "Nie",
   "N\023o",
   "He\273",
   "No",
   "Hayir"
},
/* STR_YES              */ 
{
   "Ano",
   "Ja",
   "Yes",
   "Kyll\336",
   "Oui",
   "Ja",
   "Igen",
   "\221\006",
   "Tak",
   "Sim",
   "\312a",
   "S\205",
   "Evet"
},

//-----------------------------------------------------------------------------
// Country enum :
//-----------------------------------------------------------------------------

/* STR_COUNTRY_INTERNATIONAL */ 
{
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International",
   "International"
},
/* STR_COUNTRY_ALBANIA  */ 
{
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania",
   "Albania"
},
/* STR_COUNTRY_ALGERIA  */ 
{
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria",
   "Algeria"
},
/* STR_COUNTRY_ARGENTINA */ 
{
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina",
   "Argentina"
},
/* STR_COUNTRY_AUSTRALIA */ 
{
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia",
   "Australia"
},
/* STR_COUNTRY_AUSTRIA  */ 
{
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria",
   "Austria"
},
/* STR_COUNTRY_BANGLADESH */ 
{
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh",
   "Bangladesh"
},
/* STR_COUNTRY_BELARUS  */ 
{
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus",
   "Belarus"
},
/* STR_COUNTRY_BELGIUM  */ 
{
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium",
   "Belgium"
},
/* STR_COUNTRY_BOLIVIA  */ 
{
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia",
   "Bolivia"
},
/* STR_COUNTRY_BRAZIL   */ 
{
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil",
   "Brazil"
},
/* STR_COUNTRY_BULGARIA */ 
{
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria",
   "Bulgaria"
},
/* STR_COUNTRY_CANADA   */ 
{
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada",
   "Canada"
},
/* STR_COUNTRY_CHILE    */ 
{
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile",
   "Chile"
},
/* STR_COUNTRY_CHINA    */ 
{
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China",
   "China"
},
/* STR_COUNTRY_COLOMBIA */ 
{
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia",
   "Colombia"
},
/* STR_COUNTRY_CYPRUS   */ 
{
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus",
   "Cyprus"
},
/* STR_COUNTRY_CZECH    */ 
{
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech"
},
/* STR_COUNTRY_DENMARK  */ 
{
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark",
   "Denmark"
},
/* STR_COUNTRY_ECUADOR  */ 
{
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador",
   "Ecuador"
},
/* STR_COUNTRY_EGYPT    */ 
{
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt",
   "Egypt"
},
/* STR_COUNTRY_ESTONIA  */ 
{
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia",
   "Estonia"
},
/* STR_COUNTRY_FINLAND  */ 
{
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland",
   "Finland"
},
/* STR_COUNTRY_FRANCE   */ 
{
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France",
   "France"
},
/* STR_COUNTRY_GERMANY  */ 
{
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany",
   "Germany"
},
/* STR_COUNTRY_GREECE   */ 
{
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece",
   "Greece"
},
/* STR_COUNTRY_HUNGARY  */ 
{
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary",
   "Hungary"
},
/* STR_COUNTRY_INDIA    */ 
{
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India",
   "India"
},
/* STR_COUNTRY_INDONESIA */ 
{
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia",
   "Indonesia"
},
/* STR_COUNTRY_IRAN     */ 
{
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran",
   "Iran"
},
/* STR_COUNTRY_IRELAND  */ 
{
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland",
   "Ireland"
},
/* STR_COUNTRY_ISRAEL   */ 
{
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel",
   "Israel"
},
/* STR_COUNTRY_ITALY    */ 
{
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy",
   "Italy"
},
/* STR_COUNTRY_JAPAN    */ 
{
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan",
   "Japan"
},
/* STR_COUNTRY_JORDAN   */ 
{
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan",
   "Jordan"
},
/* STR_COUNTRY_LATVIA   */ 
{
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia",
   "Latvia"
},
/* STR_COUNTRY_LEBANON  */ 
{
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon",
   "Lebanon"
},
/* STR_COUNTRY_LITHUANIA */ 
{
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania",
   "Lithuania"
},
/* STR_COUNTRY_LUXEMBOURG */ 
{
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg",
   "Luxembourg"
},
/* STR_COUNTRY_MALAYSIA */ 
{
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia",
   "Malaysia"
},
/* STR_COUNTRY_MALTA    */ 
{
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta",
   "Malta"
},
/* STR_COUNTRY_MEXICO   */ 
{
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico",
   "Mexico"
},
/* STR_COUNTRY_MONGOLIA */ 
{
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia",
   "Mongolia"
},
/* STR_COUNTRY_MOROCCO  */ 
{
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco",
   "Morocco"
},
/* STR_COUNTRY_NEPAL    */ 
{
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal",
   "Nepal"
},
/* STR_COUNTRY_NETHERLANDS */ 
{
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland",
   "Netherland"
},
/* STR_COUNTRY_NEW_ZEALAND */ 
{
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand",
   "New Zealand"
},
/* STR_COUNTRY_NIGERIA  */ 
{
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria",
   "Nigeria"
},
/* STR_COUNTRY_NORWAY   */ 
{
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway",
   "Norway"
},
/* STR_COUNTRY_PAKISTAN */ 
{
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan",
   "Pakistan"
},
/* STR_COUNTRY_PARAGUAY */ 
{
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay",
   "Paraguay"
},
/* STR_COUNTRY_PERU     */ 
{
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru",
   "Peru"
},
/* STR_COUNTRY_PHILIPPINES */ 
{
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines",
   "Philippines"
},
/* STR_COUNTRY_POLAND   */ 
{
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland",
   "Poland"
},
/* STR_COUNTRY_PORTUGAL */ 
{
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal",
   "Portugal"
},
/* STR_COUNTRY_ROMANIA  */ 
{
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania",
   "Romania"
},
/* STR_COUNTRY_RUSSIA   */ 
{
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia",
   "Russia"
},
/* STR_COUNTRY_SLOVAKIA */ 
{
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia",
   "Slovakia"
},
/* STR_COUNTRY_SLOVENIA */ 
{
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia",
   "Slovenia"
},
/* STR_COUNTRY_SOUTH_AFRICA */ 
{
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa",
   "South Africa"
},
/* STR_COUNTRY_SOUTH_KOREA */ 
{
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea",
   "Korea"
},
/* STR_COUNTRY_SPAIN    */ 
{
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain",
   "Spain"
},
/* STR_COUNTRY_SWEDEN   */ 
{
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden",
   "Sweden"
},
/* STR_COUNTRY_SWITZERLAND */ 
{
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland",
   "Switzerland"
},
/* STR_COUNTRY_SYRIA    */ 
{
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria",
   "Syria"
},
/* STR_COUNTRY_THAILAND */ 
{
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand",
   "Thailand"
},
/* STR_COUNTRY_TUNISIA  */ 
{
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia",
   "Tunisia"
},
/* STR_COUNTRY_TURKEY   */ 
{
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey",
   "Turkey"
},
/* STR_COUNTRY_UKRAINE  */ 
{
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine",
   "Ukraine"
},
/* STR_COUNTRY_UK       */ 
{
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK",
   "UK"
},
/* STR_COUNTRY_USA      */ 
{
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA",
   "USA"
},
/* STR_COUNTRY_URUGUAY  */ 
{
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay",
   "Uruguay"
},
/* STR_COUNTRY_VENEZUELA */ 
{
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela",
   "Venezuela"
},
/* STR_COUNTRY_VIETNAM  */ 
{
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam",
   "Vietnam"
},

//-----------------------------------------------------------------------------
// Language enum :
//-----------------------------------------------------------------------------

/* STR_LNG_CZECH        */ 
{
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech",
   "Czech"
},
/* STR_LNG_DUTCH        */ 
{
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch",
   "Dutch"
},
/* STR_LNG_ENGLISH      */ 
{
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English",
   "English"
},
/* STR_LNG_FINNISH      */ 
{
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish",
   "Finnish"
},
/* STR_LNG_FRENCH       */ 
{
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French",
   "French"
},
/* STR_LNG_GERMAN       */ 
{
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German",
   "German"
},
/* STR_LNG_HUNGARIAN    */ 
{
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian",
   "Hungarian"
},
/* STR_LNG_JAPANESE     */ 
{
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese",
   "Japanese"
},
/* STR_LNG_POLISH       */ 
{
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish",
   "Polish"
},
/* STR_LNG_PORTUGUESE   */ 
{
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese",
   "Portugese"
},
/* STR_LNG_RUSSIAN      */ 
{
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian",
   "Russian"
},
/* STR_LNG_SPANISH      */ 
{
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish",
   "Spanish"
},
/* STR_LNG_TURKISH      */ 
{
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish",
   "Turkish"
},

//-----------------------------------------------------------------------------
// Date format enum :
//-----------------------------------------------------------------------------

/* STR_DATE_FORMAT_DDMMYYYY */ 
{
   "DD MM RRRR",
   "DD MM JJJJ",
   "DD MM YYYY",
   "PP KK VVVV",
   "JJ MM AAAA",
   "TT MM JJJJ",
   "Nap H\207nap \222v",
   "\224/\206\020/\217\265",
   "DD MM RRRR",
   "DD MM AAAA",
   "\312\312 MM \311\311\311\311",
   "DD MM AAAA",
   "GG AA YYYY"
},
/* STR_DATE_FORMAT_MMDDYYYY */ 
{
   "MM DD RRRR",
   "MM DD JJJJ",
   "MM DD YYYY",
   "KK PP VVVV",
   "MM JJ AAAA",
   "MM TT JJJJ",
   "H\207nap Nap \222v",
   "\206\020/\224/\217\265",
   "MM DD RRRR",
   "MM DD AAAA",
   "MM \312\312 \311\311\311\311",
   "MM DD AAAA",
   "AA GG YYYY"
},
/* STR_DATE_FORMAT_YYYYMMDD */ 
{
   "RRRR MM DD",
   "JJJJ MM DD",
   "YYYY MM DD",
   "VVVV KK PP",
   "AAAA MM JJ",
   "JJJJ MM TT",
   "\222v H\207nap Nap",
   "\217\265/\206\020/\224",
   "RRRR MM DD",
   "AAAA MM DD",
   "\311\311\311\311 MM \312\312",
   "AAAA MM DD",
   "YYYY AA GG"
},
/* STR_DATE_FORMAT_YYYYDDMM */ 
{
   "RRRR DD MM",
   "JJJJ DD MM",
   "YYYY DD MM",
   "VVVV PP KK",
   "AAAA JJ MM",
   "JJJJ TT MM",
   "\222v Nap H\207nap",
   "\217\265/\224/\206\020",
   "RRRR DD MM",
   "AAAA DD MM",
   "\311\311\311\311 \312\312 MM",
   "AAAA DD MM",
   "YYYY GG AA"
},
/* STR_DATE_FORMAT_DDMMMYYYY */ 
{
   "DD Mmm RRRR",
   "DD Mmm JJJJ",
   "DD Mmm YYYY",
   "PP Kkk VVVV",
   "JJ Mmm AAAA",
   "TT Mmm JJJJ",
   "Nap H\207napn\203v \222v",
   "\224/\206\020/\217\265",
   "DD Mmm RRRR",
   "DD Mmm AAAA",
   "\312\312 M\270\270 \311\311\311\311",
   "DD Mmm AAAA",
   "GG Aaa YYYY"
},
/* STR_DATE_FORMAT_MMMDDYYYY */ 
{
   "Mmm DD RRRR",
   "Mmm DD JJJJ",
   "Mmm DD YYYY",
   "Kkk PP VVVV",
   "Mmm JJ AAAA",
   "Mmm TT JJJJ",
   "H\207napn\203v Nap \222v",
   "\206\020/\224/\217\265",
   "Mmm DD RRRR",
   "Mmm DD AAAA",
   "M\270\270 \312\312 \311\311\311\311",
   "Mmm DD AAAA",
   "Aaa GG YYYY"
},
/* STR_DATE_FORMAT_YYYYMMMDD */ 
{
   "RRRR Mmm DD",
   "JJJJ Mmm DD",
   "YYYY Mmm DD",
   "VVVV Kkk PP",
   "AAAA Mmm JJ",
   "JJJJ Mmm TT",
   "\222v H\207napn\203v Nap",
   "\217\265/\206\020/\224",
   "RRRR Mmm DD",
   "AAAA Mmm DD",
   "\311\311\311\311 M\270\270 \312\312",
   "AAAA Mmm DD",
   "YYYY Aaa GG"
},
/* STR_DATE_FORMAT_YYYYDDMMM */ 
{
   "RRRR DD Mmm",
   "JJJJ DD Mmm",
   "YYYY DD Mmm",
   "VVVV PP Kkk",
   "AAAA JJ Mmm",
   "JJJJ TT Mmm",
   "\222v Nap H\207napn\203v",
   "\217\265/\224/\206\020",
   "RRRR DD Mmm",
   "AAAA DD Mmm",
   "\311\311\311\311 \312\312 M\270\270",
   "AAAA DD Mmm",
   "YYYY GG Aaa"
},

//-----------------------------------------------------------------------------
// Time format enum :
//-----------------------------------------------------------------------------

/* STR_TIME_FORMAT_24   */ 
{
   "24 hodin",
   "24 uur",
   "24 hours",
   "24 h",
   "24 heures",
   "24 Stunden",
   "24 \207r\200s",
   "24\033\016\265\224\251\010\020",
   "24 godzinny",
   "24 horas",
   "24 \277aca",
   "24 horas",
   "24 saat"
},
/* STR_TIME_FORMAT_12   */ 
{
   "12 hodin",
   "12 uur",
   "12 hours",
   "12 h",
   "12 heures",
   "12 Stunden",
   "12 \207r\200s",
   "12\033\016\265\224\251\010\020",
   "12 godzinny",
   "12 horas",
   "12 \277aco\257",
   "12 horas",
   "12 saat"
},

//-----------------------------------------------------------------------------
// Daylight saving time enum :
//-----------------------------------------------------------------------------

/* STR_DST_TYPE_OFF     */ 
{
   "Vypnuto",
   "Uit",
   "Off",
   "Pois",
   "Off",
   "Aus",
   "Kikapcsol",
   "\015\227",
   "Wy\241.",
   "Delisgar",
   "B\303\266\267",
   "Apagar",
   "Kapal\361"
},
/* STR_DST_TYPE_EU      */ 
{
   "EU",
   "EU",
   "EU",
   "EU",
   "EU",
   "EU",
   "EU",
   "EU",
   "EU",
   "EU",
   "E\257po\272a",
   "EU",
   "Avrupa saati"
},
/* STR_DST_TYPE_US      */ 
{
   "US",
   "US",
   "US",
   "US",
   "US",
   "US",
   "USA",
   "\004\243\254\016",
   "US",
   "US",
   "C\326A",
   "USA",
   "Amerika saati"
},

//-----------------------------------------------------------------------------
// Units enum :
//-----------------------------------------------------------------------------

/* STR_UNITS_KG         */ 
{
   "kg",
   "kg",
   "kg",
   "kg",
   "kg",
   "kg",
   "kg",
   "kg",
   "kg",
   "kg",
   "\317\260",
   "kg",
   "kg"
},
/* STR_UNITS_G          */ 
{
   "g",
   "g",
   "g",
   "g",
   "g",
   "g",
   "g",
   "g",
   "g",
   "g",
   "\311p",
   "g",
   "gr"
},
/* STR_UNITS_LB         */ 
{
   "lb",
   "lb",
   "lb",
   "lb",
   "lb",
   "lb",
   "lb",
   "lb",
   "lb",
   "lb",
   "\323",
   "lb",
   "lb"
},

//-----------------------------------------------------------------------------
// Saving mode enum :
//-----------------------------------------------------------------------------

/* STR_SAVING_MODE_AUTOMATIC */ 
{
   "Automaticky",
   "Automatisch",
   "Automatic",
   "Automaattinen",
   "Automatique",
   "Automatisch",
   "Automata",
   "\033\213\010",
   "Automatyczny",
   "Autom\200tico",
   "A\257\273o\270a\273\264\277ec\266a\307",
   "Autom\200tico",
   "Otomatik"
},
/* STR_SAVING_MODE_MANUAL */ 
{
   "Ru\201n\204",
   "Handmatig",
   "Manual",
   "Manuaalinen",
   "Manuel",
   "Manuell",
   "K\203zi",
   "\032\247\213\010",
   "R\240czny",
   "Manual",
   "P\274\277\271a\307",
   "Manual",
   "Elle"
},
/* STR_SAVING_MODE_MANUAL_BY_SEX */ 
{
   "Ru\201n\204 s pohlav\205m",
   "Sexe selecteren",
   "Manual by sex",
   "Manuaalinen uros/naaras",
   "Manuel par sexe",
   "Manuell n. Geschlecht",
   "K\203zi nemenk\203nti",
   "\032\247\213\010\036\006\233\206\235\200\265",
   "R\240czny wg p\241ci",
   "Manual por sexo",
   "P\274\277\271a\307 \272o \272o\267\274",
   "Manual por sexo",
   "Cinsiyet ayrimi"
},

//-----------------------------------------------------------------------------
// Weight sorting enum :
//-----------------------------------------------------------------------------

/* STR_WEIGHT_SORTING_NONE */ 
{
   "\235\200dn\203",
   "Geen geldige waarde",
   "None",
   "Ei",
   "Aucun",
   "Kein",
   "Semmi",
   "\214\032",
   "Brak",
   "Nenhum ",
   "\310e\263",
   "Ninguno",
   "Hi\024biri"
},
/* STR_WEIGHT_SORTING_LIGHT_HEAVY */ 
{
   "Lehk\215/T\204\216k\215",
   "Licht/Zwaar",
   "Light/Heavy",
   "Kevyet/Painavat",
   "L\203ger/Lourd",
   "Leicht/Schwer",
   "K\337nny\363/Neh\203z",
   "\024\006/\033\247\010",
   "Lekkie/Ci\240\245kie",
   "Leve/Pesado",
   "\320e\260\266\264e/T\307\262e\267\303e",
   "Ligero/Pesado",
   "Hafif/Agir"
},
/* STR_WEIGHT_SORTING_LIGHT_OK_HEAVY */ 
{
   "Lehk\215/OK/T\204\216k\215",
   "Licht/OK/Zwaar",
   "Light/OK/Heavy",
   "Kevyet/OK/Painavat",
   "L\203ger/OK/Lourd",
   "Leicht/OK/Schwer",
   "K\337nny\363/\217tlag/Neh\203z",
   "\024\006/OK/\033\247\010",
   "Lekkie/OK/Ci\240\245kie",
   "Leve/OK/Pesado",
   "\320e\260\266\264e/OK/T\307\262e\267\303e",
   "Ligero/Ok/Pesado",
   "Hafif/Normal/Agir"
},

//-----------------------------------------------------------------------------
// Backlight mode enum :
//-----------------------------------------------------------------------------

/* STR_BACKLIGHT_MODE_AUTO */ 
{
   "Automaticky",
   "Automatisch",
   "Automatic",
   "Automaattinen",
   "Automatique",
   "Automatisch",
   "Automata",
   "\033\213\010",
   "Automatyczny",
   "Autom\200tico",
   "A\257\273o\270a\273\264\277ec\266\264",
   "Autom\200tico",
   "Otomatik"
},
/* STR_BACKLIGHT_MODE_ON */ 
{
   "Zapnuto",
   "Aan",
   "On",
   "P\336\336ll\336",
   "On",
   "An",
   "Be",
   "\015\265",
   "W\241.",
   "Liga",
   "B\266\267",
   "Encendido",
   "Acik"
},
/* STR_BACKLIGHT_MODE_OFF */ 
{
   "Vypnuto",
   "Uit",
   "Off",
   "Pois",
   "Off",
   "Aus",
   "Ki",
   "\015\227",
   "Wy\241.",
   "Desliga",
   "B\303\266\267",
   "Apagado",
   "Kapali"
},

//-----------------------------------------------------------------------------
// Histogram mode enum :
//-----------------------------------------------------------------------------

/* STR_HISTOGRAM_MODE_RANGE */ 
{
   "Rozsah",
   "Aantal nummers",
   "Range",
   "Alue",
   "\222chelle",
   "Bereich",
   "Tartom\200ny",
   "\221\265\006",
   "Zakres",
   "Limite",
   "\312\264a\272a\263o\271",
   "Rango",
   "Hassasiyet"
},
/* STR_HISTOGRAM_MODE_STEP */ 
{
   "Krok",
   "Stap",
   "Step",
   "Porrastus",
   "Pas",
   "Schrittweite",
   "L\203p",
   "\034\210\205\231",
   "Krok",
   "Passo",
   "Yc\273a\271o\257\264\273\304",
   "Pago",
   "Olcu birimi"
},

//-----------------------------------------------------------------------------
// Display mode enum :
//-----------------------------------------------------------------------------

/* STR_DISPLAY_MODE_BASIC */ 
{
   "Z\200kladn\205",
   "Basis",
   "Basic",
   "Perustila",
   "Basique",
   "Einfach",
   "Alap",
   "\020\235\265\017\243\265\244\276\213",
   "Podstawowy",
   "Basico",
   "\310a\263o\257a\307",
   "B\200sico",
   "Basit"
},
/* STR_DISPLAY_MODE_ADVANCED */ 
{
   "Roz\211\205\210en\203",
   "Alle waarden",
   "Advanced",
   "Edistynyt",
   "Avanc\203e",
   "Erweitert",
   "Aj\200nlott",
   "\032\251\010\030\006\017\243\265\244\276\213",
   "Zaawansowany",
   "Avan\024ado",
   "Y\267\274\277\300e\271\271\303\265",
   "Avanzado",
   "Detayl\361"
},
/* STR_DISPLAY_MODE_STRONG */ 
{
   "Zv\204t\211en\203",
   "Groot",
   "Large",
   "Iso",
   "Large",
   "Gro\341",
   "Nagy",
   "\016\022\202\006\017\243\265\244\276\213",
   "Du\245y",
   "Grande",
   "\310o\267\304\300o\265",
   "Grande",
   "Buyuk"
},

//-----------------------------------------------------------------------------
// Printer protocol enum :
//-----------------------------------------------------------------------------

/* STR_PROTOCOL_STATISTICS_HISTOGRAM */ 
{
   "Statistika a histogram",
   "Statistieken en histogram",
   "Statistics and histogram",
   "Tilastot ja histogrammi",
   "Statistiques et histogramme",
   "Statistik und Histogramm",
   "Statisztika \203s grafikon",
   "\212\010\024\006\212\224\034\212\023\253\242",
   "Statystyki i histogram",
   "Estat\205stica e histograma",
   "C\273a\273\264c\273\264\266a \264 \260pa\275\264\266\264",
   "Estad\205sticas e histograma",
   "Istatistikler ve Grafik"
},
/* STR_PROTOCOL_STATISTICS */ 
{
   "Statistika",
   "Statistieken",
   "Statistics",
   "Tilastot",
   "Statistiques",
   "Statistik",
   "Statisztika",
   "\212\010\024\006",
   "Statystyki",
   "Estat\205stica",
   "C\273a\273\264c\273\264\266a",
   "Estad\205sticas",
   "Istatistikler"
},
/* STR_PROTOCOL_TOTAL_STATISTICS */ 
{
   "Celkov\200 statistika",
   "Totaal statistieken",
   "Total statistics",
   "Tilastot yhteens\336",
   "Statistiques totales",
   "Gesamtstatistik",
   "\343sszesitett statisztika",
   "\037\265\212\010\024\006",
   "Podsumowanie statystyk",
   "Estat\205stica Total",
   "O\256\301a\307 c\273a\273\264c\273\264\266a",
   "Total de las estad\205sticas",
   "Toplam istatistikler"
},
/* STR_PROTOCOL_SAMPLES */ 
{
   "Hmotnosti",
   "Gewichten",
   "Weights",
   "Punnitukset",
   "Pes\203e",
   "Gewichtswerte",
   "S\213lyok",
   "\201\006\033\247\010",
   "Pomiary wagi",
   "Pesos",
   "Bec",
   "Pesas",
   "Tartim"
},

//-----------------------------------------------------------------------------
// Beep tone enum :
//-----------------------------------------------------------------------------

/* STR_BEEP_TONE1       */ 
{
   "T\207n 1",
   "Geluidstoon 1",
   "Tone 1",
   "\342\336ni 1",
   "Tonalit\203 1",
   "Ton 1",
   "Hang 1",
   "\212\276\2651",
   "D\244wi\240k 1",
   "Tom 1",
   "To\271 1",
   "Sonido 1",
   "Ses 1"
},
/* STR_BEEP_TONE2       */ 
{
   "T\207n 2",
   "Geluidstoon 2",
   "Tone 2",
   "\342\336ni 2",
   "Tonalit\203 2",
   "Ton 2",
   "Hang 2",
   "\212\276\2652",
   "D\244wi\240k 2",
   "Tom 2",
   "To\271 2",
   "Sonido 2",
   "Ses 2"
},
/* STR_BEEP_TONE3       */ 
{
   "T\207n 3",
   "Geluidstoon 3",
   "Tone 3",
   "\342\336ni 3",
   "Tonalit\203 3",
   "Ton 3",
   "Hang 3",
   "\212\276\2653",
   "D\244wi\240k 3",
   "Tom 3",
   "To\271 3",
   "Sonido 3",
   "Ses 3"
},
/* STR_BEEP_TONE4       */ 
{
   "T\207n 4",
   "Geluidstoon 4",
   "Tone 4",
   "\342\336ni 4",
   "Tonalit\203 4",
   "Ton 4",
   "Hang 4",
   "\212\276\2654",
   "D\244wi\240k 4",
   "Tom 4",
   "To\271 4 ",
   "Sonido 4",
   "Ses 4"
},
/* STR_BEEP_TONE5       */ 
{
   "T\207n 5",
   "Geluidstoon 5",
   "Tone 5",
   "\342\336ni 5",
   "Tonalit\203 5",
   "Ton 5",
   "Hang 5",
   "\212\276\2655",
   "D\244wi\240k 5",
   "Tom 5",
   "To\271 5",
   "Sonido 5",
   "Ses 5"
},
/* STR_BEEP_TONE6       */ 
{
   "T\207n 6",
   "Geluidstoon 6",
   "Tone 6",
   "\342\336ni 6",
   "Tonalit\203 6",
   "Ton 6",
   "Hang 6",
   "\212\276\2656",
   "D\244wi\240k 6",
   "Tom 6",
   "To\271 6",
   "Sonido 6",
   "Ses 6"
},
/* STR_BEEP_TONE7       */ 
{
   "T\207n 7",
   "Geluidstoon 7",
   "Tone 7",
   "\342\336ni 7",
   "Tonalit\203 7",
   "Ton 7",
   "Hang 7",
   "\212\276\2657",
   "D\244wi\240k 7",
   "Tom 7",
   "To\271 7",
   "Sonido 7",
   "Ses 7"
},
/* STR_BEEP_TONE8       */ 
{
   "T\207n 8",
   "Geluidstoon 8",
   "Tone 8",
   "\342\336ni 8",
   "Tonalit\203 8",
   "Ton 8",
   "Hang 8",
   "\212\276\2658",
   "D\244wi\240k 8",
   "Tom 8",
   "To\271 8",
   "Sonido 8",
   "Ses 8"
},
/* STR_BEEP_MELODY1     */ 
{
   "Melodie 1",
   "Melodie 1",
   "Melody 1",
   "Melodia 1",
   "M\203lodie 1",
   "Melodie 1",
   "Dallam 1",
   "\243\257\211\0051",
   "Melodia 1",
   "Melodia 1",
   "Me\267o\261\264\307 1",
   "Melod\205a 1",
   "Melodi 1"
},
/* STR_BEEP_MELODY2     */ 
{
   "Melodie 2",
   "Melodie 2",
   "Melody 2",
   "Melodia 2",
   "M\203lodie 2",
   "Melodie 2",
   "Dallam 2",
   "\243\257\211\0052",
   "Melodia 2",
   "Melodia 2",
   "Me\267o\261\264\307 2",
   "Melod\205a 2",
   "Melodi 2"
},
/* STR_BEEP_MELODY3     */ 
{
   "Melodie 3",
   "Melodie 3",
   "Melody 3",
   "Melodia 3",
   "M\203lodie 3",
   "Melodie 3",
   "Dallam 3",
   "\243\257\211\0053",
   "Melodia 3",
   "Melodia 3",
   "Me\267o\261\264\307 3",
   "Melod\205a 3",
   "Melodi 3"
},
/* STR_BEEP_MELODY4     */ 
{
   "Melodie 4",
   "Melodie 4",
   "Melody 4",
   "Melodia 4",
   "M\203lodie 4",
   "Melodie 4",
   "Dallam 4",
   "\243\257\211\0054",
   "Melodia 4",
   "Melodia 4",
   "Me\267o\261\264\307 4",
   "Melod\205a 4",
   "Melodi 4"
},

//-----------------------------------------------------------------------------
// Capacity enum :
//-----------------------------------------------------------------------------

/* STR_CAPACITY_NORMAL  */ 
{
   "30kg/60lb",
   "30kg/60lb",
   "30kg/60lb",
   "30kg/60lb",
   "30kg/60lb",
   "30kg/60lb",
   "30kg/60lb",
   "30kg",
   "30kg/60lb",
   "30kg/60lb",
   "30\266\260/60\323",
   "30kg/60lb",
   "30kg/60lb"
},
/* STR_CAPACITY_EXTENDED */ 
{
   "50kg/100lb",
   "50kg/100lb",
   "50kg/100lb",
   "50kg/100lb",
   "50kg/100lb",
   "50kg/100lb",
   "50kg/100lb",
   "50kg",
   "50kg/100lb",
   "50kg/100lb",
   "50\266\260/100\323",
   "50kg/100lb",
   "50kg/100lb"
},

//-----------------------------------------------------------------------------
// Charger screen :
//-----------------------------------------------------------------------------

/* STR_CHARGING         */ 
{
   "Nab\205jen\205",
   "Batterij laadt op",
   "Charging",
   "Latautuu",
   "En charge",
   "Laden",
   "T\337lt\203s",
   "\033\247\010\211\265\203\247\010",
   "\251adowanie",
   "Carregando",
   "\314ap\307\261\264\273e \256a\273ape\306",
   "Cargando",
   "Sarj ediyor"
},
/* STR_CHARGED          */ 
{
   "Nabito",
   "Bettaerij opgeladen",
   "Charged",
   "Ladattu",
   "Charg\203e",
   "Geladen",
   "Felt\337ltve",
   "\033\247\010\211\265\016\265\254\251\010",
   "Na\241adowana",
   "Carregado",
   "\310a\273ape\307 pa\263p\307\262e\271a",
   "Cargado",
   "Batarya dolu"
},

//-----------------------------------------------------------------------------
// Wait screen :
//-----------------------------------------------------------------------------

/* STR_WAIT             */ 
{
   "\220ekejte pros\205m",
   "Aub wachten",
   "Please wait",
   "Odota",
   "Veuillez patienter",
   "Bitte warten",
   "V\200rjon",
   "\201\006\020",
   "Prosz\240 czeka\237",
   "Favor esperar",
   "\321o\261o\262\261\264\273e",
   "Por favor espere",
   "Lutfen bekleyiniz"
},

//-----------------------------------------------------------------------------
// No memory screen :
//-----------------------------------------------------------------------------

/* STR_NO_MEMORY        */ 
{
   "Pam\204\212 je pln\200",
   "Geheugen is vol",
   "Memory full",
   "Muisti t\336ynn\336",
   "M\203moire pleine",
   "Speicher voll",
   "Mem\207ria tele",
   "\252\010\254\251\010\017\006\205\223\006\211\034",
   "Pami\240\237 pe\241na",
   "Mem\207ria cheia",
   "\321a\270\307\273\304 \263a\272o\267\271e\271a",
   "Memoria completa",
   "Hafiza dolu"
},

//-----------------------------------------------------------------------------
// Weighing screen :
//-----------------------------------------------------------------------------

/* STR_NO_FILE          */ 
{
   "<Bez souboru>",
   "<geen bestand>",
   "<No file>",
   "<Ei tiedostoa>",
   "<Pas de fichier>",
   "<Keine Datei>",
   "<Nincs adat>",
   "<No file>",
   "<Brak pliku>",
   "<Nenhum arquivo>",
   "<He\273 \275a\265\267a>",
   "<No hay archivo>",
   "<Bo\362>"
},

//-----------------------------------------------------------------------------
// Off screen :
//-----------------------------------------------------------------------------

/* STR_OFF              */ 
{
   "VYPNUTO",
   "UIT",
   "OFF",
   "POIS",
   "OFF",
   "AUS",
   "KIKAPCSOLVA",
   "\015\227",
   "WY\251.",
   "DESLIGA",
   "B\331\317\320",
   "APAGADO",
   "KAPALI"
},

//-----------------------------------------------------------------------------
// Main menu :
//-----------------------------------------------------------------------------

/* STR_MAIN_MENU        */ 
{
   "Hlavn\205 nab\205dka",
   "Hoofd menu",
   "Main menu",
   "P\336\336valikko",
   "Menu principal",
   "Hauptmenue",
   "F\364men\340",
   "\243\006\265\243\215\247\276",
   "Menu g\241\207wne",
   "Menu principal",
   "Me\271\306",
   "Men\213 principal",
   "Ana menu"
},
/* STR_WEIGHING         */ 
{
   "V\200\216en\205",
   "Wegen",
   "Weighing",
   "Punnitukset",
   "Pesage",
   "Wiegen",
   "M\203r\203s",
   "\024\006\177\022",
   "Wa\245enie",
   "Pesando",
   "B\263\257e\300\264\257a\271\264e",
   "Pesando",
   "Canli agirliklar"
},
/* STR_STATISTICS       */ 
{
   "Statistika",
   "Statistieken",
   "Statistics",
   "Tilastot",
   "Statistiques",
   "Statistik",
   "Statisztika",
   "\212\010\024\006",
   "Statystyki",
   "Estat\205stica",
   "C\273a\273\264c\273\264\266a",
   "Estad\205sticas",
   "Istatistikler"
},
/* STR_SETTINGS         */ 
{
   "U\216ivatelsk\203 nastaven\205",
   "Gebruikers instellingen",
   "User settings",
   "K\336ytt\337asetukset",
   "Param\025tre utilisateur",
   "Benutzereinstellungen",
   "Be\200ll\205t\200sok",
   "\250\276\031\276\036\205\210\006",
   "Ustawienia u\245ytkownika",
   "Ajuste do usu\200rio",
   "\315c\272o\267\304\263\274e\270\303e \274c\273a\271o\257\266\264",
   "Rutina del usuario",
   "Kullanici ayarlari"
},
/* STR_CONFIGURATION    */ 
{
   "Konfigurace",
   "Configuratie",
   "Configuration",
   "Asetukset",
   "Configuration",
   "Konfiguration",
   "Testreszab\200s",
   "\016\265\020\251\010\036\205\210\006",
   "Konfiguracja",
   "Configura\024\023o",
   "\317o\271\275\264\260\274pa\276\264\307",
   "Configuraci\207n",
   "Kurulum"
},
/* STR_MAINTENANCE      */ 
{
   "Servisn\205 nastaven\205",
   "Onderhoud",
   "Maintenance",
   "Yll\336pito",
   "Maintenance",
   "Wartung",
   "Karbantart\200s",
   "\243\265\210\214\265\034",
   "Konserwacja",
   "Manuten\024\023o",
   "P\274\266o\257o\261c\273\257o",
   "Mantenimiento",
   "Ayarlar"
},

//STR_DELETE_SAMPLE �Delete sample�
/* STR_DELETE_LAST_SAMPLE */ 
{
   "Opravdu smazat posledn\205\nhmotnost ?",
   "Laatste gewicht echt verwijderen?",
   "Really delete last weight ?",
   "Haluatko varmasti poistaa\nviimeisimm\336n punnituksen?",
   "Voulez-vous vraiment\nsupprimer la derniere pes\203e?",
   "Wollen Sie den letzten\nGewichtswert wirklich l\337schen?",
   "T\337rli az utols\207 m\203r\203st?",
   "\030\006\027\220\201\006\033\247\010\264\235\265\212\n\010\215\030\022\033\251\032\240\034\016?",
   "Czy na pewno chcesz usun\236\237\nostatni pomiar wagi?",
   "Relamente deletar ultimo peso?",
   "Y\261a\267\264\273\304 \272oc\267e\261\271ee \257ec?",
   "\360Realmente anular\nel \213ltimo peso?",
   "Son tartim silinsin mi?"
},

//-----------------------------------------------------------------------------
// Weighing menu :
//-----------------------------------------------------------------------------

/* STR_ACTIVE_FILE      */ 
{
   "Aktivn\205 soubor",
   "Actief bestand",
   "Active file",
   "Aktiivinen tiedosto",
   "Fichier actif",
   "Aktive Datei",
   "Aktu\200lis adat",
   "\004\022\210\005\230\227\003\006\255",
   "Aktywny plik",
   "Ativar arquivo",
   "Hop\270a\267\304\271\303\265 \275a\265\267",
   "Archivo activo",
   "Aktif dosya"
},
/* STR_VIEW_FILE        */ 
{
   "Zobrazit soubor",
   "Bestand inzien",
   "View file",
   "N\336yt\336 tiedosto",
   "Voir le fichier",
   "Datei \337ffnen",
   "Adat mutat\200sa",
   "\227\003\006\255\264\241\255",
   "Wy\243wietl plik",
   "Ver arquivo",
   "B\264\261 \261a\273\303 \275a\265\267a",
   "Mirar el archivo",
   "Dosyayi goster"
},
/* STR_NUMBER_OF_BIRDS  */ 
{
   "Po\201et kus\214",
   "Aantal dieren",
   "Number of birds",
   "Lintujen lukum\336\336r\336",
   "Nombre d'animaux",
   "Anzahl V\337gel",
   "Madarak sz\200ma",
   "\221\034\010",
   "Ilo\243\237 ptak\207w",
   "N\213mero de aves",
   "\317o\267\264\277ec\273\257o \272\273\264\276\303",
   "N\213mero de aves",
   "Hayvan adedi"
},
/* STR_LIMIT            */ 
{
   "Mez",
   "Gewichten instellen",
   "Limit",
   "Raja",
   "Limite",
   "Grenze",
   "Hat\200r",
   "\254\241\205\212",
   "Limit",
   "Limite",
   "\320\264\270\264\273",
   "L\205mite",
   "Limit"
},
/* STR_LOW_LIMIT        */ 
{
   "Spodn\205 mez",
   "Laagste gewicht instellen",
   "Low limit",
   "Alaraja",
   "Limite basse",
   "Untere Grenze",
   "Alacsony hat\200r",
   "\016\025\265",
   "Niski limit",
   "Limite baixo",
   "H\264\262\271\264\265 \272pe\261e\267",
   "L\205mite bajo",
   "Alt limit"
},
/* STR_HIGH_LIMIT       */ 
{
   "Horn\205 mez",
   "Hoogste gewicht instellen",
   "High limit",
   "Yl\336raja",
   "Limite haute",
   "Obere Grenze",
   "Magas hat\200r",
   "\033\251\010\025\265",
   "Wysoki limit",
   "Limite alto",
   "Bepx\271\264\265 \272pe\261e\267",
   "L\205mite alto",
   "Ust limit"
},
/* STR_CLEAR_FILE       */ 
{
   "Smazat hmotnosti",
   "Verwijder gewichten",
   "Delete weights",
   "Poista punnitukset",
   "Effacer la pes\203e",
   "Gewichtswerte l\337schen",
   "S\213lyok t\337rl\203se",
   "\201\006\033\247\010\264\030\022\033\251",
   "Usu\242 wa\245enia",
   "Deletar pesos",
   "Y\261a\267\264\273\304 \257eca",
   "Anular pesos",
   "Tartimlari sil"
},
/* STR_CLEAR_ALL_FILES  */ 
{
   "Smazat v\211echny hmotnosti",
   "Verwijder alle gewichten",
   "Delete all weights",
   "Poista kaikki punnitukset",
   "Effacer toutes les pes\203es",
   "Alle Gewichtswerte l\337schen",
   "\343sszes s\213ly t\337rl\203se",
   "\034\233\210\220\201\006\033\247\010\264\030\022\033\251",
   "Usu\242 wszystkie wa\245enia",
   "Deletar todos os pesos",
   "Y\261a\267\264\273\304 \257ce \257eca",
   "Anulas todos los pesos",
   "Tum tartimlari sil"
},

// Weighing menu messages :
/* STR_REALLY_CLEAR     */ 
{
   "Opravdu smazat hmotnosti\nv souboru ?",
   "Gewichten in bestand echt\nverwijderen?",
   "Really delete weights in the file ?",
   "Haluatko varmasti poistaa\npunnitukset tiedostosta?",
   "Voulez-vous vraiment supprimer\nles pes\203es dans ce fichier?",
   "Wirklich alle Gewichtswerte\nin der Datei l\337schen?",
   "S\213ly t\337rl\203se?",
   "\227\003\006\255\214\006\220\201\006\033\247\010\264\235\n\265\212\010\215\030\022\033\251\032\240\034\016?",
   "Czy na pewno chcesz\nusun\236\237 wa\245enia z pliku?",
   "Relamente deletar pesos\nno arquivo?",
   "Y\261a\267\264\273\304 \257eca \264\263 \275a\265\267a?",
   "\360Realmente anular los pesos\nen este archivo?",
   "Tartim gercekten\nsilinsin mi?"
},
/* STR_REALLY_CLEAR_ALL */ 
{
   "Opravdu smazat hmotnosti\nve v\211ech souborech ?",
   "Gewichten in alle bestanden\nechter verwijderen?",
   "Really delete weights in all files ?",
   "Haluatko varmasti poistaa\npunnitukset kaikista tiedostoista?",
   "Voulez-vous vraiment supprimer\nles pes\203es dans tous les fichiers?",
   "Wirklich Gewichtswerte\nin allen Dateien l\337schen?",
   "\343sszes s\213ly t\337rl\203se?",
   "\037\265\227\003\006\255\220\201\006\033\247\010\264\235\265\n\212\010\215\030\022\033\251\032\240\034\016?",
   "Czy na pewno chcesz usun\236\237\nwa\245enia ze wszystkich plik\207w?",
   "Relamente deletar pesos\nem todos os arquivos?",
   "Y\261a\267\264\273\304 \257eca \264\263 \257cex \275a\265\267o\257?",
   "\360Realmente anular los pesos\nen todos los archivos?",
   "Tum tartimlar gercekten\nsilinsin mi?"
},

//-----------------------------------------------------------------------------
// Statistics menu :
//-----------------------------------------------------------------------------

/* STR_TOTAL_STATISTICS */ 
{
   "Celkov\200 statistika",
   "Totale statistieken",
   "Total statistics",
   "Tilastot yhteens\336",
   "Statistiques totales",
   "Gesamtstatistik",
   "\343sszes\205tett statisztika",
   "\037\265\212\010\024\006",
   "Podsumowanie statystyk",
   "Estat\205stica total",
   "O\256\301a\307 c\273a\273\264c\273\264\266a",
   "Total de estad\205sticas",
   "Toplam istatistikler"
},
/* STR_COMPARE_FILES    */ 
{
   "Porovnat soubory",
   "Vergelijk bestanden",
   "Compare files",
   "Vertaile tiedostoja",
   "Comparer les fichiers",
   "Dateien vergleichen",
   "\343sszehasonl\205t\200s",
   "\227\003\006\255\264\224\016\022",
   "Por\207wnaj pliki",
   "Comparar arquivos",
   "Cpa\257\271e\271\264e \275a\267o\257",
   "Comparar archivos",
   "Dosyalari karsilastir"
},
/* STR_PRINT            */ 
{
   "Tisk",
   "Afdrukken",
   "Print",
   "Tulosta",
   "Imprimer",
   "Drucken",
   "Nyomtat",
   "\006\265\030\206",
   "Drukuj",
   "Imprimir",
   "\321e\277a\273\304",
   "Imprimir",
   "Yazdir"
},

// Total statistics selection menu :
/* STR_ONE_FILE         */ 
{
   "Jeden soubor",
   "Een bestand",
   "One file",
   "Yksi tiedosto",
   "Un fichier",
   "Eine Datei",
   "Egy adat",
   "\224\212\206\220\227\003\006\255",
   "Jeden plik",
   "Um arquivo",
   "O\261\264\271 \275a\265\267",
   "Un archivo",
   "Tek dosya"
},
/* STR_ONE_GROUP        */ 
{
   "Skupina soubor\214",
   "Een groep",
   "One group",
   "Yksi ryhm\336",
   "Un groupe",
   "Eine Gruppe",
   "Egy csoport",
   "\224\212\206\220\023\255\276\231",
   "Jedna grupa",
   "Um grupo",
   "O\261\271a \260p\274\272\272a",
   "Un grupo",
   "Grup"
},
/* STR_MORE_FILES       */ 
{
   "V\205ce soubor\214",
   "Meer bestanden",
   "More files",
   "Useampi tiedosto",
   "Plus de fichier",
   "Weitere Dateien",
   "T\337bb adat kiv\200laszt\200sa",
   "\030\253\214\255\227\003\006\255",
   "Wi\240cej plik\207w",
   "Mais arquivos",
   "M\271o\260o \275a\265\267o\257",
   "Mas archivos",
   "Coklu dosya"
},
/* STR_ALL_FILES        */ 
{
   "V\211echny soubory",
   "Alle bestanden",
   "All files",
   "Kaikki tiedostot",
   "Tous les fichiers",
   "Alle Dateien",
   "\343sszes adat",
   "\034\233\210\220\227\003\006\255",
   "Wszystkie pliki",
   "Todos os arquivos",
   "Bce \275a\265\267\303",
   "Todos los archivos",
   "Tum dosyalar"
},

// Total statistics messages :
/* STR_NO_FILES_SELECTED */ 
{
   "Nen\205 vybran\215 \216\200dn\215 soubor",
   "Geen bestanden geselecteerd",
   "No files selected",
   "Tiedostoja ei ole valittu",
   "Pas de fichiers choisis",
   "Keine Datei ausgew\336hlt",
   "Nincs adat v\200lasztva",
   "\227\003\006\255\017\036\265\201\022\030\256\210\006\240\036\265",
   "Brak zaznaczonych plik\207w",
   "Nenhum arquivo selecionado",
   "He \257\303\256pa\271 \275a\265\267",
   "No hay archivos seleccionados",
   "Hicbir dosya secilmedi"
},

// Compare file messages :
/* STR_TOTAL            */ 
{
   "Celkem",
   "Totaal statistieken",
   "Total",
   "Yhteens\336",
   "Total",
   "Summe",
   "\343sszes",
   "\212\276\201\255",
   "Suma",
   "Total",
   "Bce\260o",
   "Total",
   "Toplam"
},

// Print messages :
/* STR_ENTER_PROTOCOL_TYPE */ 
{
   "Zadejte typ protokolu",
   "Maak een keuze",
   "Select report type",
   "Valitse protokolla",
   "Choisir le type de protocole",
   "Protokolltyp ausw\336hlen",
   "Jelent\203s t\205pusa",
   "\256\237\276\212\220\201\006\231\264\036\265\201\022",
   "Wybierz typ raportu",
   "Selecionar tipo de protocolo",
   "B\303\256ep\264\273e \273\264\272 \272po\273o\266o\267a",
   "Seleccionar tipo de informe",
   "Rapor tipini seciniz"
},

//-----------------------------------------------------------------------------
// Settings menu :
//-----------------------------------------------------------------------------

/* STR_DISPLAY_BACKLIGHT */ 
{
   "Podsvit displeje",
   "Stel schermverlichting in",
   "Display backlight",
   "N\336yt\337n taustavalo",
   "Luminosit\203 de l'\203cran",
   "Hintergrundbeleuchtung",
   "Kijelz\364 megvil\200g\205t\200s",
   "\222\205\022\253\006\212\264\224\251\010\033",
   "Pod\243wietlenie wy\243wietlacza",
   "Exibir luz traseira",
   "\335p\266oc\273\304",
   "Luz posterior de la pantalla",
   "Ekran aydinlatma"
},
/* STR_DISPLAY_CONTRAST */ 
{
   "Kontrast displeje",
   "Stel contrast in",
   "Display contrast",
   "N\336yt\337n kontrasti",
   "Contraste de l'\203cran",
   "Kontrast",
   "Kijelz\364 kontraszt",
   "\026\265\212\253\034\212\264\224\251\010\033",
   "Kontrast wy\243wietlacza",
   "Exibir contraste",
   "\317o\271\273pac\273\271oc\273\304",
   "Contraste de la pantalla",
   "Ekran kontrasti"
},
/* STR_SAVING_VOLUME    */ 
{
   "Hlasitost ukl\200d\200n\205",
   "Volume opslaan",
   "Saving volume",
   "Tallennus\336\336nen voimakkuus",
   "Enregistrer le volume",
   "Lautst\336rke",
   "Ment\203si hanger\364",
   "\015\265\254\251\010\264\235\200\265",
   "G\241o\243no\243\237 zapisywania",
   "Salvando volume",
   "Coxpa\271e\271\264e o\256\302e\270a",
   "Bajando el volumen",
   "Kaydet sesi"
},
/* STR_DATE_AND_TIME    */ 
{
   "Datum a \201as",
   "Datum en tijd",
   "Date and time",
   "Pvm ja aika",
   "Date et heure",
   "Datum und Zeit",
   "D\200tum \203s id\364",
   "\224\207\024\212\033\016\265",
   "Data i godzina",
   "Data e hora",
   "\312a\273a \264 \257pe\270\307",
   "Fecha y hora",
   "Tarih ve saat"
},

// Settings menu messages :
/* STR_ENTER_DATE       */ 
{
   "Zadejte datum",
   "Voer datum in",
   "Enter date",
   "Anna pvm",
   "Entrer la date",
   "Datum eingeben",
   "D\200tum megad\200sa",
   "\224\207\024\264\215\247\010\254\251\022",
   "Wprowad\244 dat\240",
   "Entrar em data",
   "B\257o\261 \261a\273\303",
   "Introducir fecha",
   "Tarih gir"
},
/* STR_ENTER_TIME       */ 
{
   "Zadejte \201as",
   "Voer tijd in",
   "Enter time",
   "Anna aika",
   "Entrer l'heure",
   "Zeit eingeben",
   "Id\364 megad\200sa",
   "\033\016\265\264\215\247\010\254\251\022",
   "Wprowad\244 godzin\240",
   "Entrar em hora",
   "B\257o\261 \257pe\270e\271\264",
   "Introducir hora",
   "Saat gir"
},

//-----------------------------------------------------------------------------
// Configuration menu :
//-----------------------------------------------------------------------------

/* STR_FILES            */ 
{
   "Soubory",
   "Bestanden",
   "Files",
   "Tiedostot",
   "Fichiers",
   "Dateien",
   "Adatok",
   "\227\003\006\255",
   "Pliki",
   "Arquivos",
   "\323a\265\267\303",
   "Archivos",
   "Calisilan dosyalar"
},
/* STR_FILE_GROUPS      */ 
{
   "Skupiny soubor\214",
   "Bestandsgroepen",
   "File groups",
   "Tiedostoryhm\336t",
   "Groupes de fichiers",
   "Dateigruppen",
   "Adatcsoportok",
   "\227\003\006\255\023\255\276\231",
   "Grupy pliku",
   "Grupos de arquivos",
   "\323a\265\267\303 \260p\274\272\272",
   "Grupos de archivos",
   "Dosya gruplari"
},
/* STR_SAVING           */ 
{
   "Ukl\200d\200n\205",
   "Opslaan",
   "Saving",
   "Tallennus",
   "Sauvegarde",
   "Speichern",
   "Ment\203s",
   "\235\200\265\203\247\010",
   "Zapisywanie",
   "Salvando",
   "Coxpa\271e\271\264e o\256\302e\270a",
   "Guardar",
   "Kaydet"
},
/* STR_DISPLAY          */ 
{
   "Displej",
   "Weergeven",
   "Display",
   "N\336ytt\337",
   "\222cran",
   "Display",
   "Kijelz\364",
   "\224\251\010\033",
   "Wy\243wietlanie",
   "Exibir",
   "\312\264c\272\267e\265",
   "Exhibir",
   "Ekran"
},
/* STR_SOUNDS           */ 
{
   "Zvuky",
   "Geluiden",
   "Sounds",
   "\342\336net",
   "Sons",
   "Kl\336nge",
   "Hangok",
   "\030\010\265\213",
   "D\244wi\240ki",
   "Sons",
   "\314\257\274\266",
   "Sonidos",
   "Sesler"
},
//STR_STATISTICS        Statistics

//-----------------------------------------------------------------------------
// Configuration/Files menu :
//-----------------------------------------------------------------------------

/* STR_CREATE           */ 
{
   "Vytvo\210it",
   "Cre\027ren",
   "Create",
   "Luo",
   "Cr\203er",
   "Erzeugen",
   "\232j",
   "\032\265\020\227\003\006\255",
   "Utw\207rz",
   "Criar",
   "Co\263\261a\273\304",
   "Crear",
   "Yeni dosya ac"
},
/* STR_EDIT_SAVING_OPTIONS */ 
{
   "Parametry ukl\200d\200n\205",
   "Opslaan opties bewerken",
   "Edit saving options",
   "Muuta tallennusasetuksia",
   "Editer les options de sauvegarde",
   "Speicheroptionen \336ndern",
   "Ment\203si be\200ll\205t\200sok",
   "\235\200\265\036\205\210\006\264\232\265\026\010",
   "Edytuj opcje zapisywania",
   "Editar op\024\036es salvas",
   "Pe\261a\266\273\264po\257a\273\304 o\272\276\264\264",
   "Editar opciones de guardar",
   "Kayit seceneklerini d\340zenle"
},
/* STR_EDIT_NOTE        */ 
{
   "Upravit pozn\200mku",
   "Bewerken",
   "Edit note",
   "Muuta kuvausta",
   "Editer une note",
   "Bemerkungstext \336ndern",
   "Sor szerkeszt\203se",
   "\203\247\010\032\245\022\264\232\265\032\247\010",
   "Edytuj notatk\240",
   "Editar notas",
   "Pe\261a\266\273\264po\257a\273\304 \263a\270e\277a\271\264e",
   "Editar nota",
   "Yeni not"
},
/* STR_RENAME           */ 
{
   "P\210ejmenovat",
   "Herbenoem",
   "Rename",
   "Nime\336 uudelleen",
   "Renommer",
   "Umbenennen",
   "\217tnevez",
   "\214\240\013\264\232\265\026\010",
   "Zmie\242 nazw\240",
   "Renomear",
   "\321epe\264\270e\271o\257a\273\304",
   "Cambiar nombre",
   "Yeni ad ver"
},
/* STR_DELETE           */ 
{
   "Smazat",
   "Verwijderen",
   "Delete",
   "Poista",
   "Effacer",
   "L\337schen",
   "T\337r\337l",
   "\030\022\033\251",
   "Usu\242",
   "Deletar",
   "Y\261a\267\264\273\304",
   "Anular",
   "Sil"
},

// Files menu messages :
/* STR_ENTER_NOTE       */ 
{
   "Zadejte pozn\200mku",
   "Notitie invoeren",
   "Enter note",
   "Anna kuvaus",
   "Entrer une note",
   "Bemerkung eingeben",
   "Sor bevitel megad\200s",
   "\203\247\010\032\245\022\264\215\247\010\254\251\022",
   "Wprowad\244 notatk\240",
   "Entrar em nota",
   "B\257o\261 \263a\270e\273\266\264",
   "Introducir nota",
   "Not gir"
},
/* STR_ENTER_NAME       */ 
{
   "Zadejte n\200zev",
   "Naam invoeren",
   "Enter name",
   "Anna nimi",
   "Entrer un nom",
   "Name eingeben",
   "N\203v megad\200s",
   "\214\240\013\264\215\247\010\254\251\022",
   "Wprowad\244 nazw\240",
   "Entrar em nome",
   "B\257o\261 \264\270e\271\264",
   "Introducir nombre",
   "Isim gir"
},
/* STR_FILE_EXISTS      */ 
{
   "Soubor ji\216 existuje",
   "Bestandsnaam bestaat al",
   "File already exists",
   "Tiedosto on jo olemassa",
   "Le fichier existe d\203j\021",
   "Datei bereits vorhanden",
   "M\200r l\203tez\364 adat",
   "\227\003\006\255\017\034\211\215\177\265\031\006\032\240\034",
   "Plik ju\245 istnieje",
   "O arquivo j\200 existe",
   "\323a\265\267 \274\262e c\274\301ec\273\257\274e\273",
   "Archivo ya existe",
   "Dosya zaten var"
},
/* STR_NOT_ENOUGH_SPACE */ 
{
   "Pam\204\212 je pln\200",
   "Geheugen is vol",
   "Memory full",
   "Muisti t\336ynn\336",
   "M\203moire pleine",
   "Speicher voll",
   "Megtelt a mem\207ria",
   "\252\010\254\251\010\017\006\205\223\006\211\034",
   "Pami\240\237 pe\241na",
   "Mem\207ria cheia",
   "\321a\270\307\273\304 \263a\272o\267\271e\271a",
   "Memoria completa",
   "Haf\361za dolu"
},
/* STR_REALLY_DELETE_FILE */ 
{
   "Opravdu smazat soubor ?",
   "Bestand echt verwijderen?",
   "Really delete file ?",
   "Haluatko varmasti poistaa\ntiedoston?",
   "Voulez-vous vraiment supprimer\nce fichier?",
   "Datei wirklich l\337schen?",
   "Val\207ban t\337rli?",
   "\235\265\212\010\215\227\003\006\255\264\n\030\022\033\251\032\240\034\016?",
   "Czy na pewno chcesz\nusun\236\237 plik?",
   "Realmente deletar o arquivo?",
   "Y\261a\267\264\273\304 \275a\265\267?",
   "\360Realmente anular el archivo?",
   "Dosya gercekten silinsin mi?"
},

//-----------------------------------------------------------------------------
// Configuration/File groups menu :
//-----------------------------------------------------------------------------

//STR_CREATE         Create
/* STR_EDIT_FILE_LIST   */ 
{
   "Upravit seznam soubor\214",
   "Bestandslijst bewerken",
   "Edit file list",
   "Muokkaa tiedostolistaa",
   "Editer la liste des fichiers",
   "Dateiliste bearbeiten",
   "Adatok szerkeszt\203se",
   "\227\003\006\255\254\034\212\301\232\265\032\247\010",
   "Edytuj list\240 pliku",
   "Editar lista de arquivo",
   "Pe\261a\266\273\264po\257a\273\304 \275a\265\267 \267\264c\273",
   "Editar la lista de archivos",
   "Gruptaki dosyalar\361 listele"
},
//STR_EDIT_NOTE      Edit note
//STR_RENAME         Rename
//STR_DELETE         Delete

//STR_ENTER_NOTE        Enter note
//STR_ENTER_NAME        Enter Name
/* STR_GROUP_EXISTS     */ 
{
   "Skupina ji\216 existuje",
   "Groep bestaat al",
   "Group already exists",
   "Ryhm\336 on jo olemassa",
   "Ce groupe existe d\203j\021",
   "Gruppe bereits vorhanden",
   "M\200r l\203tez\364 csoport",
   "\023\255\276\231\017\034\211\215\177\265\031\006\032\240\034",
   "Grupa ju\245 istnieje",
   "O grupo j\200 existe",
   "\311p\274\272\272a \274\262e c\274\301ec\273\257\274e\273",
   "Grupo ya existe",
   "Grup zaten var"
},
/* STR_REALLY_DELETE_GROUP */ 
{
   "Opravdu smazat skupinu ?",
   "Groep echt verwijderen?",
   "Really delete group ?",
   "Haluatko varmasti poistaa\nryhm\336n?",
   "Voulez-vous vraiment supprimer\nce groupe?",
   "Gruppe wirklich l\337schen?",
   "Val\207ban t\337rli a csoportot?",
   "\235\265\212\010\215\023\255\276\231\264\n\030\022\033\251\032\240\034\016?",
   "Czy na pewno chcesz\nusun\236\237 grup\240?",
   "Realmente deletar o grupo?",
   "Y\261a\267\264\273 \260p\274\272\272\274?",
   "\360Realmente anular el grupo?",
   "Grup gercekten silinsin mi?"
},
//STR_NO_FILES_SELECTED No files selected
//STR_NOT_ENOUGH_SPACE  Not enough space

//-----------------------------------------------------------------------------
// Configuration/Backlight menu :
//-----------------------------------------------------------------------------

/* STR_MODE             */ 
{
   "Re\216im",
   "Schermverlichting",
   "Mode",
   "Tila",
   "Mode",
   "Betriebsart",
   "M\207d",
   "\244\276\213",
   "Tryb",
   "Modo",
   "Mo\261e\267\304",
   "Modalidad",
   "Mod"
},
/* STR_DURATION         */ 
{
   "Doba podsvitu",
   "Duur",
   "Duration",
   "Kesto",
   "Dur\203e",
   "Dauer",
   "Id\364tartam",
   "\210\265\212\010\033\016\265",
   "Czas trwania",
   "Dura\024\023o",
   "\321po\261o\267\262\264\273e\267\304\271oc\273\304",
   "Duraci\207n",
   "Sure"
},
/* STR_INTENSITY        */ 
{
   "Intenzita",
   "Intensiteit",
   "Intensity",
   "Kirkkaus",
   "Intensit\203",
   "Intensit\336t",
   "\222rz\203kenys\203g",
   "\210\265\212\010\020\251\010\213",
   "Intensywno\243\237",
   "Intensidade",
   "\315\271\273e\271c\264\257\271oc\273\304",
   "Intensidad",
   "Siddet"
},

//-----------------------------------------------------------------------------
// Configuration/Display menu :
//-----------------------------------------------------------------------------

/* STR_BACKLIGHT        */ 
{
   "Podsvit",
   "Schermverlichting weergeven",
   "Backlight",
   "Taustavalo",
   "Luminosit\203",
   "Hintergrundbeleuchtung",
   "Megvil\200g\205t\200s",
   "\222\205\022\253\006\212",
   "Pod\243wietlenie",
   "Luz traseira",
   "\335p\266oc\273\304",
   "Luz posterior",
   "Aydinlatma"
},
/* STR_CONTRAST         */ 
{
   "Kontrast",
   "Contrast",
   "Contrast",
   "Kontrasti",
   "Contraste",
   "Kontrast",
   "Kontraszt",
   "\026\265\212\253\034\212",
   "Kontrast",
   "Contraste",
   "\317o\271\273pac\273\271oc\273\304",
   "Contraste",
   "Kontrast"
},
/* STR_DISPLAY_MODE     */ 
{
   "Typ zobrazen\205",
   "Maak keuze",
   "Display mode",
   "N\336yt\337n tila",
   "Mode d'affichage",
   "Display Betriebsart",
   "Megjelen\205t\203si m\207d",
   "\244\276\213\264\224\251\010\033",
   "Tryb wy\243wietlania",
   "Modo de exibi\024\023o",
   "Mo\261e\267\304 \261\264c\272\267e\307",
   "Modo de presentaci\207n",
   "Ekran modu"
},

//-----------------------------------------------------------------------------
// Configuration/Sounds menu :
//-----------------------------------------------------------------------------

//STR_SAVING           Saving
/* STR_KEYBOARD         */ 
{
   "Kl\200vesnice",
   "Toetsenbord",
   "Keyboard",
   "N\336pp\336imist\337",
   "Clavier",
   "Tastatur",
   "Billenty\363zet",
   "\020\276\236\276\213",
   "Klawiatura",
   "Teclado",
   "\317\267a\257\264a\273\274pa",
   "Teclado",
   "Tus takimi"
},

// Sounds/Saving submenu :
/* STR_SAVING_SOUNDS    */ 
{
   "Zvuky p\210i ulo\216en\205",
   "Geluiden opslaan",
   "Saving sounds",
   "Tallennus\336\336net",
   "Sauvegarder le son",
   "T\337ne abspeichern",
   "Ment\203si hangok",
   "\235\200\265\033\220\030\010\265\213",
   "D\244wi\240ki zapisu",
   "Salvando sons",
   "Coxpa\271\264\273\304 \263\257\274\266",
   "Guardando sonidos",
   "Kayit sesleri"
},
/* STR_DEFAULT_TONE     */ 
{
   "Standardn\205 zvuk",
   "Default geluid",
   "Default tone",
   "Oletus\336\336ni",
   "Tonalit\203 par d\203faut",
   "Standardsound",
   "Hiba hangja",
   "\032\205\223\006\033\220\212\276\265",
   "Domy\243lny d\244wi\240k",
   "Falha de tom",
   "To\271 c\256o\307",
   "Tono normal",
   "Uyari sesi"
},
/* STR_TONE_LIGHT       */ 
{
   "Zvuk pro lehk\215",
   "Zacht geluid",
   "Tone Light",
   "\342\336ni Kevyt",
   "Tonalit\203 lumi\025re",
   "Ton Leicht",
   "K\337nny\363 mad\200r jelz\203s",
   "\235\200\265\033\220\212\276\265(\024\006)",
   "D\244wi\240k - dla lekkich",
   "Luz de tom",
   "To\271 \267e\260\266\264x",
   "Entonaci\207n de luz",
   "Hafif sesi"
},
/* STR_TONE_OK          */ 
{
   "Zvuk pro OK",
   "Geluid OK",
   "Tone OK",
   "\342\336ni OK",
   "Tonalit\203 OK",
   "Ton OK",
   "J\207 mad\200r jelz\203s",
   "\235\200\265\033\220\212\276\265(OK)",
   "D\244wi\240k - dla OK",
   "Tom OK",
   "To\271 OK",
   "Tono Ok",
   "Normal sesi"
},
/* STR_TONE_HEAVY       */ 
{
   "Zvuk pro t\204\216k\215",
   "Geluid te sterk",
   "Tone Heavy",
   "\342\336ni Painava",
   "Tonalit\203 lourd",
   "Ton Schwer",
   "Neh\203z mad\200r jelz\203s",
   "\235\200\265\033\220\212\276\265(\033\247\010)",
   "D\244wi\240k - dla ci\240\245kich",
   "Tom Forte",
   "To\271 \273\307\262e\267\303x",
   "Tono pesado",
   "Agir sesi"
},
/* STR_VOLUME           */ 
{
   "Hlasitost",
   "Volume opslaan",
   "Volume",
   "Voimakkuus",
   "Volume",
   "Lautst\336rke",
   "Hanger\364",
   "\015\265\254\251\010",
   "G\241o\243no\243\237",
   "Volume",
   "\311po\270\266oc\273\304",
   "Volumen",
   "Ses"
},

// Sound/Keyboard submenu :
/* STR_KEYBOARD_SOUNDS  */ 
{
   "Zvuky kl\200vesnice",
   "Geluiden toetsenbord",
   "Keyboard sounds",
   "N\336pp\336in\336\336net",
   "Son du clavier",
   "Tastatursignal",
   "Billenty\363zet hangja",
   "\020\276\236\276\213\220\030\010\265\213",
   "D\244wi\240k klawiatury",
   "Sons do teclado",
   "To\271 \266\267a\257\264a\273\274p\303",
   "Sonidos del teclado",
   "Tus sesi"
},
/* STR_TONE             */ 
{
   "T\207n",
   "Geluid",
   "Tone",
   "\342\336ni",
   "Tonalit\203",
   "Ton",
   "Hang",
   "\212\276\265",
   "D\244wi\240k",
   "Tom",
   "To\271",
   "Tono",
   "Ton"
},
//STR_VOLUME           Volume
/* STR_SPECIAL_SOUNDS   */ 
{
   "Speci\200ln\205 zvuky",
   "Speciale geluiden",
   "Special sounds",
   "Erikois\336\336net",
   "Sons speciaux",
   "Spezialt\337ne",
   "Speci\200lis hangok",
   "\212\022\032\247\015\265",
   "D\244wi\240ki specjalne",
   "Sons especiais",
   "C\272e\276\264a\267\304\271\303\265 \263\257\274\266",
   "Sonidos especiales",
   "Ozel sesler"
},

//-----------------------------------------------------------------------------
// Configuration/Statistics menu :
//-----------------------------------------------------------------------------

/* STR_UNIFORMITY_RANGE */ 
{
   "Rozsah uniformity",
   "Uniformiteit instellen",
   "Uniformity range",
   "Yhdenmukaisuuden alue",
   "Uniformit\203 de l'\203chelle",
   "Bereich der Gleichm\336\341igkeit",
   "Egy\337ntet\363s\203g",
   "\020\265\006\206\221\265\006",
   "Zakres jednorodno\243ci",
   "Limite de uniformidade",
   "O\261\264\271a\266o\257\303\265 \261\270a\272a\263o\271",
   "Rango uniforme",
   "Uniformite araligi"
},
/* STR_HISTOGRAM_MODE   */ 
{
   "Re\216im histogramu",
   "Histogram",
   "Histogram mode",
   "Histogrammin tila",
   "Mode de l'histogramme",
   "Histogramm Modus",
   "Grafikon m\207d",
   "\224\034\212\023\253\242\244\276\213",
   "Tryb histogramu",
   "Modo de histograma",
   "Mo\261e\267\304 \260pa\275\264\266o\257",
   "Modalidad de histrograma",
   "Grafik modu"
},
/* STR_HISTOGRAM_RANGE  */ 
{
   "Rozsah histogramu",
   "Histogram",
   "Histogram range",
   "Histogrammin alue",
   "\222chelle de l'histogramme",
   "Histogramm Bereich",
   "Grafikon",
   "\224\034\212\023\253\242\221\265\006",
   "Zakres histogramu",
   "Limite de histograma",
   "\312\264a\272a\263o\271 \260pa\275\264\266o\257",
   "Rando de histograma",
   "Grafik araligi"
},
/* STR_HISTOGRAM_STEP   */ 
{
   "Krok histogramu",
   "Histogram stap",
   "Histogram step",
   "Histogrammin porrastus",
   "Pas de l'histogramme",
   "Histogramm Schritt",
   "Grafikon l\203p\203s",
   "\224\034\212\023\253\242\034\210\205\231",
   "Krok histogramu",
   "Passo de histograma",
   "B\257o\261 \260pa\275\264\266a",
   "Paso de histograma",
   "Grafik adimi"
},

//-----------------------------------------------------------------------------
// Maintenance menu :
//-----------------------------------------------------------------------------

/* STR_SCALE_NAME       */ 
{
   "Jm\203no v\200hy",
   "Naam weegschaal",
   "Scale name",
   "Laitteen nimi",
   "Nom du plateau",
   "Waagenname",
   "M\203rleg neve",
   "\201\006\033\247\010\024\006\243\006",
   "Nazwa wagi",
   "Escalar nomes",
   "\315\270\307 \257eco\257",
   "Nombre de la balanza",
   "Terazi ismi"
},
/* STR_COUNTRY          */ 
{
   "Zem\204",
   "Land",
   "Country",
   "Maa",
   "Pays",
   "Land",
   "Orsz\200g",
   "\022\215",
   "Kraj",
   "Pa\205s",
   "C\273pa\271a",
   "Pa\205s",
   "Ulke"
},
//STR_WEIGHING                Weighing
/* STR_MEMORY           */ 
{
   "Pam\204\212",
   "Geheugen",
   "Memory",
   "Muisti",
   "M\203moire",
   "Speicher",
   "Mem\207ria ",
   "\243\244\254\276",
   "Pami\240\237",
   "Memoria ",
   "\321a\270\307\273\304 \263a\272o\267\271e\271a",
   "Memoria",
   "Hafiza"
},
/* STR_PRINTER          */ 
{
   "Tisk\200rna",
   "Printer",
   "Printer",
   "Tulostin",
   "Imprimante",
   "Drucker",
   "Nyomtat\207",
   "\231\254\265\201\276",
   "Drukarka",
   "Impressora",
   "\321p\264\271\273ep",
   "Impresor",
   "Yazici"
},
/* STR_AUTO_POWER_OFF   */ 
{
   "Automatick\203 vypnut\205",
   "Uitschakelen",
   "Auto power off",
   "Virrans\336\336st\337",
   "Alimentation auto",
   "Automatische Abschaltung",
   "Automata kikapcsol\200s",
   "\033\213\010\211\265\025\265\015\227",
   "Automatyczne wy\241. ",
   "Auto power off",
   "A\257\273o\270a\273\264\277ec\266oe \257\303\266\267",
   "Apagado autom\200tico",
   "Otomatik kapatma"
},
/* STR_PASSWORD         */ 
{
   "Heslo",
   "Wachtwoord",
   "Password",
   "Salasana",
   "Mot de passe",
   "Passwort",
   "Jelsz\207",
   "\223\034\261\276\213",
   "Has\241o",
   "Senha",
   "\321apo\267\304",
   "Contrase\033a",
   "Sifre"
},
/* STR_RESTORE_FACTORY_DEFAULTS */ 
{
   "Obnovit tov\200rn\205 nastaven\205",
   "Alles opnieuw instellen",
   "Restore factory defaults",
   "Palauta tehdasasetukset",
   "Restaurer param\025tres d\203faut",
   "Auf Werkseinstellungen zur\340ck",
   "Gy\200ri be\200ll\205t\200s vissza\200ll\205t\200sa",
   "\032\251\020\036\205\210\006\215\244\213\034",
   "Przywr\207\237 ustawienia domy\243lne",
   "Restaurar falhas de f\200brica",
   "Boc\273a\271o\257\264\273\304 \275a\256p\264\277\271\303e \274c\273a\271o\257.",
   "Restaurar los factores de f\200brica",
   "Fabrika ayarlarina don"
},

// Service menu messages :
/* STR_ENTER_SCALE_NAME */ 
{
   "Zadejte jm\203no v\200hy",
   "Voer naam weegschaal in",
   "Enter scale name",
   "Anna laitteen nimi",
   "Entrer le nom du plateau",
   "Waagenname eingeben",
   "M\203rleg nev\203nek megad\200sa",
   "\201\006\033\247\010\024\006\243\006\264\215\247\010\254\251\022",
   "Wprowad\244 nazw\240 wagi",
   "Entrar em nome de escala",
   "B\257o\261 \264\270e\271\264 \257eco\257",
   "Introduzca nombre de la balanza",
   "Terazi ismi girin"
},
/* STR_REALLY_RESTORE_FACTORY_DEFAULTS */ 
{
   "Obnovit tov\200rn\205 nastaven\205 ?",
   "Echt alles opnieuw instellen?",
   "Really restore factory defaults ?",
   "Haluatko varmasti palauttaa\ntehdasasetukset?",
   "Voulez-vous vraiment restaurer\nles param\025tres par d\203faut?",
   "Wirklich auf Herstellereinstellungen\nzur\340cksetzen?",
   "Gy\200ri be\200ll\205t\200s vissza\200ll\205t\200sa ?",
   "\235\265\212\010\215\032\251\020\036\205\210\n\006\215\244\213\032\240\034\016?",
   "Czy na pewno chcesz\nprzyr\207ci\237 ustawienia domy\243lne?",
   "Relamente restaurar falhas\nde f\200brica?",
   "Boc\273a\271o\257\264\273\304 \274c\273a\271o\257\266\264?",
   "\360Realmente restaurar\nlos factores de f\200brica?",
   "Fabrika ayarlarina\ndonulsun mu?"
},

//-----------------------------------------------------------------------------
// Maintenance/Weighing menu :
//-----------------------------------------------------------------------------

/* STR_UNITS            */ 
{
   "Jednotky",
   "Units",
   "Units",
   "Yksik\337t",
   "Unit\203s",
   "Einheiten",
   "M\203rt\203kegys\203gek",
   "\201\265\006",
   "Jednostki",
   "Unidades",
   "E\261\264\271\264\276a \264\263\264epe\271\264\307",
   "Unidades",
   "Olcu birimi"
},
/* STR_DIVISION         */ 
{
   "D\205lek",
   "Afdeling",
   "Division",
   "Tarkkuus",
   "Pr\203cision",
   "Teilung",
   "N\203zet",
   "\230\265\255\006",
   "Podzia\241",
   "Divis\023o",
   "To\277\271oc\273\304",
   "Divisi\207n",
   "Hassasiyet"
},
/* STR_RANGE            */ 
{
   "V\200\216ivost",
   "Capaciteit",
   "Capacity",
   "Kapasiteetti",
   "Capacit\203",
   "Kapazit\336t",
   "Kapacit\200s",
   "\030\006\202\006\252\010\254\251\010",
   "Wydajno\243\237",
   "Capacidade",
   "\321pe\261e\267",
   "Capacidad",
   "Kapasite"
},
/* STR_CALIBRATION      */ 
{
   "Kalibrace",
   "Calibratie",
   "Calibration",
   "Kalibrointi",
   "Calibrage",
   "Kalibrierung",
   "Kalibr\200l\200s",
   "\243\244\254",
   "Kalibracja",
   "Calibra\024\023o",
   "\317a\267\264\256po\257\266a",
   "Calibraci\207n",
   "Kalibrasyon"
},

//-----------------------------------------------------------------------------
// Maintenance/Memory menu :
//-----------------------------------------------------------------------------

/* STR_CAPACITY         */ 
{
   "Kapacita",
   "Capaciteit",
   "Capacity",
   "Kapasiteetti",
   "Capacit\203",
   "Kapazit\336t",
   "Kapacit\200s",
   "\243\244\254\276\252\010\254\251\010",
   "Pojemno\243\237 pami\240ci",
   "Capacidade",
   "\321pe\261e\267",
   "Capacidad de memoria",
   "Kapasite"
},
/* STR_FORMAT           */ 
{
   "Form\200tovat",
   "Geheugen weegschaal formatteren",
   "Format",
   "Alustus",
   "Formatage",
   "Formatieren",
   "Format\200l\200s",
   "\032\251\020\016",
   "Format",
   "Formatar",
   "\323op\270a\273",
   "Formato",
   "Format"
},

// Memory menu messages :
/* STR_REALLY_FORMAT    */ 
{
   "Opravdu form\200tovat ?",
   "Echt formatteren?",
   "Really format ?",
   "Haluatko varmasti alustaa?",
   "Voulez-vous vraiment formater?",
   "Wirklich formatieren?",
   "T\203nyleg format\200lja a g\203pet?",
   "\235\265\212\010\215\032\251\020\016\032\240\034\016?",
   "Czy na pewno chcesz\nzmieni\237 format?",
   "Realmente formatar?",
   "\321pa\257\264\267\304\271\303\265 \275op\270a\273?",
   "\360Realmente formato?",
   "Gercekten format atilsin mi?"
},

//-----------------------------------------------------------------------------
// Maintenance/Printer menu :
//-----------------------------------------------------------------------------

/* STR_PAPER_WIDTH      */ 
{
   "\230\205\210ka pap\205ru",
   "Papier breedte",
   "Paper width",
   "Paperin leveys",
   "\222paisseur du papier?",
   "Papierbreite",
   "Pap\205r sz\203less\203g",
   "\006\265\030\206\032\220\221\222",
   "Szeroko\243\237 papieru",
   "Largura do papel",
   "\326\264p\264\271a \256\274\270a\260\264",
   "Ancho del papel",
   "Kag\361t genisligi"
},
/* STR_COMMUNICATION_SPEED */ 
{
   "Rychlost komunikace",
   "Communicatie",
   "Communication speed",
   "Yhteysnopeus",
   "Vitesse de comm.",
   "\344bertragungsgeschw.",
   "Kommunik\200ci\207 sebess\203ge",
   "\206\010\032\265\177\022\213",
   "Pr\240dko\243\237 komunikacji",
   "Velocidade de Comun.",
   "C\266opoc\273\304 coe\261\264\271e\271\264\307",
   "Rapidez de comunicaci\207n",
   "Baglanti hizi"
},
/* STR_COMMUNICATION_FORMAT */ 
{
   "Form\200t komunikace",
   "Communicatie formatteren",
   "Communication format",
   "Yhteysformaatti",
   "Format de communication",
   "\344bertragungsformat",
   "Kommunik\200ci\207s forma",
   "\206\010\032\265\024\006\032\020",
   "Format komunikacji",
   "Formato de Comunica\024\023o",
   "\323op\270a\273 coe\261\264\271e\271\264\307",
   "Formato de comunicaci\207n",
   "Baglanti formati"
},

//-----------------------------------------------------------------------------
// Maintenance/Country menu :
//-----------------------------------------------------------------------------

//STR_COUNTRY             Country
/* STR_LANGUAGE         */ 
{
   "Jazyk",
   "Taal",
   "Language",
   "Kieli",
   "Langage",
   "Sprache",
   "Nyelv",
   "\025\265\027",
   "J\240zyk",
   "L\205ngua",
   "\335\263\303\266",
   "Lenguaje",
   "Dil"
},
/* STR_DATE_FORMAT      */ 
{
   "Form\200t data",
   "Datum formatteren",
   "Date format",
   "Pvm muoto",
   "Format de date",
   "Datumsformat",
   "D\200tum forma",
   "\224\207\024\024\006\032\020",
   "Format daty",
   "Formato de Data",
   "\323op\270a\273 \261a\273\303",
   "Fecha del formato",
   "Tarihi sil"
},
/* STR_TIME_FORMAT      */ 
{
   "Form\200t \201asu",
   "Tijd formatteren",
   "Time format",
   "Ajan muoto",
   "Format de l'heure",
   "Zeitformat",
   "Id\364 forma",
   "\033\016\265\224\251\010\033\024\006\032\020",
   "Format godziny",
   "Formato de tempo",
   "\323op\270a\273 \257pe\270e\271\264",
   "Hora del formato",
   "Saati sil"
},
/* STR_DAYLIGHT_SAVING_TIME */ 
{
   "Letn\205 \201as",
   "Winter-of zomertijd instellen",
   "Daylight saving",
   "Kes\336aika",
   "Saison de sauvegarde",
   "Sommer-/ Winterzeit",
   "Napszak ment\203s",
   "\215\205\032\251\010\033\016\265\036\265\201\022",
   "Zapisywanie dzienne",
   "Hor\200rio de ver\023o",
   "\312\267\264\273e\267\304\271oc\273\304 \261\271\307",
   "Ahorro de luz del d\205a",
   "Sezonu gir"
},

// Country menu messages :
/* STR_ENTER_DATE_FORMAT */ 
{
   "Zadejte form\200t data",
   "Kies vorm van datum",
   "Enter date format",
   "Anna pvm muoto",
   "Entrer le format de la date",
   "Datumsformat eingeben",
   "D\200tumform\200tum megad\200sa",
   "\224\207\024\024\006\032\020\264\215\247\010\254\251\022",
   "Wprowad\244 format daty",
   "Entrar em formato de Data",
   "B\257o\261 \275op\270a\273a \261a\273\303",
   "Introduzca el formato de fecha",
   "Tarih formati gir"
},
/* STR_ENTER_DATE_SEPARATOR1 */ 
{
   "Prvn\205 odd\204lova\201",
   "Eerste datumomschrijving",
   "First separator",
   "Ensimm\336inen erotinmerkki",
   "Premi\025re s\203paration",
   "Erstes Trennzeichen",
   "Els\364 elv\200laszt\207",
   "\230\265\254\2271",
   "Piewszy separator",
   "Primeiro Separador",
   "\321ep\257oe pa\263\261e\267e\271\264e",
   "Primer separador",
   "Tarih ayiraci"
},
/* STR_ENTER_DATE_SEPARATOR2 */ 
{
   "Druh\215 odd\204lova\201",
   "Tweede datumomschrijving",
   "Second separator",
   "Toinen erotinmerkki",
   "Seconde s\203paration",
   "Zweites Trennzeichen",
   "M\200sodik elv\200laszt\207",
   "\230\265\254\2272",
   "Drugi separator",
   "Segundo Separador",
   "B\273opoe pa\263\261e\267e\271\264e",
   "Segundo separador",
   "Saniye ayiraci"
},
/* STR_ENTER_TIME_FORMAT */ 
{
   "Zadejte form\200t \201asu",
   "Kies vorm van tijd",
   "Enter time format",
   "Anna ajan muoto",
   "Entrer le format de l'heure",
   "Zeitformat eingeben",
   "D\200tumform\200tum megad\200sa",
   "\033\016\265\024\006\032\020\302\215\247\010\254\251\022",
   "Wprowad\244 format godziny",
   "Entrar em formato de tempo",
   "B\257o\261 \275op\270a\273a \257pe\270e\271\264 ",
   "Introduzca el formato de hora",
   "Zaman formati gir"
},
/* STR_ENTER_TIME_SEPARATOR */ 
{
   "Zadejte odd\204lova\201",
   "Maak uw keuze",
   "Enter separator",
   "Anna erotinmerkki",
   "Entrer la s\203paration",
   "Trennzeichen eingeben",
   "Elv\200laszt\207 megad\200sa",
   "\230\265\254\227\264\215\247\010\254\251\022",
   "Wprowad\244 separator",
   "Entrar em separador",
   "B\257o\261 pa\263\261e\267e\271\264\307",
   "Introduzca separador",
   "Ayirac gir"
},

//-----------------------------------------------------------------------------
// Saving options menu :
//-----------------------------------------------------------------------------

//STR_MODE                  Mode
/* STR_WEIGHING_MORE_BIRDS */ 
{
   "V\200\216it v\205ce kus\214",
   "Meer dieren tegelijkertijd wegen",
   "More birds",
   "Useampi lintu",
   "Plus d'animaux",
   "Weitere V\337gel",
   "T\337bb mad\200r",
   "\221\034\010\264\227\246\034",
   "Wi\240cej ptak\207w",
   "Mais aves",
   "\310o\267\304\300e \272\273\264\276",
   "Mas aves",
   "Onceki tartimlar"
},
//STR_NUMBER_OF_BIRDS       Number of birds
/* STR_WEIGHT_SORTING   */ 
{
   "T\210\205d\204n\205",
   "Sorteren op gewicht",
   "Weight sorting",
   "Punnitusten j\336rjestys",
   "Tri du poids",
   "Nach Gewicht sortieren",
   "S\213ly szerinti csoportok",
   "\201\006\033\247\010\233\206\215\036\265\233\206",
   "Sortowanie pomiar\207w wagi",
   "Classifica\024\023o",
   "Cop\273\264po\257\266a \257eca",
   "Clasificacion de peso",
   "Siniflama tartimi"
},
/* STR_SORTING          */ 
{
   "T\210\205d\204n\205",
   "Sorteren",
   "Sorting",
   "J\336rjestys",
   "Tri",
   "Sortieren",
   "Szort\205roz\200s",
   "\036\265\233\206",
   "Sortowanie",
   "Classifica\024\023o",
   "Cop\273\264po\257\266a",
   "Clasificar",
   "Siniflama"
},
/* STR_FILTER           */ 
{
   "Filtr",
   "Filter",
   "Filter",
   "Suodatus",
   "Filtre",
   "Filter",
   "Sz\363r\364",
   "\227\005\255\201\276",
   "Filtr",
   "Filtro",
   "\323\264\267\304\273p",
   "Filtro",
   "Boylama kriterleri"
},
/* STR_STABILISATION_TIME */ 
{
   "\220as ust\200len\205",
   "Stabilisatie tijd",
   "Stabilisation time",
   "Stabiloitumisaika",
   "Temps de stabilisation",
   "Stabilisierungszeit",
   "Stabiliz\200ci\207s id\364",
   "\004\265\210\006\033\016\265",
   "Czas stabilizacji",
   "Estabiliza\024\023o de tempo",
   "C\273a\256\264\267\264\263a\276\264\307 \257pe\270e\271\264",
   "Tiempo de estabilizaci\207n",
   "Sabitleme zamani"
},
/* STR_MINIMUM_WEIGHT   */ 
{
   "Minim\200ln\205 hmotnost",
   "Minimum gewicht",
   "Minimum weight",
   "Minimipaino",
   "Poids minimum",
   "Minimalgewicht",
   "Minimum s\213ly",
   "\030\006\210\006\201\006\033\247\010",
   "Minimalna waga",
   "Peso minimo",
   "M\264\271\264\270a\267\304\271\303\265 \257ec",
   "Peso m\205nimo",
   "Minimum agirlik"
},
/* STR_STABILISATION_RANGE */ 
{
   "Rozsah ust\200len\205",
   "Stabilisatie bereik",
   "Stabilisation range",
   "Stabiloitumisalue",
   "Stabilisation de l'\203chelle",
   "Stabilisierungsbereich",
   "Stabiliz\200ci\207s fok",
   "\004\265\210\006\221\265\006",
   "Zakres stabilizacji",
   "Estabiliza\024\023o de limite",
   "C\273a\256\264\267\264\263a\276\264\307 \261\264a\272a\263o\271a",
   "Rango de estabilizaci\207n",
   "Sabitleme araligi"
},
/* STR_RESET_DEFAULTS   */ 
{
   "Nastavit v\215choz\205 hodnoty",
   "Reset defaults",
   "Reset defaults",
   "Palauta oletusasetukset",
   "Reset d\203fauts",
   "Auf Standardwerte zur\340cksetzen",
   "Hiba t\337rl\203se",
   "\254\036\205\212\211\227\014\255\212",
   "Resetuj domy\243lne",
   "Ressetar falhas",
   "C\256op c\256o\307",
   "Restaurar valores",
   "Standartlari degistir"
},
/* STR_ENABLE_FILE_PARAMETERS */ 
{
   "Parametry souboru",
   "Bestandsindeling activeren",
   "File parameters",
   "Tiedoston parametrit",
   "Param\025tre du fichier",
   "Dateiparameter",
   "Adat param\203terek",
   "\227\003\006\255\223\253\243\276\201\276",
   "Parametry pliku",
   "Parametros de arquivos",
   "\321apa\270e\273p\303 \275a\265\267a",
   "Par\200metros de los archivos",
   "Parametreler"
},
/* STR_COPY_TO_FILE     */ 
{
   "Kop\205rovat do soubor\214",
   "Naar bestand kopieren",
   "Copy to file",
   "Kopioi tiedostoon",
   "Copier les param\025tres sur fichier",
   "In Datei kopieren",
   "M\200sol\200s file-ba",
   "\227\003\006\255\264\026\226\276",
   "Kopiuj do pliku",
   "Copiar para arquivo",
   "\317o\272\264po\257a\273\304 \275a\265\267",
   "Copiar a un archivo",
   "Dosyaya kopyala"
},

// Saving options menu messages :
/* STR_REALLY_RESET_DEFAULTS */ 
{
   "Opravdu nastavit v\215choz\205\nhodnoty ?",
   "Eacht alles opnieuw instellen?",
   "Really reset defaults ?",
   "Haluatko varmasti palauttaa\noletusasetukset?",
   "Voulez-vous vraiment restaurer les\nparam\025tres par d\203faut?",
   "Wirkich auf Standardwerte\nzur\340cksetzen?",
   "Hiba t\337rl\203se?",
   "\235\265\212\010\215\032\251\020\036\205\210\n\006\215\244\213\032\240\034\016?",
   "Czy na pewno chcesz\nresetowa\237 domy\243lne?",
   "Relamente ressetar falhas?",
   "C\256poc\264\273\304 c\256o\265?",
   "\360Realmente restaurar valores?",
   "Standartlar degistirilsin mi?"
},
/* STR_REALLY_COPY_TO_FILES */ 
{
   "Opravdu kop\205rovat do soubor\214 ?",
   "Echt naar bestanden kopieren?",
   "Really copy to files ?",
   "Haluatko varmasti kopioida\ntiedostoihin?",
   "Voulez-vous vraiment copier ces\nparam\025tres sur fichier?",
   "Wirklich in Datei kopieren?",
   "Val\207ban m\200solja?",
   "\235\265\212\010\215\227\003\006\255\264\n\026\226\276\032\240\034\016?",
   "Czy na pewno chcesz\nkopiowa\237 do plik\207w?",
   "Relamente copiar para arquivos?",
   "C\266o\272\264po\257a\273\304 \275a\265\267?",
   "\360Realmente copiar a los archivos?",
   "Standartlar dosyaya\nkopyalansin mi?"
},

//-----------------------------------------------------------------------------
// Weight sorting menu :
//-----------------------------------------------------------------------------

//STR_WEIGHT_SORTING    Weight sorting
//STR_MODE            Mode
//STR_LIMIT           Limit
//STR_LOW_LIMIT       Low limit
//STR_HIGH_LIMIT      High limit

//-----------------------------------------------------------------------------
// Calibration menu :
//-----------------------------------------------------------------------------

// calibration menu messages :
/* STR_ENTER_WEIGHT     */ 
{
   "Zadejte hmotnost",
   "Voer gewicht in",
   "Enter weight",
   "Anna paino",
   "Entrer le poids",
   "Gewicht eingeben",
   "S\213ly megad\200sa",
   "\201\006\033\247\010\264\215\247\010\254\251\022",
   "Wprowad\244 wag\240",
   "Entrar peso",
   "B\257ec\273\264 \257ec",
   "Introduzca peso",
   "Agirlik gir"
},
/* STR_RELEASE_WEIGHT   */ 
{
   "Odleh\201ete v\200hu",
   "Alles van de haak verwijderen",
   "Remove all from the hook",
   "Tyhjenn\336 koukku",
   "Liberer le poids du plateau",
   "Alles vom Haken entfernen",
   "Vegye le a kamp\207r\207l",
   "\227\205\022\264\227\254\276\215\034\255",
   "Usu\242 wszystko z haka",
   "Remover tudo do quadro",
   "C\271\264\270\264\273e \257c\027 c \272o\261\257ec\266\264",
   "Retire todo del gancho",
   "Kancadan tum agirliklari kaldir"
},

//-----------------------------------------------------------------------------
// Password :
//-----------------------------------------------------------------------------

/* STR_PROTECT_BY_PASSWORD */ 
{
   "Chr\200nit heslem ?",
   "Met wachtwoord beschermen?",
   "Protect by password ?",
   "Suojaa salasanalla?",
   "Protection par mot de passe?",
   "Mit Passwort sch\340tzen?",
   "V\203dje jelsz\207?",
   "\223\034\261\276\213\211\235\027\032\240\034\016?",
   "Chroni\237 has\241em?",
   "Proteger por senha?",
   "B\257ec\273\264 \272apo\267\304?",
   "Protegido por una contrase\033a?",
   "Sifreyi koru?"
},
/* STR_ENTER_PASSWORD   */ 
{
   "Zadejte heslo",
   "Voer wachtwoord in",
   "Enter password",
   "Anna salasana",
   "Entrer un mot de passe",
   "Passwort eingeben",
   "Jelsz\207 megad\200sa",
   "\223\034\261\276\213\264\215\247\010\254\251\022",
   "Wprowad\244 has\241o",
   "Entrar em senha",
   "B\257o\261 \272apo\267\307",
   "Introduzca contrase\033a",
   "Sifre gir"
},
/* STR_ENTER_NEW_PASSWORD */ 
{
   "Zadejte nov\203 heslo",
   "Voer nieuw wachtwoord in",
   "Enter new password",
   "Anna uusi salasana",
   "Entrer un nouveau mot de passe",
   "Neues Passwort eingeben",
   "\232j jelsz\207 megad\200sa",
   "\004\201\253\032\006\223\034\261\276\213\264\215\247\010\254\251\022\034\255",
   "Wprowad\244 nowe has\241o",
   "Entrar nova senha",
   "B\257o\261 \271o\257o\260o \272apo\267\307",
   "Introduzca nueva contrase\033a",
   "Yeni sifre gir"
},
/* STR_CONFIRM_PASSWORD */ 
{
   "Potrvr\202te heslo",
   "Bevestig wachtwoord",
   "Confirm password",
   "Vahvista salasana",
   "Confirmer le mot de passe",
   "Passwort best\336tigen",
   "Jelsz\207 j\207v\200hagy\200sa",
   "\223\034\261\276\213\264\016\022\215\265\034\255",
   "Potwierd\244 has\241o",
   "Confirmar senha",
   "\321o\261\273\257ep\261\264\273\304 \272apo\267\304",
   "Confirme contrase\033a",
   "Sifreyi tekrar gir"
},
/* STR_PASSWORDS_DONT_MATCH */ 
{
   "Hesla nesouhlas\205",
   "Wachtwoorden verschillen",
   "Passwords don't match",
   "Salasana ei t\336sm\336\336",
   "Le mot de passe ne correspond\npas",
   "Passwort stimmt nicht\n\340berein",
   "Jelszavak nem egyeznek",
   "\223\034\261\276\213\017\240\203\017\205\210\006\240\034",
   "Has\241a si\240 nie zgadzaj\236",
   "Senhas n\023o confirmam",
   "\321apo\267\304 \271e co\257\272a\261ae\273",
   "Contrase\033as no coinciden",
   "Ilk sifreyi tutmuyor"
},
/* STR_INVALID_PASSWORD */ 
{
   "Neplatn\203 heslo",
   "Onjuist wachtwoord",
   "Invalid password",
   "V\336\336r\336 salasana",
   "Mot de passe invalide",
   "Ung\340ltiges Passwort",
   "\222rv\203nytelen jelsz\207",
   "\242\026\010\214\223\034\261\276\213",
   "Nieprawid\241owe has\241o",
   "Senha inv\200lida",
   "He \257ep\271\303\265 \272apo\267\304",
   "Contrase\033a incorrecta",
   "Gecersiz sifre"
},

//-----------------------------------------------------------------------------
// Printer :
//-----------------------------------------------------------------------------

/* STR_PRINTING         */ 
{
   "Tisk...",
   "Afdrukken...",
   "Printing...",
   "Tulostaa...",
   "Impression...",
   "Drucken...",
   "Nyomtat\200s...",
   "\006\265\030\206\203\247\010",
   "Drukowanie...",
   "Imprimindo...",
   "\321e\277a\273ae\273c\307...",
   "Imprimiendo...",
   "Yaziliyor..."
},
/* STR_START_PRINTING   */ 
{
   "Zah\200jit tisk ?",
   "Beginnen met afdrukken?",
   "Start printing ?",
   "Aloita tulostus?",
   "D\203buter l'impression?",
   "Drucken starten?",
   "Nyomtat\200s kezd\203se?",
   "\006\265\030\206\264\016\006\032\032\240\034\016?",
   "Rozpocz\236\237 drukowanie?",
   "Come\024ar a imprimir?",
   "Ha\277a\267o \272e\277a\273\264?",
   "Empezar a imprimir?",
   "Yazdirmayi baslat?"
},

//-----------------------------------------------------------------------------
// Screen
//-----------------------------------------------------------------------------

/* STR_MEMORY_CAPACITY  */ 
{
   "Kapacita pam\204ti",
   "Geheugen capaciteit",
   "Memory capacity",
   "Muistin kapasiteetti",
   "Capacit\203 de la m\203moire",
   "Speicherkapazit\336t",
   "Mem\207ria kapacit\200s",
   "\243\244\254\276\252\010\254\251\010",
   "Pojemno\243\237 pami\240ci",
   "Capacidade de Mem\207ria",
   "\006\270\266oc\273\304 \272a\270\307\273\264",
   "Capacidad de la memoria",
   "Hafiza durumu"
},
/* STR_FILES_COUNT      */ 
{
   "Po\201et soubor\214",
   "Aantal bestanden",
   "Number of files",
   "Tiedostojen lukum\336\336r\336",
   "Nombre de fichiers",
   "Anzahl der Dateien",
   "File-ok sz\200ma",
   "\227\003\006\255\220\016\035",
   "Ilo\243\237 plik\207w",
   "N\213mero de arquivos",
   "Ho\270ep \275a\265\267a",
   "N\213mero de archivos",
   "Dosya adeti"
},
/* STR_FILES_SIZE       */ 
{
   "Po\201et hmotnost\205",
   "Aantal gewichten",
   "Number of weights",
   "Punnitusten lukum\336\336r\336",
   "Nombre de pes\203es",
   "Anzahl der Gewichtswerte",
   "M\203r\203sek sz\200ma",
   "\201\006\033\247\010\034\010",
   "Ilo\243\237 ptak\207w",
   "N\213mero de pesos",
   "Ho\270ep \257\263\257e\300\264\257a\271\264\307",
   "N\213mero de pesos",
   "Hayvan adedi"
},
/* STR_GROUPS_COUNT     */ 
{
   "Po\201et skupin",
   "Aantal groepen",
   "Number of groups",
   "Ryhmien lukum\336\336r\336",
   "Nombre de groupe",
   "Anzahl der Gruppen",
   "Csoportok sz\200ma",
   "\023\255\276\231\034\010",
   "Ilo\243\237 grup",
   "N\213mero de grupos",
   "Ho\270ep \260p\274\272\272",
   "N\213mero de grupos",
   "Grup adeti"
},
/* STR_MEMORY_USED      */ 
{
   "Pou\216it\200 pam\204\212",
   "Gebruikt geheugen",
   "Memory used",
   "Muistia k\336ytetty",
   "M\203moire utilis\203e",
   "Belegter Speicher",
   "Foglalt mem\207ria",
   "\032\252\010\203\247\010\220\243\244\254\276",
   "Zaj\240to\243\237 pami\240ci",
   "Mem\207ria utilizada",
   "\321o\267\304\263o\257a\273e\267\304",
   "Memoria utilizada",
   "Kullanilan hafiza"
},
// system :
/* _STR_LAST */
{
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?",
   "?"
}
};
