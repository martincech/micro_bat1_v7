//******************************************************************************
//                                                                            
//  PMan.h         Power management utilities
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __PMan_H__
   #define __PMan_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//------------------------------------------------------------------------------
// Executive
//------------------------------------------------------------------------------

void PManInit( void);
// Initialize power management

void PManExecute( void);
// Main executive, call periodically

void PManExecuteSlow( void);
// Main executive, call periodically by 1s

void PManSetShutdown( void);
// Prepare power off mode

void PManClrShutdown( void);
// Leave power off mode

void PManSetPowerOn( void);
// Power on request

void PManSetPowerOff( void);
// Power off request

void PManPowerOff( void);
// Switch power off

//------------------------------------------------------------------------------
// Get status
//------------------------------------------------------------------------------

TYesNo PManBoot( void);
// Returns YES on boot key

TYesNo PManPowerOn( void);
// Returns YES on key power on

TYesNo PManShutdown( void);
// Returns YES for shutdown

TYesNo PManAccuWarning( void);
// Returns YES for low accu voltage

TYesNo PManAccuFail( void);
// Returns YES for discharged accu

TYesNo PManUsb( void);
// Returns YES for active USB

TYesNo PManCharger( void);
// Returns YES for connected charger

TYesNo PManCharged( void);
// Returns YES for charging done

TYesNo PManChargeOnly( void);
// Returns YES for USB/charger (without power on button)

int PManAccuCapacity( void);
// Returns accu capacity [%]

int PManAccuVoltage( void);
// Returns accu voltage [mV]

#endif
