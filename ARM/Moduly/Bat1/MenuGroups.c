//******************************************************************************
//                                                                            
//   MenuGroups.c   File groups menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "MenuGroups.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/File/Fd.h"       // File directory list

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DDir.h"             // Display file directory

#include "Str.h"                  // Strings
#include "SdbDef.h"               // Class only
#include "Group.h"                // File Groups
#include <string.h>

static DefMenu( FileGroupsMenu)
   STR_CREATE,
   STR_EDIT_FILE_LIST,
   STR_EDIT_NOTE,
   STR_RENAME,
   STR_DELETE,
EndMenu()

typedef enum {
   FG_CREATE,
   FG_EDIT_FILE_LIST,
   FG_EDIT_NOTE,
   FG_RENAME,
   FG_DELETE,
} TFileGroupsMenuEnum;

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuFileGroups( void)
// File groups menu
{
TMenuData   MData;
TFdirHandle Handle;
char        Name[ FDIR_NAME_LENGTH + 1];
char        Note[ FDIR_NOTE_LENGTH + 1];
TGroup      Group;

   DMenuClear( MData);
   FdSetClass( GRP_CLASS);             // group directory class
   forever {
      if( !DMenu( STR_FILE_GROUPS, FileGroupsMenu, 0, 0, &MData)){
         FdSetClass( SDB_CLASS);                 // samples database directory class
         return;
      }
      if( MData.Item != FG_CREATE){
         // select file :
         Handle = DDirSelectGroup( STR_SELECT_GROUP, FDIR_INVALID);
         if( Handle == FDIR_INVALID){
            continue;
         }
      }
      switch( MData.Item){
         case FG_CREATE :
            Name[ 0] = '\0';                     // empty string
            if( !DInputText( STR_CREATE, STR_ENTER_NAME, Name, FDIR_NAME_LENGTH, NO)){
               break;
            }
            if( FdExists( Name)){
               DMsgOk( STR_ERROR, STR_GROUP_EXISTS, Name);
               break;
            }
            GroupClear( Group);                  // empty group
            FdSetClass( SDB_CLASS);              // samples database directory class
            if( !DDirSelectSet( STR_SELECT_FILES, Group)){       // edit file set
               FdSetClass( GRP_CLASS);           // back to group
               break;
            }
            FdSetClass( GRP_CLASS);              // back to group
            if( GroupIsEmpty( Group)){
               DMsgOk( STR_ERROR, STR_NO_FILES_SELECTED, 0);
               break;                            // no files
            }
            if( !GroupCreate( Name, Group)){
               DMsgOk( STR_ERROR, STR_NOT_ENOUGH_SPACE, 0);
               break;
            }
            break;

         case FG_EDIT_FILE_LIST :
            GroupLoad( Handle, Group);           // get group file set
            FdSetClass( SDB_CLASS);              // samples database directory class
            if( !DDirSelectSet( STR_SELECT_FILES, Group)){       // edit file set
               FdSetClass( GRP_CLASS);           // back to group
               continue;
            }
            FdSetClass( GRP_CLASS);              // back to group
            if( GroupIsEmpty( Group)){
               DMsgOk( STR_ERROR, STR_NO_FILES_SELECTED, 0);
               break;                            // no files
            }
            GroupSave( Handle, Group);
            break;

         case FG_EDIT_NOTE :
            GroupLoadNote( Handle, Note);
            if( !DInputText( STR_EDIT_NOTE, STR_ENTER_NOTE, Note, FDIR_NOTE_LENGTH, YES)){
               break;
            }
            GroupSaveNote( Handle, Note);
            break;

         case FG_RENAME :
            FdGetName( Handle, Name);
            if( !DInputText( STR_RENAME, STR_ENTER_NAME, Name, FDIR_NAME_LENGTH, NO)){
               break;
            }
            if( !GroupRename( Handle, Name)){
               DMsgOk( STR_ERROR, STR_FILE_EXISTS, Name);
               break;
            }
            break;

         case FG_DELETE :
            FdGetName( Handle, Name);
            if( !DMsgYesNo( STR_DELETE, STR_REALLY_DELETE_GROUP, Name)){
               break;
            }
            GroupDelete( Handle);
            break;
      }
   }
} // MenuFileGroups
