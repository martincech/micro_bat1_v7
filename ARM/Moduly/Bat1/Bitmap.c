//*****************************************************************************
//
//    Bitmap.c - project bitmaps
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Bitmap.h"

//------------------------------------------------------------------------------
//  Off screen
//------------------------------------------------------------------------------

#include "Bmp/BmpOff.c"

//------------------------------------------------------------------------------
//  Status Line
//------------------------------------------------------------------------------

#include "Bmp/BmpBattery.c"
#include "Bmp/BmpMemory.c"

#include "Bmp/BmpCharger.c"
#include "Bmp/BmpPC.c"

#include "Bmp/BmpLightHeavy.c"
#include "Bmp/BmpLightOKHeavy.c"

#include "Bmp/BmpModeAuto.c"
#include "Bmp/BmpModeManual.c"
#include "Bmp/BmpModeManualBySex.c"

#include "Bmp/BmpMoreBirds.c"

#include "Bmp/BmpAm.c"
#include "Bmp/BmpPm.c"

//------------------------------------------------------------------------------
//  Big accu
//------------------------------------------------------------------------------

#include "Bmp/BmpAccu.c"

//------------------------------------------------------------------------------
//  Advanced
//------------------------------------------------------------------------------

#include "Bmp/BmpWeight.c"
#include "Bmp/BmpSigma.c"
#include "Bmp/BmpFile.c"
#include "Bmp/BmpLastSaved.c"
#include "Bmp/BmpCount.c"

#include "Bmp/BmpFlagMale.c"
#include "Bmp/BmpFlagFemale.c"
#include "Bmp/BmpFlagLight.c"
#include "Bmp/BmpFlagOK.c"
#include "Bmp/BmpFlagHeavy.c"

#include "Bmp/BmpAdvancedUnderflow.c"
#include "Bmp/BmpAdvancedOverflow.c"

#include "Bmp/BmpSortAdvancedNone.c"
#include "Bmp/BmpSortAdvancedLight.c"
#include "Bmp/BmpSortAdvancedOK.c"
#include "Bmp/BmpSortAdvancedHeavy.c"

//------------------------------------------------------------------------------
//  Basic
//------------------------------------------------------------------------------

#include "Bmp/BmpBasicUnderflow.c"
#include "Bmp/BmpBasicOverflow.c"

#include "Bmp/BmpSortNone.c"
#include "Bmp/BmpSortLight.c"
#include "Bmp/BmpSortOK.c"
#include "Bmp/BmpSortHeavy.c"

//------------------------------------------------------------------------------
//  Large
//------------------------------------------------------------------------------

#include "Bmp/BmpFlagLargeMale.c"
#include "Bmp/BmpFlagLargeFemale.c"
#include "Bmp/BmpFlagLargeLight.c"
#include "Bmp/BmpFlagLargeOK.c"
#include "Bmp/BmpFlagLargeHeavy.c"

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

#include "Bmp/BmpStatisticsCount.c"
#include "Bmp/BmpStatisticsWeight.c"
#include "Bmp/BmpStatisticsSigma.c"
#include "Bmp/BmpStatisticsCV.c"
#include "Bmp/BmpStatisticsUNI.c"

#include "Bmp/BmpStatisticsTotal.c"
#include "Bmp/BmpStatisticsMale.c"
#include "Bmp/BmpStatisticsFemale.c"
#include "Bmp/BmpStatisticsLight.c"
#include "Bmp/BmpStatisticsOK.c"
#include "Bmp/BmpStatisticsHeavy.c"

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

#include "Bmp/BmpHistogramCount.c"
#include "Bmp/BmpHistogramWeight.c"
#include "Bmp/BmpHistogramCursor.c"

//------------------------------------------------------------------------------
//  Compare files
//------------------------------------------------------------------------------

#include "Bmp/BmpFilesWeight.c"
#include "Bmp/BmpFilesCV.c"

//------------------------------------------------------------------------------
//  Layout
//------------------------------------------------------------------------------

#include "Bmp/BmpFileBlack.c"

#include "Bmp/BmpButtonOK.c"
#include "Bmp/BmpButtonCancel.c"
#include "Bmp/BmpButtonUp.c"
#include "Bmp/BmpButtonDown.c"
#include "Bmp/BmpButtonLeft.c"
#include "Bmp/BmpButtonRight.c"

#include "Bmp/BmpIconTotal.c"
#include "Bmp/BmpIconMale.c"
#include "Bmp/BmpIconFemale.c"
#include "Bmp/BmpIconLight.c"
#include "Bmp/BmpIconOK.c"
#include "Bmp/BmpIconHeavy.c"

#include "Bmp/BmpFileChecked.c"
#include "Bmp/BmpFileUnchecked.c"
#include "Bmp/BmpFileArrow.c"

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

#include "Bmp/BmpEditUp.c"
#include "Bmp/BmpEditDown.c"

//------------------------------------------------------------------------------
//  Wait
//------------------------------------------------------------------------------

#include "Bmp/BmpWait.c"
