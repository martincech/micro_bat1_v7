//*****************************************************************************
//
//    Kbd.c -  User keyboard
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../inc/Kbd.h"         // Keyboard prototypes
#include "../Moduly/Kbd/Kbd.c"  // Keyboard template

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void KbdInit( void)
// Inicializace
{
   KBD_DIR &= ~( KBD_K0 | KBD_K1 | KBD_K2 | KBD_ON);
} // KbdInit

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

static int ReadKey( void)
// Cteni klavesy
{
dword Value;

   // dvojstisk Esc/Left :
   Value = KBD_PIN;
   if( !(Value & KBD_ON) && !(Value & KBD_K2)){
      return( K_BOOT);
   }
   // jednotlive klavesy :
   if( !(Value & KBD_ON)){
      return( K_ESC);
   }
   if( !(Value & KBD_K0)){
      return( K_RIGHT);
   }
   if( !(Value & KBD_K1)){
      return( K_DOWN);
   }
   if( !(Value & KBD_K2)){
      return( K_LEFT);
   }
   // prvni sloupec :
   DisableInts();
   KBD_DIR |= KBD_K1;
   KBD_CLR  = KBD_K1;
   SysUDelay( 1);
   Value    = KBD_PIN;
   KBD_DIR &= ~KBD_K1;
   EnableInts();
   if( !(Value & KBD_K0)){
      return( K_ENTER);
   }
   if( !(Value & KBD_K2)){
      return( K_UP);
   }
   return( K_RELEASED);         // nic neni stisknuto
} // ReadKey
