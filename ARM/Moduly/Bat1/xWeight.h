//******************************************************************************
//                                                                            
//   xWeight.h      putchar based weight formatting
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __xWeight_H__
   #define __xWeight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

#ifndef __SdbDef_H__
   #include "SdbDef.h"       // sample flag only
#endif

#ifndef __PutcharDef_H__
   #include "../inc/PutcharDef.h"
#endif


void xWeight( TPutchar *xPutchar, TWeight Weight);
// Display weight

int xWeightWidth( void);
// Weight width (chars)

void xWeightLeft( TPutchar *xPutchar, TWeight Weight);
// Display weight left aligned

void xWeighingFlag( TPutchar *xPutchar, TSampleFlag Flag);
// Display weighing flag

void xWeighingUnits( TPutchar *xPutchar);
// Display current units

void xWeightWithUnits( TPutchar *xPutchar, TWeight Weight);
// Display weight with units

void xWeightWithUnitsLeft( TPutchar *xPutchar, TWeight Weight);
// Display weight with units left aligned

#endif
