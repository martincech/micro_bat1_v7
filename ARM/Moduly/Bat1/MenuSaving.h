//******************************************************************************
//                                                                            
//   MenuSaving.c    Saving parameters menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuSaving_H__
   #define __MenuSaving_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"
#endif

void MenuSavingParameters( TWeighingParameters *Parameters, TYesNo Global);
// Edit saving parameters. <Global> == NO : file parameters

#endif
