//*****************************************************************************
//
//    Calc.c - Statistic calculations
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
#endif

#include "Calc.h"
#include "../Inc/File/Ds.h"
#include "../Inc/Statistics.h"
#include "../Inc/Cpu.h"         // WatchDog

// Global data :
TCalc Calc[ STATISTICS_MAX];


// Local data :
static TStatistic _Statistics[ STATISTICS_MAX];
static TCalcMode  _Mode;


// Local functions :

static void CalcPrimary( void);
// Calculate primary statistics

static void CalcSecondary( void);
// Calculate secondary statistics

static void AddSample( TWeight Weight, TSampleFlag Flag);
// Add sample to primary

static void UpdatePrimary( void);
// Recalculate primary statistics

static int IndexByFlag( TSampleFlag Flag);
// Returns statistics index by <Flag> or -1 invalid

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void CalcClear(void)
// Clear results
{
int i;

   for( i = 0; i < STATISTICS_MAX; i++){  
      StatClear( &_Statistics[ i]);
      HistogramClear( &Calc[ i].Histogram);
      Calc[ i].Count      = 0;
      Calc[ i].Average    = 0;
      Calc[ i].MinWeight  = 0;
      Calc[ i].MaxWeight  = 0;
      Calc[ i].Sigma      = 0;
      Calc[ i].Cv         = 0;
      Calc[ i].Uniformity = 0;
   }
} // CalcClear

//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

void CalcMode( TCalcMode Mode)
// Set calculation mode
{
   _Mode = Mode;
} // CalcMode

//-----------------------------------------------------------------------------
// Primary statistics
//-----------------------------------------------------------------------------

static void CalcPrimary( void)
// Calculate primary statistics
{
TSdbRecord Record;

   DsBegin();
   while( DsNext( &Record)){
      AddSample( Record.Weight, Record.Flag);
      WatchDog();
   }
   UpdatePrimary();
} // CalcPrimary

//-----------------------------------------------------------------------------
// Secondary statistics
//-----------------------------------------------------------------------------

static void CalcSecondary( void)
// Calculate secondary statistics
{
int        i;
int        Index;
TSdbRecord Record;

   // Uniformity & histogram initialisation :
   for( i = 0; i < STATISTICS_MAX; i++){
      StatUniformityInit( &_Statistics[ i], 
                          Calc[ i].Average * (TNumber)(100 - Config.StatisticParameters.UniformityRange) / 100.0,
                          Calc[ i].Average * (TNumber)(100 + Config.StatisticParameters.UniformityRange) / 100.0);
      HistogramClear( &Calc[ i].Histogram);
      HistogramSetCenter( &Calc[ i].Histogram, Calc[ i].Average);
      if( Config.StatisticParameters.Histogram.Mode == HISTOGRAM_MODE_RANGE) {      
         HistogramSetRange( &Calc[ i].Histogram, Config.StatisticParameters.Histogram.Range);  // by range
      } else {        
         HistogramSetStep( &Calc[ i].Histogram, Config.StatisticParameters.Histogram.Step);    // by step
      }
   }
   // scan samples :
   DsBegin();
   while( DsNext( &Record)){
      // update total :
      StatUniformityAdd( &_Statistics[ STATISTICS_TOTAL], Record.Weight);
      HistogramAdd( &Calc[ STATISTICS_TOTAL].Histogram, Record.Weight);
      WatchDog();
      // update by flag :
      Index = IndexByFlag( Record.Flag);
      if( Index < 0){
         continue;                     // invalid group
      }
      StatUniformityAdd( &_Statistics[ Index], Record.Weight);
      HistogramAdd( &Calc[ Index].Histogram, Record.Weight);
   }
   // uniformity results :
   for( i = 0; i < STATISTICS_MAX; i++){
      Calc[ i].Uniformity = (word)(10 * StatUniformityGet( &_Statistics[ i]));
   }
} // CalcSecondary

//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

void CalcStatistics( void)
// Calc statistic of the dataset
{
   CalcClear();
   CalcPrimary();
   CalcSecondary();
} // CalcStatistics

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

void CalcAppend( TWeight Weight, TSampleFlag Flag)
// Append sample & recalculate
{
   AddSample( Weight, Flag);
   UpdatePrimary();
   CalcSecondary();
} // CalcAppend

//*****************************************************************************

//-----------------------------------------------------------------------------
// Add sample
//-----------------------------------------------------------------------------

static void AddSample( TWeight Weight, TSampleFlag Flag)
// Add sample to primary
{
int Index;

   // update total statistics :
   StatAdd(&_Statistics[ STATISTICS_TOTAL], Weight);
   if( _Statistics[ STATISTICS_TOTAL].Count <= 1){         
      // first sample - setup min/max weight
      Calc[ STATISTICS_TOTAL].MinWeight = Weight;
      Calc[ STATISTICS_TOTAL].MaxWeight = Weight;
   } else {
      // Minimal & maximal weight
      if( Weight < Calc[ STATISTICS_TOTAL].MinWeight) {
         Calc[ STATISTICS_TOTAL].MinWeight = Weight;
      }
      if( Weight > Calc[ STATISTICS_TOTAL].MaxWeight) {
         Calc[ STATISTICS_TOTAL].MaxWeight = Weight;
      }
   }
   // update by flag :
   Index = IndexByFlag( Flag);
   if( Index < 0){
      return;                          // invalid group
   }
   StatAdd(&_Statistics[ Index], Weight);
   if( _Statistics[ Index].Count <= 1){         
      // first sample - setup min/max weight
      Calc[ Index].MinWeight = Weight;
      Calc[ Index].MaxWeight = Weight;
   } else {
      // Minimal & maximal weight
      if( Weight < Calc[ Index].MinWeight) {
         Calc[ Index].MinWeight = Weight;
      }
      if( Weight > Calc[ Index].MaxWeight) {
         Calc[ Index].MaxWeight = Weight;
      }
   }
} // AddSample

//-----------------------------------------------------------------------------
// Update primary
//-----------------------------------------------------------------------------

static void UpdatePrimary( void)
// Recalculate primary statistics
{
int i;

   for( i = 0; i < STATISTICS_MAX; i++){
      Calc[ i].Count   = _Statistics[ i].Count;
      Calc[ i].Average = StatAverage( &_Statistics[ i]);
      Calc[ i].Sigma   = StatSigma( &_Statistics[ i]);
      Calc[ i].Cv      = (word)(10 * StatVariation( Calc[ i].Average, Calc[ i].Sigma));
   }
} // UpdatePrimary

//-----------------------------------------------------------------------------
// Index by Flag
//-----------------------------------------------------------------------------

static int IndexByFlag( TSampleFlag Flag)
// Returns statistics index by <Flag> or -1 invalid
{
   if( _Mode == CALC_TOTAL){
      return( -1);                     // total only
   }
   if( _Mode == CALC_BY_SEX){
      switch( Flag){
         case FLAG_MALE :
            return( STATISTICS_MALE);

         case FLAG_FEMALE :
            return( STATISTICS_FEMALE);

         default :
            return( -1);
      }
   }
   // CALC_LIGHT_(OK)_HEAVY
   switch( Flag){
      case FLAG_LIGHT :
         return( STATISTICS_LIGHT);

      case FLAG_OK :
         if( _Mode == CALC_LIGHT_HEAVY){
            return( -1);               // sort without OK
         }
         return( STATISTICS_OK);

      case FLAG_HEAVY :
         return( STATISTICS_HEAVY);

      default :
         return( -1);
   }
} // IndexByFlag
