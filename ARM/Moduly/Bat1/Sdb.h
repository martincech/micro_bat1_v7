//******************************************************************************
//
//   Sdb.h        Samples database
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __Sdb_H__
   #define __Sdb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Fs_H__
   #include "../Inc/File/Fs.h"
#endif

#ifndef __Db_H__
   #include "../Inc/File/Db.h"
#endif

#ifndef __SdbDef_H__
   #include "SdbDef.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Basic functions
//------------------------------------------------------------------------------

TYesNo SdbCreate( const char *Name);
// Create new database

void SdbOpen( TFdirHandle Handle, TYesNo ReadOnly);
// Open database

#define SdbEmpty()                     DbEmpty()
// Delete all records

#define SdbRename( Handle, Name)       DbRename( Handle, Name)
// Rename database

#define SdbDelete()                    DbDelete()
// Delete current database

void SdbClose( void);
// Close current database

#define SdbInfo()                      FsInfo()
// Returns directory entry of the database

TSdbConfig *SdbConfig( void);
// Returns working configuration

#define SdbBegin()                     DbBegin()
// Set before start of the table

#define SdbNext( Record)               DbNext( Record)
// Get next record

int SdbFileSize( int Size);
// Returns samples count by file <Size> bytes

//------------------------------------------------------------------------------
// Random Access
//------------------------------------------------------------------------------

#define SdbCount()                     DbCount()
// Returns items count

#define SdbMoveAt( RecordIndex)        DbMoveAt( RecordIndex)
// Move current record pointer at <RecordIndex>

//------------------------------------------------------------------------------
// Enable write
//------------------------------------------------------------------------------

#define SdbDeleteRecord( RecordIndex)  DbDeleteRecord( RecordIndex)
// Delete record at <RecordIndex> position

#define SdbDeleteLastRecord()          DbDeleteLastRecord()
// Delete last record

#define SdbAppend( Record)             DbAppend( Record)
// Append <Record> at end of databease

#ifdef __cplusplus
}
#endif

#endif
