//******************************************************************************
//                                                                            
//   MenuGroups.h   File groups menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __MenuGroups_H__
   #define __MenuGroups_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuFileGroups( void);
// File groups menu

#endif
