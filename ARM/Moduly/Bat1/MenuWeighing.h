//******************************************************************************
//                                                                            
//   MenuWeighing.h  Weighing menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuWeighing_H__
   #define __MenuWeighing_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighing( void);
// Weighing menu

#endif
