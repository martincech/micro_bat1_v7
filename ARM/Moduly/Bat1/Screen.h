//******************************************************************************
//
//   Screen.h     Basic screen drawings
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Screen_H__
   #define __Screen_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __SdbDef_H__
   #include "Sdb.h"
#endif

void ScreenLogo( void);
// Display initial logo

void ScreenBoot( void);
// Display boot logo

void ScreenCharger( void);
// Display charger screen

void ScreenRedraw( void);
// Redraw screen

void ScreenFlash( void);
// Clear flashing objects

void ScreenSorting( TSampleFlag Flag);
// Display sorting <Flag>

void ScreenPowerFail( void);
// Display Power fail screen

void ScreenShutdown( void);
// Display shutdown screen

void ScreenChargerOff( void);
// Display charger shutdown

void ScreenWait( void);
// Display waiting

void ScreenNoMemory( void);
// Display no memory message

void ScreenCapacity( int FilesCount, int FilesSize, int GroupsCount, int PercentUsed);
// Memory capacity screen

#endif
