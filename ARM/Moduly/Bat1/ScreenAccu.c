//*****************************************************************************
//
//    ScreenAccu.c  Accumulator maitenance display
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "ScreenAccu.h"
#include "../inc/conio.h"       // Display
#include "../Inc/Graphic.h"     // GFlush
#include "Wgt/DMsg.h"           // Display message
#include "PAccu.h"              // Accumulator maitenance data
#include "Fonts.h"              // Font list
#include "Wgt/DLayout.h"        // Display layout

//-----------------------------------------------------------------------------
//   Accumulator data
//-----------------------------------------------------------------------------

void ScreenAccumulatorData( void)
// Display accumulator data
{
   SetFont( TAHOMA16);
   GClear();
   DLayoutTitle( "Accumulator data");
   DLayoutStatus( 0, "OK", 0);
   GTextAt( 0, DLAYOUT_TITLE_H);
   cprintf( " Range     :%6.3f V\n",      PAccuGetRange());
   cprintf( " Umax      :%6.3f V (%d)\n", PAccuVoltage( PAccuGetUMax()),  PAccuGetUMax());
   cprintf( " Umid      :%6.3f V (%d)\n", PAccuVoltage( PAccuGetUMid()),  PAccuGetUMid());
   cprintf( " Umin      :%6.3f V (%d)\n", PAccuVoltage( PAccuGetUMin()),  PAccuGetUMin());
   cprintf( " Uwarn     :%6.3f V (%d)\n", PAccuVoltage( PAccuGetUWarn()), PAccuGetUWarn());
   cprintf( " Charge    : %02d:%02d\n",   PAccuGetChargeTime()    / 60,   PAccuGetChargeTime()    % 60);
   cprintf( " Discharge : %02d:%02d\n",   PAccuGetDischargeTime() / 60,   PAccuGetDischargeTime() % 60);
   // clear status area
   GFlush();
} // ScreenAccumulatorData

//-----------------------------------------------------------------------------
//   Charger off
//-----------------------------------------------------------------------------

void ScreenConnectCharger( void)
// Display error message - charger off
{
   DMsgOk( "Error", "Connect charger", 0);
} // ScreenConnectCharger

//-----------------------------------------------------------------------------
//   Format
//-----------------------------------------------------------------------------

void ScreenFormat( void)
// Display accumulator format screen
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 0, 10);
   cputs( "FORMAT");
   GFlush();
} // ScreenFormat

//-----------------------------------------------------------------------------
//   Discharge
//-----------------------------------------------------------------------------

void ScreenDischarge( void)
// Display accumulator discharging screen
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 0, 10);
   cputs( "DISCHARGE");
   GFlush();
} // ScreenDischarge

//-----------------------------------------------------------------------------
//   Charge
//-----------------------------------------------------------------------------

void ScreenCharge( void)
// Display accumulator charging screen
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 0, 10);
   cputs( "CHARGE");
   GFlush();
} // ScreenCharge

//-----------------------------------------------------------------------------
//   Format done
//-----------------------------------------------------------------------------

void ScreenFormatDone( void)
// Display format done
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 4, 5);
   cputs( "FORMAT DONE");
   GFlush();
} // ScreenFormatDone

//-----------------------------------------------------------------------------
//   Discharge done
//-----------------------------------------------------------------------------

void ScreenDischargeDone( void)
// Display discharging done
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 4, 5);
   cputs( "DISCHARGE DONE");
   GFlush();
} // ScreenDischargeDone

//-----------------------------------------------------------------------------
//   Charge done
//-----------------------------------------------------------------------------

void ScreenChargeDone( void)
// Display charging done
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 4, 5);
   cputs( "CHARGE DONE");
   GFlush();
} // ScreenChargeDone

//-----------------------------------------------------------------------------
//   EOF
//-----------------------------------------------------------------------------

void ScreenEof( void)
// Terminated by EOF
{
   SetFont( TAHOMA16);
   cclear();
   cgoto( 4, 5);
   cputs( "FILE SPACE OVERFLOW");
   GFlush();
} // ScreenEof

//-----------------------------------------------------------------------------
//   Time
//-----------------------------------------------------------------------------

void ScreenTime( unsigned Time)
// Display time
{
unsigned Hour, Min, Sec;

   Sec = Time % 60;
   Time /= 60;
   Min = Time % 60;
   Time /= 60;
   Hour = Time;
   cgoto( 3, 0);
   cclreol();
   cprintf( "Time        : %02d:%02d:%02d", Hour, Min, Sec);
   GFlush();
} // ScreenTime

//-----------------------------------------------------------------------------
//   Voltage
//-----------------------------------------------------------------------------

void ScreenVoltage( unsigned Voltage)
// Display voltage
{
   cgoto( 4, 0);
   cclreol();
   cprintf( "Voltage     :%6.3f V",  PAccuVoltage( Voltage));
   GFlush();
} // ScreenVoltage

//-----------------------------------------------------------------------------
//   Sample
//-----------------------------------------------------------------------------

void ScreenSample( int Count, unsigned Voltage)
// Display sampled voltage
{
   cgoto( 5, 0);
   cclreol();
   cprintf( "Sample #%03d :%6.3f V",  Count, PAccuVoltage( Voltage));
   GFlush();
} // ScreenSample

//-----------------------------------------------------------------------------
//   Delta
//-----------------------------------------------------------------------------

void ScreenDelta( int DeltaCount)
// Display Delta count
{
   cgoto( 6, 0);
   cclreol();
   if( DeltaCount < 0){
      cputs(   "Delta       : -");
   } else {
      cprintf( "Delta       : %d", DeltaCount);
   }
   GFlush();
} // ScreenDelta
