//******************************************************************************
//
//   CalDef.h     Calibration definition
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __CalDef_H__
   #define __CalDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

typedef word TCalCrc;                  // calibration checksum

typedef struct {
   TRawWeight Offset;                  // zero calibration
   TRawWeight Factor;                  // range calibration
   TWeight    Range;                   // physical range
   TCalCrc    CheckSum;                // record checksum
   byte       Inversion;               // reversed bridge polarity
   byte       _Spare;
} TCalibration;

#endif
