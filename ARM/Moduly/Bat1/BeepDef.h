//******************************************************************************
//                                                                            
//  BeepDef.h      Sound definitions
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __BeepDef_H__
   #define __BeepDef_H__

// Beep tone enum :
typedef enum {
   BEEP_TONE1,
   BEEP_TONE2,
   BEEP_TONE3,
   BEEP_TONE4,
   BEEP_TONE5,
   BEEP_TONE6,
   BEEP_TONE7,
   BEEP_TONE8,
   BEEP_MELODY1,
   BEEP_MELODY2,
   BEEP_MELODY3,
   BEEP_MELODY4,
} TBeepTone;

#define BEEP_SINGLE_TONE_COUNT    (BEEP_TONE8 + 1)               // max. single tone count
#define BEEP_PATTERN_COUNT        (BEEP_MELODY4 - BEEP_TONE8)    // max. pattern count

#define BEEP_TONE_COUNT (BEEP_MELODY4 + 1) // total tone count

#endif
