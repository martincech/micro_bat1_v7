//******************************************************************************
//
//   Cal.h        Calibration utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Cal_H__
   #define __Cal_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __CalDef_H__
   #include "CalDef.h"
#endif

void CalLoad( void);
// Load calibration from EEPROM

void CalSave( void);
// Save calibration to EEPROM

void CalUpdate( TRawWeight RawZero, TRawWeight RawRange, TWeight Range);
// Update calibration : ADC value <RawZero> by zero, <RawRange> by physical <Range>

TWeight CalGetRange( void);
// Returns physical range

TYesNo CalGetInversion( void);
// Returns bridge polarity inversion

TWeight CalWeight( TRawWeight RawValue);
// Calculate physical weight by <RawValue>

TWeight CalRawWeight( TWeight Value);
// Calculate raw weight by physical <Value>

#endif
