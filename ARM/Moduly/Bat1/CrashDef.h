//*****************************************************************************
//
//    CrashDef.h   Crash info structure
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __CrashDef_H__
  #define __CrashDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

// Signal status :
#define CRASH_PPR     0x01             // PPR signal status
#define CRASH_CHG     0x02             // CHG signal status
#define CRASH_USBON   0x04             // USBON signal status

// Exception information :
typedef struct {
   dword Timestamp;                    // Event time
   dword Address;                      // Memory address
   word  Type;                         // Exception type
   word  Status;                       // Signal status
} TExceptionInfo;

// Watchdog information :
typedef struct {
   dword Timestamp;                    // Event time
   dword Status;                       // Signal status
} TWatchDogInfo;

#endif
