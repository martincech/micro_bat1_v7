//******************************************************************************
//
//   Db.c          Database utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Db.h"
#include "../../Inc/File/Fs.h"
#include "../../Inc/Cpu.h"      // WatchDog

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo DbCreate( const char *Name, TFdirClass Class, void *Configuration)
// Create table with <Name>
{
   if( !FsCreate( Name, Class)){
      return( NO);
   }
   if( !FsWrite( Configuration, DB_CONFIG_SIZE)){
      FsDelete();                      // not enough space
      return( NO);
   }
   return( YES);
} // DbCreate

//------------------------------------------------------------------------------
// Open
//------------------------------------------------------------------------------

void DbOpen( TFdirHandle Handle, TYesNo ReadOnly)
// Open table with <Handle>, set <ReadOnly>
{
   FsOpen( Handle, ReadOnly);
   if( !ReadOnly){
      FsWSeek( 0, FS_SEEK_END);
   }
} // DbOpen

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void DbClose( void)
// Close current table
{
   FsClose();
} // DbClose

//------------------------------------------------------------------------------
// Empty
//------------------------------------------------------------------------------

void DbEmpty( void)
// Delete all records
{
   FsTruncate( DB_CONFIG_SIZE);
   FsWSeek( 0, FS_SEEK_END);
} // DbEmpty

//------------------------------------------------------------------------------
// Load config
//------------------------------------------------------------------------------

void DbLoadConfig( TDbConfig *Configuration)
// Load configuration record <Configuration>
{
   FsRSeek( 0, FS_SEEK_SET);
   FsRead( Configuration, DB_CONFIG_SIZE);
} // DbLoadConfig

//******************************************************************************
// Save config
//******************************************************************************

void DbSaveConfig( TDbConfig *Configuration)
// Save configuration record <Configuration>
{
int    i;
TYesNo SameData;
byte   Data;
byte   *p;

   // check for old data :
   SameData = YES;
   p        = (byte *)Configuration;
   FsRSeek( 0, FS_SEEK_SET);
   for( i = 0; i < DB_CONFIG_SIZE; i++){
      if( !FsRead( &Data, 1)){
         SameData = NO;                // short file
         break;
      }
      if( Data != *p){
         SameData = NO;                // different data
         break;
      }
      p++;
   }
   if( SameData){
      return;                          // already saved
   }
   FsWSeek( 0, FS_SEEK_SET);
   FsWrite( Configuration, DB_CONFIG_SIZE);
   FsWSeek( 0, FS_SEEK_END);
} // DbSaveConfig

//------------------------------------------------------------------------------
// Begin
//------------------------------------------------------------------------------

void DbBegin( void)
// Set before start of the table
{
   FsRSeek( DB_CONFIG_SIZE, FS_SEEK_SET);        // at start of records
} // DbBegin

//------------------------------------------------------------------------------
// Next
//------------------------------------------------------------------------------

TYesNo DbNext( TDbRecord *Record)
// Get next record
{
   return( FsRead( Record, DB_RECORD_SIZE));
} // DbNext

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

int DbCount( void)
// Returns items count
{
int Count;

   Count  = FsInfo()->Size;
   Count -= DB_CONFIG_SIZE;
   Count /= DB_RECORD_SIZE;
   return( Count);
} // DbCount

//------------------------------------------------------------------------------
// Move
//------------------------------------------------------------------------------

TYesNo DbMoveAt( int RecordIndex)
// Move current record pointer at <RecordIndex>
{
   return( FsRSeek( DB_CONFIG_SIZE + RecordIndex * DB_RECORD_SIZE, FS_SEEK_SET));
} // DbMoveAt

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void DbDeleteRecord( int RecordIndex)
// Delete record at <RecordIndex> position
{
TDbRecord Record;

   // write seek at deleted item :
   if( !FsWSeek( DB_CONFIG_SIZE + RecordIndex * DB_RECORD_SIZE, FS_SEEK_SET)){
      return;                                    // wrong record index
   }
   // read seek at next item :
   if( FsRSeek( DB_CONFIG_SIZE + (RecordIndex + 1) * DB_RECORD_SIZE, FS_SEEK_SET)){
      // move records
      while( FsRead( &Record, DB_RECORD_SIZE)){
         FsWrite( &Record, DB_RECORD_SIZE);
         WatchDog();
      }
   }
   FsTruncate( FsWTell());                       // truncate at last record
   FsWSeek( 0, FS_SEEK_END);                     // back to end
   FsRSeek( DB_CONFIG_SIZE + RecordIndex * DB_RECORD_SIZE, FS_SEEK_SET); // back to start of deleted item
} // DbDeleteRecord

//------------------------------------------------------------------------------
// Delete last
//------------------------------------------------------------------------------

TYesNo DbDeleteLastRecord( void)
// Delete last record
{
   if( DbCount() < 1){
      return( NO);                     // nothing to delete
   }
   FsTruncate( FsWTell() - DB_RECORD_SIZE);
   FsWSeek( 0, FS_SEEK_END);
   return( YES);
} // DbDeleteLastRecord

//------------------------------------------------------------------------------
// Append
//------------------------------------------------------------------------------

TYesNo DbAppend( TDbRecord *Record)
// Append <Record> at end of databease
{
   return( FsWrite( Record, DB_RECORD_SIZE));
} // DbAppend
