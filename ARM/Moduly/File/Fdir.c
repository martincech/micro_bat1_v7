//******************************************************************************
//
//   Fdir.c       File Directory utilites
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Fdir.h"
#include "../../inc/File/FsDef.h"
#include "../../inc/Eep.h"
#include "../../inc/System.h"       // SysGetClock
#include "MemoryDef.h"              // project directory EEPROM layout
#include <string.h>

#define GetIndex( Handle) (Handle - 1)
#define GetHandle( Index) (Index  + 1)

static short _ItemsCount;           // number of used directory entries

#define FDIR_ADDRESS FS_OFFSET + offsetof( TFsData, Directory)

// Local Functions :

static TYesNo DirIsDeleted( int Index);
// Get deleted item status from <Index>

static void DirSetDeleted( int Index);
// Set deleted item status to <Index>

static void DirGetName( int Index, char *Name);
// Read item <Name> from <Index>

static void DirSetName( int Index, const char *Name);
// Write item <Name> at <Index>

static int DirGetSize( int Index);
// Read item size from <Index>

//------------------------------------------------------------------------------
// Initialisation
//------------------------------------------------------------------------------

void FdirInit( void)
// Initialisation
{
int i;

   // search for last valid item :
   _ItemsCount = 0;
   for( i = 0; i < FDIR_SIZE; i++){
      if( !DirIsDeleted( i)){
         _ItemsCount = i + 1;
      }
   }
} // FdirInit

//------------------------------------------------------------------------------
// Format
//------------------------------------------------------------------------------

void FdirFormat( void)
// Formatting / initialization
{
int i;

   for( i = 0; i < FDIR_SIZE; i++){
      DirSetDeleted( i);
   }
   _ItemsCount = 0;
} // FdirFormat

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TFdirHandle FdirCreate( const char *Name)
// Create directory entry with <Name>
{
int Index;
int i;
TFdirInfo Item;

   if( _ItemsCount < FDIR_SIZE){
      // append at end
      Index = _ItemsCount;
      _ItemsCount++;
   } else {
      // search for deleted entries
      Index = -1;
      for( i = 0; i < FDIR_SIZE; i++){
         if( DirIsDeleted( i)){
            Index = i;
            break;
         }
      }
      if( Index < 0){
         return( FDIR_INVALID);        // directory full
      }
   }
   memset( &Item, 0, sizeof( Item));   // prepare item
   strcpy( Item.Name, Name);           // set name
   Item.Created = SysGetClock();       // creation time
   FdirSave( GetHandle( Index), &Item);
   return( GetHandle( Index));
} // FdirCreate

//------------------------------------------------------------------------------
// Search
//------------------------------------------------------------------------------

TFdirHandle FdirSearch( const char *Name)
// Search directory entry for <Name>
{
int  i;
char ItemName[ FDIR_NAME_LENGTH + 1];

   for( i = 0; i < _ItemsCount; i++){
      DirGetName( i, ItemName);
      if( ItemName[0] == FDIR_DELETED_FLAG){
         continue;                     // deleted item
      }
      if( strequ( Name, ItemName)){
         return( GetHandle( i));
      }
   }
   return( FDIR_INVALID);
} // FdirSearch

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void FdirDelete( TFdirHandle Handle)
// Delete directory entry
{
   DirSetDeleted( GetIndex( Handle));
} // FdirDelete

//------------------------------------------------------------------------------
// Load entry
//------------------------------------------------------------------------------

void FdirLoad( TFdirHandle Handle, TFdirInfo *Item)
// Get directory <Item> by <Handle>
{
   EepLoadData( FDIR_ADDRESS + GetIndex( Handle) * sizeof( TFdirInfo),
                Item, sizeof( TFdirInfo));
} // FdirLoad

//------------------------------------------------------------------------------
// Save entry
//------------------------------------------------------------------------------

void FdirSave( TFdirHandle Handle, TFdirInfo *Item)
// Save directory entry <Item> at <Handle>
{
   if( EepMatchData( FDIR_ADDRESS + GetIndex( Handle) * sizeof( TFdirInfo),
                     Item, sizeof( TFdirInfo))){
      return;                          // same data, don't save
   }
   EepSaveData( FDIR_ADDRESS + GetIndex( Handle) * sizeof( TFdirInfo), Item, sizeof( TFdirInfo));
} // FdirLoad

//------------------------------------------------------------------------------
// Get Name
//------------------------------------------------------------------------------

void FdirGetName( TFdirHandle Handle, char *Name)
// Fills <Name> by <Handle>
{
   DirGetName( GetIndex( Handle), Name);
} // FdirGetName

//------------------------------------------------------------------------------
// Get Size
//------------------------------------------------------------------------------

int FdirGetSize( TFdirHandle Handle)
// Returns file size by <Handle>
{
   return( DirGetSize( GetIndex( Handle)));
} // FDirGetSize

//------------------------------------------------------------------------------
// Rename
//------------------------------------------------------------------------------

void FdirRename( TFdirHandle Handle, const char *Name)
// Rename with <Name> at <Handle>
{
   DirSetName( GetIndex( Handle), Name);
} // FdirGetName

//******************************************************************************


//------------------------------------------------------------------------------
// Get deleted status
//------------------------------------------------------------------------------

static TYesNo DirIsDeleted( int Index)
// Get deleted item status from <Index>
{
   return( EepReadByte( FDIR_ADDRESS + offsetof( TFdirInfo, Name) + Index * sizeof( TFdirInfo))
           == FDIR_DELETED_FLAG);
} // DirIsDeleted

//------------------------------------------------------------------------------
// Set deleted status
//------------------------------------------------------------------------------

static void DirSetDeleted( int Index)
// Set deleted item status to <Index>
{
   EepWriteByte( FDIR_ADDRESS + offsetof( TFdirInfo, Name) + Index * sizeof( TFdirInfo),
                 FDIR_DELETED_FLAG);
} // DirIsDeleted

//------------------------------------------------------------------------------
// Get Name
//------------------------------------------------------------------------------

static void DirGetName( int Index, char *Name)
// Read item <Name> from <Index>
{
   EepLoadData( FDIR_ADDRESS + offsetof( TFdirInfo, Name) + Index * sizeof( TFdirInfo),
                Name, FDIR_NAME_LENGTH + 1);
} // DirGetName

//------------------------------------------------------------------------------
// Set Name
//------------------------------------------------------------------------------

static void DirSetName( int Index, const char *Name)
// Write item <Name> at <Index>
{
   EepSaveData( FDIR_ADDRESS + offsetof( TFdirInfo, Name) + Index * sizeof( TFdirInfo),
                Name, FDIR_NAME_LENGTH + 1);
} // DirSetName

//------------------------------------------------------------------------------
// Get Size
//------------------------------------------------------------------------------

static int DirGetSize( int Index)
// Read item size from <Index>
{
TFsSize Size;

   Size = 0;
   EepLoadData( FDIR_ADDRESS + offsetof( TFdirInfo, Size) + Index * sizeof( TFdirInfo),
                &Size, sizeof( TFsSize));
   return( Size);
} // DirGetSize
