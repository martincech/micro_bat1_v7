//*****************************************************************************
//
//    Backlight.c   Backlight functions
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Backlight.h"
#include "../inc/System.h"
#include "../inc/Pwm.h"

int _Intensity;

// Local functions :

static int PwmPercent( int Intensity);
// PWM duty cycle by backlight <Intensity>

static int PwmIntensity( void);
// PWM duty by current intensity

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void BacklightInit( void)
// Initialisation
{
   PwmInitPorts();
   PwmInit();
   _Intensity = 9;
} // BacklightInit

//------------------------------------------------------------------------------
//  On
//------------------------------------------------------------------------------

void BacklightOn( void)
// Conditionaly on
{
   PwmStart( PwmIntensity());
} // BacklightOn

//------------------------------------------------------------------------------
//  Off
//------------------------------------------------------------------------------

void BacklightOff( void)
// Conditionaly off
{
   PwmStop();
} // BacklightOff

//------------------------------------------------------------------------------
//  Set
//------------------------------------------------------------------------------

void BacklightSet( int Intensity)
// Set intensity
{
   _Intensity = Intensity;
} // BacklightSet

//------------------------------------------------------------------------------
//  Test
//------------------------------------------------------------------------------

void BacklightTest( int Intensity)
// Test backlight intensity
{
   PwmStart( PwmPercent( Intensity));
} // BacklightTest

//*****************************************************************************

//------------------------------------------------------------------------------
//  Percent
//------------------------------------------------------------------------------

static const byte Percent[10] =
{ 0,10,12,16,21,28,35,50,73,100};

static int PwmPercent( int Intensity)
// PWM duty cycle by backlight intensity
{
   return( Percent[ Intensity]);
} // PwmPercent

//------------------------------------------------------------------------------
//  Intensity
//------------------------------------------------------------------------------

static int PwmIntensity( void)
// PWM duty by current intensity
{
   return( PwmPercent( _Intensity));
} // PwmIntensity
