//*****************************************************************************
//
//    Fonts.c - project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#include "Fonts.h"

#include "../Moduly/Font/Font8x8.c"
#include "../Moduly/Font/Tahoma8.c"
#include "../Moduly/Font/Lucida6.c"

const TFontDescriptor const *Fonts[] = {
   /* FONT_8x8 */   &Font8x8,
   /* Tahoma8  */   &Tahoma8,
   /* Lucida6  */   &Lucida6,
   /* last     */   0
};   
