//*****************************************************************************
//
//    Ads123x.c  -  A/D convertor ADS1230/ADS1232 services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../inc/Ads123x.h"
#include "../inc/System.h"   // SysDelay
#include "../inc/Cpu.h"

#if defined( __ADS1232__)
   // Parametry dat prevodniku ADS 1232 :
   #define ADC_DATA_WIDTH         24
   #define ADC_CALIBRATION_CLOCK  26
#elif defined ( __ADS1230__)
   // Parametry dat prevodniku ADS 1230 :
   #define ADC_DATA_WIDTH         20
   #define ADC_CALIBRATION_CLOCK  26
#else
   #error "Unknown ADS123x convertor type"
#endif

//-----------------------------------------------------------------------------
#ifdef ADC_INTERRUPT       // prevodnik v prerusovacim modu

void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

int32 AdcReadValue( void);
// Precteni namerene hodnoty

static void __irq EintHandler( void);
// External interrupt handler

#define EINT_PORT       ADC_DRDY_PORT  // EINT port number (P0.EINT_PORT)
#define EINT_FALL_EDGE  1              // EINT fall edge sensitivity

#include "../Moduly/LPC/Eint.c"     // include template

#ifdef ADC_AVERAGE_SHIFT   // je definovan shift, bude se prumerovat
   #define ADC_AVERAGE  1
   #define ADC_AVERAGE_COUNT (1 << (ADC_AVERAGE_SHIFT))        // pocet vzorku prumeru
#else
   #define ADC_AVERAGE_COUNT  0                                // pocet vzorku prumeru = 0
#endif

// Globalni data :
volatile byte          _AdcStatus = 0;            // stav mereni
static volatile int32  _AdcValue;                 // merena hodnoty

#ifdef ADC_AVERAGE
static volatile int32  _AdcSum;                   // suma mereni
#endif

#if ADC_DRDY == ADC_DOUT                             // common DRDY/DOUT input
   #define EintAttachPin()   EintSelectPin()         // use EINT
   #define EintReleasePin()  EintDeselectPin()       // use EINT as I/O
   #ifdef ADC_PINSEL_INTERRUPT
      #define AdcDisableInts()   DisableInts()
      #define AdcEnableInts()    EnableInts()
   #else
      #define AdcDisableInts()
      #define AdcEnableInts()
   #endif
#else // ADC_DRDY != ADC_DOUT separate DRDY/DOUT inputs
   #define EintAttachPin()
   #define EintReleasePin()
   #define AdcDisableInts()
   #define AdcEnableInts()
#endif

#else // no ADC_INTERRUPT
   #define EintAttachPin()
   #define EintReleasePin()
   #define AdcDisableInts()
   #define AdcEnableInts()
#endif // ADC_INTERRUPT

//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AdcInit( void)
// Inicializace prevodniku
{
   AdcInitPorts();                     // port init
#ifdef ADC_INTERRUPT
   EintSetup();                        // EINT init     
#endif
   AdcClrSCLK();
   AdcClrPDWN();                       // power down
   SysUDelay( 100);                    // min 26us
   AdcSetPDWN();                       // power up
   AdcSetup();
} // AdcInit

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

void AdcSetup( void)
// Nastaveni prevodu, autokalibrace
{
native i;

   AdcDisableInts();                   // PINSEL access
   EintReleasePin();                   // use DRDY/DOUT as standard I/O
   AdcEnableInts();
   // DRDY may be configured as EINT, use DOUT instead :
   while( !AdcGetDOUT());              // wait for DRDY = H
   while(  AdcGetDOUT());              // wait for DRDY = L - data ready
   i = ADC_CALIBRATION_CLOCK;
   do {
      AdcSetSCLK();
      SysUDelay( 1);
      AdcClrSCLK();
      SysUDelay( 1);
   } while( --i);
   AdcDisableInts();                   // PINSEL access
   EintAttachPin();                    // use DRDY/DOUT as EINT
   AdcEnableInts();
} // AdcSetup

//-----------------------------------------------------------------------------
// Cteni dat
//-----------------------------------------------------------------------------

int32 AdcReadValue( void)
// Precteni namerene hodnoty
{
int32  Value;
native i;

   EintReleasePin();                   // use DRDY/DOUT as standard I/O
   // znamenkovy bit :
   AdcSetSCLK();
   SysUDelay( 1);
   if( AdcGetDOUT()){
      Value = -1;                      // rozsireni zaporneho znamenka
   } else {
      Value =  0;                      // kladne znamenko
   }
   AdcClrSCLK();
   SysUDelay( 1);
   // vyznamove bity :
   i = ADC_DATA_WIDTH - 1;
   do {
      AdcSetSCLK();
      SysUDelay( 1);
      Value <<= 1;
      if( AdcGetDOUT()){
         Value |= 1;
      }
      AdcClrSCLK();
      SysUDelay( 1);
   } while( --i);
   // nastavit DRDY do H :
   AdcSetSCLK();
   SysUDelay( 1);
   AdcClrSCLK();
   SysUDelay( 1);
   EintAttachPin();                    // use DRDY/DOUT as EINT
   return( Value);
} // AdcReadValue

#ifdef ADC_INTERRUPT       // prevodnik v prerusovacim modu
//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Zahajeni periodickeho mereni
{
   EintDisableIrq();
   _AdcStatus = ADC_AVERAGE_COUNT;     // flag 0, pocet vzorku prumeru
   #ifdef ADC_AVERAGE
      _AdcSum = 0;                     // suma mereni
   #endif
   AdcSetup();                         // inicializace
   EintEnableIrq();                    // enable EINT
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Zastaveni periodickeho mereni
{
   EintDisableIrq();
   _AdcStatus = 0;
} // AdcStop

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

int32 AdcRead( void)
// Cteni merene hodnoty
{
int32 Value;

   EintDisableIrq();
   _AdcStatus &= ~ADC_STATUS_READY;    // zrus priznak
   #ifndef ADC_AVERAGE
      Value  = _AdcValue;              // precti prevod
   #else
      Value  = _AdcValue >> ADC_AVERAGE_SHIFT;   // vydel poctem vzorku
   #endif
   EintEnableIrq();
   return( Value);
} // AdcRead

#ifndef ADC_AVERAGE  // bez prumerovani
//-----------------------------------------------------------------------------
// Prerusovaci rutina externiho vstupu
//-----------------------------------------------------------------------------

static void __irq EintHandler( void)
// Preruseni od externiho vstupu
{

   _AdcValue   = AdcReadValue();       // precti prevod
   // upravit priznaky :
   _AdcStatus |= ADC_STATUS_READY;     // nova data
   EintClearFlag();                    // zrus priznak nastaveny ctenim hodnoty
   CpuIrqDone();
} // EintHandler

#else // ADC_AVERAGE - s prumerovanim
//-----------------------------------------------------------------------------
// Prerusovaci rutina externiho vstupu
//-----------------------------------------------------------------------------

static void __irq EintHandler( void)
// Preruseni od externiho vstupu
{
dword Value;

   Value    = AdcReadValue();          // precti prevod
   _AdcSum += Value;                   // akumuluj
   _AdcStatus--;                       // pocet vzorku
   if( !(_AdcStatus & ADC_STATUS_MASK)){
      // konec prumerovani
      _AdcValue  = _AdcSum;                                  // zapamatuj jako hodnotu prevodu
      _AdcStatus = ADC_STATUS_READY | ADC_AVERAGE_COUNT;     // nova data, novy pocet vzorku
      _AdcSum    = 0;                                        // nova akumulace
   }
   EintClearFlag();                    // zrus priznak nastaveny ctenim hodnoty
   CpuIrqDone();
} // EintHandler
#endif // ADC_AVERAGE

#endif // ADC_INTERRUPT
