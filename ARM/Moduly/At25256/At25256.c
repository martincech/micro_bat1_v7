//*****************************************************************************
//
//    At25256.c     AT25256 EEPROM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Eep.h"

// Instrukce AT25256 :

#define EEP_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define EEP_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define EEP_RDSR    0x05           // Read Status Register
#define EEP_WRSR    0x01           // Write Status Register
#define EEP_READ    0x03           // Read data
#define EEP_WRITE   0x02           // Write data

// Status register format AT25256 :

#define EEP_MASK_RDY  0x01         // -READY, je v 1 probiha-li zapis
#define EEP_MASK_WEN  0x02         // je v 1 je-li zarizeni Write Enabled
#define EEP_MASK_BP0  0x04         // ochranny bit 0 (1/4 kapacity)
#define EEP_MASK_BP1  0x08         // ochranny bit 1 (1/2 kapacity)
#define EEP_MASK_WPEN 0x80         // ovladani Write Protect /WP pinu

// Lokalni funkce :

static void WrenCmd( void);
// Posle instrukci WREN

static byte RdsrCmd( void);
// Precte a vrati status registr

static void WrsrCmd( byte value);
// Zapise do status registru

static void WaitForReady( void);
// Cekani na pripravenost

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void EepInit()
// Nastavi klidove hodnoty na sbernici, inicializuje pamet
{
   SpiInit();
   // zapis do stavoveho registru :
   WaitForReady();
   WrenCmd();                          // Set write enable latch
   WrsrCmd( 0);                        // Zrusit ochranu pameti
} // EepInit

//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

byte EepReadByte( unsigned Address)
// Precte byte z EEPROM <Address>
{
byte Value;

   EepBlockReadStart( Address);        // zacatek bloku
   Value = SpiReadByte();              // jeden datovy byte
   SpiRelease();                       // konec cteni
   return( Value);
} // EepReadByte

//-----------------------------------------------------------------------------
// Zapis do pameti
//-----------------------------------------------------------------------------

TYesNo EepWriteByte( unsigned Address, byte Value)
// Zapise byte <Value> na <Address> v EEPROM
{
   if( !EepPageWriteStart( Address)){
      return( NO);
   }
   SpiWriteByte( Value);               // data
   SpiRelease();                       // provedeni zapisu
   return( YES);                       // necekame na dokonceni zapisu
} // EepWriteByte

//-----------------------------------------------------------------------------
// Strankove cteni z pameti
//-----------------------------------------------------------------------------

void EepBlockReadStart( unsigned Address)
// Zahaji blokove cteni z EEPROM <Address>
{
   WaitForReady();                     // pripravenost
   SpiAttach();                        // select CS
   SpiWriteByte( EEP_READ);            // READ instruction
   SpiWriteByte( Address >> 8);        // high address
   SpiWriteByte( Address);             // low address
} // EepBlockReadStart

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

TYesNo EepPageWriteStart( unsigned Address)
// Zahaji zapis stranky od <Address> v EEPROM
{
   if( !SysAccuOk()){
      return( NO);
   }
   WaitForReady();                     // cekani na pripravenost
   WrenCmd();                          // Set write enable latch
   SpiAttach();                        // select CS
   SpiWriteByte( EEP_WRITE);           // WRITE instruction
   SpiWriteByte( Address >> 8);        // high address
   SpiWriteByte( Address);             // low address
   return( YES);
} // EepPageWriteStart

//-----------------------------------------------------------------------------
// Instrukce WREN
//-----------------------------------------------------------------------------

static void WrenCmd()
// Posle instrukci WREN
{
   SpiAttach();                    // select CS
   SpiWriteByte( EEP_WREN);
   SpiRelease();                   // deselect CS
} // WrenCmd


//-----------------------------------------------------------------------------
// Instrukce RDSR
//-----------------------------------------------------------------------------

static byte RdsrCmd()
// Precte a vrati status registr
{
byte value;

   SpiAttach();                      // select CS
   SpiWriteByte( EEP_RDSR);
   value  = SpiReadByte();
   SpiRelease();                    // deselect CS
   return( value);
}  // RdsrCmd

//-----------------------------------------------------------------------------
// Instrukce WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd( byte value)
// Zapise do status registru
{
   SpiAttach();                      // select CS
   SpiWriteByte( EEP_WRSR);
   SpiWriteByte( value);
   SpiRelease();                    // deselect CS
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Cekani na dokonceni zapisu
//-----------------------------------------------------------------------------

static void WaitForReady()
// Cekani na pripravenost
{
   while( RdsrCmd() & EEP_MASK_RDY);
} // WaitForReady
