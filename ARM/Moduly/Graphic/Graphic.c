//*****************************************************************************
//
//    Graphic.c     Graphic services 2/4 colors
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Graphic.h"
#include "GIntern.h"            // Graphic internals
#include "../../inc/Gpu.h"
#include "../../inc/Font.h"
#include "Fonts.h"              // Fonty prislusejici projektu
#include <string.h>

#define GetBit( y)       ((y) & 0x07)                      // index bitu
#define GetPixel( y)     (1 << GetBit( y))                 // maska bytu
#define GetLine( y1, y2) ((0xFF >> (7 - (y2) + (y1))) << (y1)) // kratka cara
#define GetRow( y)       ((y) >> 3)                        // adresa bytu
#define LineFrom( y)     (0xFF << GetBit(y))               // cara od <y> do MSB
#define LineTo( y)       (0xFF >> (7 - GetBit(y)))         // cara od LSB do <y>

#define SetMinX( x)      if( GBuffer.MinX > (x)) GBuffer.MinX = x
#define SetMinY( y)      if( GBuffer.MinY > (y)) GBuffer.MinY = y
#define SetMaxX( x)      if( GBuffer.MaxX < (x)) GBuffer.MaxX = x
#define SetMaxY( y)      if( GBuffer.MaxY < (y)) GBuffer.MaxY = y

// Kontextove promenne :
byte _Mode;
byte _Color;
word _Pattern;

//------ Lokalni funkce :

static void PutByte( int x, int y, byte value, byte mask);
// Zapise byte do bufferu s respektovanim operace

static void HorizontalLine( int x1, int x2, int y);
// Kresli vodorovnou caru

static void VerticalLine( int x, int y1, int y2);
// Kresli svislou caru

static void Line( int x1, int y1, int x2, int y2);
// Obecna cara

#if G_PLANES > 1
static void BitCopy( int x, int y, int width, int height, const void *Bitmap);
// Prekopiruje bitmapu o velikosti <width,height> pixelu na souradnici <x,y>

static void SaveByte( int plane, int x, int y, byte value, byte mask);
// Ulozi byte do bufferu
#endif

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void GInit( void)
// Inicializace
{
   GpuInit();
   _Color   = COLOR_BLACK;
   _Mode    = GMODE_REPLACE;
   _Pattern = PATTERN_SOLID;
   GSetFont( 0);
} // GInit

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void GClear( void)
// Smaze display
{
   GBuffer.MinX = 0;
   GBuffer.MaxX = G_MAX_X;
   GBuffer.MinY = 0;
   GBuffer.MaxY = G_MAX_Y;   
   memset( GBuffer.Buffer, 0, sizeof( GBuffer.Buffer));
} // GClear

//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

void GSetMode( int GraphicMode)
// Nastavi mod prekreslovani
{
   _Mode = GraphicMode;
} // GSetMode

//-----------------------------------------------------------------------------
// Color
//-----------------------------------------------------------------------------

void GSetColor( int Color)
// Nastavi barvu kresleni
{
   _Color = Color;
} // GSetColor

//-----------------------------------------------------------------------------
// Pattern
//-----------------------------------------------------------------------------

void GSetPattern( int Pattern)
// Vzory car
{
   _Pattern = Pattern;
} // GSetPattern

//-----------------------------------------------------------------------------
// Pixel
//-----------------------------------------------------------------------------

void GPixel( int x, int y)
// Tecka
{
   // boundary :
   SetMinX( x);
   SetMaxX( x);
   SetMinY( y);
   SetMaxY( y);
   // kresleni :
   PutByte( x, GetRow( y), GetPixel( y), GetPixel( y));
} // GPixel

//-----------------------------------------------------------------------------
// Line
//-----------------------------------------------------------------------------

void GLine( int x1, int y1, int x2, int y2)
// Usecka
{
   // boundary :
   SetMinX( x1);
   SetMaxX( x2);
   SetMinY( y1);
   SetMaxY( y2);
   // kresleni podle typu :
   if( x1 == x2){
      VerticalLine( x1, y1, y2);
      return;
   } 
   if( y1 == y2){
      // vodorovna
      HorizontalLine( x1, x2, y1);
      return;
   }
   // obecna cara
   SetMinX( x2);                       // souradnice nemusi byt usporadany
   SetMaxX( x1);
   SetMinY( y2);
   SetMaxY( y1);
   Line( x1, y1, x2, y2);
} // GLine

//-----------------------------------------------------------------------------
// Box
//-----------------------------------------------------------------------------

void GBox( int x, int y, int width, int height)
// Vyplneny obdelnik
{
int maxX, maxY;

   maxX = x + width  - 1;
   maxY = y + height - 1;
   // clipping :
   if( maxX > G_MAX_X){
      maxX = G_MAX_X;
   }
   if( maxY > G_MAX_Y){
      maxY = G_MAX_Y;
   }
   // boundary :
   SetMinX( x);
   SetMaxX( maxX);
   SetMinY( y);
   SetMaxY( maxY);
   // kresleni :
   for( ; x <= maxX; x++){
      VerticalLine( x, y, maxY);      
   }
} // GBox

//-----------------------------------------------------------------------------
// Box Rounded
//-----------------------------------------------------------------------------

void GBoxRound( int x, int y, int width, int height, int LeftRadius, int RightRadius)
// Draw filled box with rounded corners
{
int maxX, maxY;
int i;

   maxX = x + width  - 1;
   maxY = y + height - 1;
   // clipping :
   if( maxX > G_MAX_X){
      maxX = G_MAX_X;
   }
   if( maxY > G_MAX_Y){
      maxY = G_MAX_Y;
   }
   // boundary :
   SetMinX( x);
   SetMaxX( maxX);
   SetMinY( y);
   SetMaxY( maxY);
   // left trapezoid :
   for( i = LeftRadius; i > 0; i--){
      VerticalLine( x + LeftRadius - i, y + i, maxY - i);      
   }
   // right trapezoid :
   for( i = 1; i <= RightRadius; i++){
      VerticalLine( maxX - RightRadius + i, y + i, maxY - i);      
   }
   // box :
   maxX -= RightRadius;
   for( i = x + LeftRadius; i <= maxX; i++){
      VerticalLine( i, y, maxY);      
   }
} // GBoxRound

//-----------------------------------------------------------------------------
// Rectangle
//-----------------------------------------------------------------------------

void GRectangle( int x, int y, int width, int height)
// Prazdny obdelnik
{
int maxX, maxY;

   if( height < 2){
      return;
   }
   maxX = x + width  - 1;
   maxY = y + height - 1;
   // clipping :
   if( maxX > G_MAX_X){
      maxX = G_MAX_X;
   }
   if( maxY > G_MAX_Y){
      maxY = G_MAX_Y;
   }
   // boundary :
   SetMinX( x);
   SetMaxX( maxX);
   SetMinY( y);
   SetMaxY( maxY);
   // kresleni :
   HorizontalLine( x,   maxX,     y); // horni
   HorizontalLine( x,   maxX,  maxY); // dolni
   y++;                               // zkratit o horni
   maxY--;                            // zkratit o dolni
   VerticalLine(   x,    y,    maxY); // leva
   VerticalLine(   maxX, y,    maxY); // prava
} // GRectangle

//-----------------------------------------------------------------------------
// Internal bitmap
//-----------------------------------------------------------------------------

#define BData        ((const byte *)Bitmap)      // pristup do bitmapy
#define GetWord( a)  ((word)(*(a)) |\
                     ((word)(*((a)+1)) << 8))    // cte slovo bez ohledu na word alignment

void GBitBlt( int x, int y, int width, int height, const void *Bitmap)
// Prekopiruje bitmapu o velikosti <width,height> pixelu na souradnici <x,y>
{
int maxX, maxY;
int r, c, i;
int Address;          // bitova adresa v bitmape
const byte *p;        // ukazatel do bitmapy
dword Tmp;            // pomocny buffer bitmapy
int Shift;            // posun v bitmape
int BHeight;          // bytova vyska
byte   Mask;
byte   BMask;         // maska neuplneho bytu
int StartRow;         // svisly posuv v bitech

   maxX = x + width  - 1;
   maxY = y + height - 1;
   if( maxX > G_MAX_X || maxY > G_MAX_Y){
      return;
   }
   SetMinX( x);
   SetMaxX( maxX);
   SetMinY( y);
   SetMaxY( maxY);
   
   Address = 0;                                  // vychozi adresa v bitmape
   StartRow = GetBit( y);                        // svisly posuv
   
   // prodlouzime sloupec o svisly posuv :
   BHeight  = GetRow( height + StartRow + 7) - 1;        // pocet zasazenych bytu (jeden je vzdy)
   BMask    = 0xFF >> (8 - GetBit( height + StartRow));  // maska nedokoncene osmice
   if( !BMask){
      BMask = 0xFF;                              // shift o 8 prekodujeme na shift o 0
   }   
   for( c = x; c <= maxX; c++){
      // jeden sloupec
      r     = GetRow( y);
      p     = &BData[ GetRow( Address)];         // zacatek sloupce v bitmape
      Shift = GetBit( Address);                  // bitovy ofset zacatku
      // priprav prvni byte :
      Tmp   = GetWord( p);                       // dvojice bytu
      Tmp >>= Shift;                             // eliminuj bitovy ofset
      Tmp <<= StartRow;                          // bitovy svisly posuv (dopln nula * StartRow)
      Mask  = 0xFF << StartRow;                  // spodni bity vynech
      // od druheho bytu nastav novy zdrojovy ofset :
      i = Address + 8 - StartRow;                // novy bitovy ofset
      p     = &BData[ GetRow( i)];               // zacatek sloupce v bitmape
      Shift = GetBit( i);                        // bitovy ofset zacatku
      // uplne byty :
      for( i = 0; i < BHeight; i++){
         PutByte( c, r, Tmp, Mask);              // zapis pripraveny byte
         r++;                                    // dalsi radek
         Tmp   = GetWord( p);                    // dvojice bytu
         Tmp >>= Shift;                          // eliminuj bitovy ofset
         Mask  = 0xFF;                           // uplny byte
         p++;                                    // dalsi byte
      }
      // posledni neuplny byte :
      PutByte( c, r, Tmp & BMask, BMask & Mask); // jeden byte bude vzdy
      Address += height;                         // dalsi sloupec bitmapy
   } // for c
} // GBitBlt

#if G_PLANES == 1
//-----------------------------------------------------------------------------
// Bitmap
//-----------------------------------------------------------------------------

void GBitmap( int x, int y, const TBitmap *Bitmap)
// Vykresli bitmapu na souradnici <x,y>
{
   GBitBlt( x, y, Bitmap->Width, Bitmap->Height, Bitmap->Array);
} // GBitmap

#else // G_PLANES > 1
//-----------------------------------------------------------------------------
// Bitmap
//-----------------------------------------------------------------------------

void GBitmap( int x, int y, const TBitmap *Bitmap)
// Vykresli bitmapu na souradnici <x,y>
{
   if( Bitmap->Planes < 2){
      GBitBlt( x, y, Bitmap->Width, Bitmap->Height, Bitmap->Array);
      return;
   }
   BitCopy( x, y, Bitmap->Width, Bitmap->Height, Bitmap->Array);
} // GBitmap
#endif // G_PLANES > 1

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void GFlush( void)
// Prekopiruje buffer do displeje
{
   if( GBuffer.MinX > GBuffer.MaxX){
      return;
   }
   if( GBuffer.MinY > GBuffer.MaxY){
      return;
   }
   GpuFlush();
} // GFlush

//-----------------------------------------------------------------------------
// Flush all
//-----------------------------------------------------------------------------

void GRedraw( void)
// Prekresli cely displej z bufferu
{
   // cela plocha :
   GBuffer.MinX = 0;
   GBuffer.MaxX = G_MAX_X;
   GBuffer.MinY = 0;
   GBuffer.MaxY = G_MAX_Y;
   GpuFlush();
} // GRedraw

#if G_PLANES == 1
//-----------------------------------------------------------------------------
// Zapis bytu
//-----------------------------------------------------------------------------

static void PutByte( int x, int y, byte value, byte mask)
// Zapise byte do bufferu s respektovanim operace
{
byte OldValue;
byte NewValue;

   // range check :
   if( x < 0 || x >= G_WIDTH){
      return;
   }
   if( y < 0 || y >= GetRow( G_HEIGHT)){
      return;
   }
   OldValue = GBuffer.Buffer[y][x];
   if( _Color == COLOR_BLACK){
      if( _Mode == GMODE_XOR){
         NewValue = OldValue ^ (value & mask);
      } else { // REPLACE, OR
         NewValue = OldValue | (value & mask);
      }
   } else {
      if( _Mode == GMODE_REPLACE){
         NewValue = OldValue & ~(value & mask);
      }
   }
   GBuffer.Buffer[y][x] = NewValue;
} // PutByte

#else // G_PLANES > 1
//-----------------------------------------------------------------------------
// Zapis bytu
//-----------------------------------------------------------------------------

static void PutByte( int x, int y, byte value, byte mask)
// Zapise byte do bufferu s respektovanim operace
{
byte OldValue;
byte NewValue;

   // range check :
   if( x < 0 || x >= G_WIDTH){
      return;
   }
   if( y < 0 || y >= GetRow( G_HEIGHT)){
      return;
   }
   // plane 0 -----------------------------------------------------------------
   OldValue = GBuffer.Buffer[0][y][x];
   if( _Color & COLOR_LIGHTGRAY){
      if( _Mode == GMODE_XOR){
         NewValue = OldValue ^ (value & mask);
      } else { // REPLACE, OR
         NewValue = OldValue | (value & mask);
      }
   } else {
      if( _Mode == GMODE_REPLACE){
         NewValue = OldValue & ~(value & mask);
      }
   }
   GBuffer.Buffer[0][y][x] = NewValue;
   // plane 1 -----------------------------------------------------------------
   OldValue = GBuffer.Buffer[1][y][x];
   if( _Color & COLOR_DARKGRAY){
      if( _Mode == GMODE_XOR){
         NewValue = OldValue ^ (value & mask);
      } else { // REPLACE, OR
         NewValue = OldValue | (value & mask);
      }
   } else {
      if( _Mode == GMODE_REPLACE){
         NewValue = OldValue & ~(value & mask);
      }
   }
   GBuffer.Buffer[1][y][x] = NewValue;
} // PutByte
#endif // G_PLANES > 1

//-----------------------------------------------------------------------------
// Vodorovna cara
//-----------------------------------------------------------------------------

static void HorizontalLine( int x1, int x2, int y)
// Kresli vodorovnou caru
{
int x;
byte   mask;
dword Pattern;

   mask = GetPixel( y);
   y    = GetRow( y);
   Pattern = (dword)_Pattern | ((dword)_Pattern << 8); // maska na word
   Pattern >>= 8 - GetBit( x1);        // posun zacatek vzoru na x1
   for( x = x1; x <= x2; x++){
      if( (1 << GetBit( x)) & Pattern){
         PutByte( x, y, mask, mask);   // jednicka ve vzoru
      } else {
         PutByte( x, y, 0,    mask);   // nula ve vzoru
      }
   }
} // HorizontalLine

//-----------------------------------------------------------------------------
// Svisla cara
//-----------------------------------------------------------------------------

static void VerticalLine( int x, int y1, int y2)
// Kresli svislou caru
{
int y;
int r1, r2;
byte mask;
word Pattern;

   r1   = GetRow( y1);
   r2   = GetRow( y2);
   Pattern = (word)_Pattern | ((word)_Pattern << 8); // maska na word
   Pattern >>= 8 - GetBit( y1);        // posun zacatek vzoru na y1
   if( r1 == r2){
      // cara v ramci bytu
      y1 = GetBit( y1);
      y2 = GetBit( y2);
      mask = GetLine( y1, y2);
      PutByte( x, r1, mask & Pattern, mask);
      return;
   }
   // pocatecni fragment :
   mask = LineFrom( y1);
   PutByte( x, r1, mask & Pattern, mask);
   // koncovy fragment :     
   mask = LineTo( y2);
   PutByte( x, r2, mask & Pattern, mask);      
   // mezilehle osmice :
   for( y = r1 + 1; y < r2; y++){
      PutByte( x, y, Pattern, 0xFF);      
   }
} // VerticalLine

//-----------------------------------------------------------------------------
// Obecna cara
//-----------------------------------------------------------------------------

static void Line( int x1, int y1, int x2, int y2)
// Obecna cara
{
int dy = y2 - y1;
int dx = x2 - x1;
int stepx, stepy;
int fraction;                          // same as 2*dy - dx
byte Mask;

   if( dy < 0){ 
      dy    = -dy;  
      stepy = -1; 
   } else { 
      stepy = 1; 
   }
   if( dx < 0){ 
      dx    = -dx;  
      stepx = -1; 
   } else { 
      stepx = 1; 
   }
   dy <<= 1;                           // dy is now 2*dy
   dx <<= 1;                           // dx is now 2*dx
   Mask = 0x01;
   if( Mask & _Pattern){
      PutByte( x1, GetRow( y1), GetPixel( y1), GetPixel( y1));
   } else {
      PutByte( x1, GetRow( y1), 0, GetPixel( y1));
   }
   Mask <<=1;
   if( dx > dy){
      fraction = dy - (dx >> 1);       // same as 2*dy - dx
      while( x1 != x2){
         if( fraction >= 0){
            y1 += stepy;
            fraction -= dx;            // same as fraction -= 2*dx
         }
         x1       += stepx;
         fraction += dy;               // same as fraction -= 2*dy
         if( Mask & _Pattern){
            PutByte( x1, GetRow( y1), GetPixel( y1), GetPixel( y1));
         } else {
            PutByte( x1, GetRow( y1), 0, GetPixel( y1));
         }
         Mask <<=1;
         if( !Mask){
            Mask = 0x01;
         }
      }
   } else {
      fraction = dx - (dy >> 1);
      while( y1 != y2){
         if( fraction >= 0){
            x1 += stepx;
            fraction -= dy;
         }
         y1       += stepy;
         fraction += dx;
         if( Mask & _Pattern){
            PutByte( x1, GetRow( y1), GetPixel( y1), GetPixel( y1));
         } else {
            PutByte( x1, GetRow( y1), 0, GetPixel( y1));
         }
         Mask <<=1;
         if( !Mask){
            Mask = 0x01;
         }
      }
   }
} // Line

#if G_PLANES > 1
//-----------------------------------------------------------------------------
// Copy bitmap
//-----------------------------------------------------------------------------

static void BitCopy( int x, int y, int width, int height, const void *Bitmap)
// Prekopiruje bitmapu o velikosti <width,height> pixelu na souradnici <x,y>
{
int maxX, maxY;
int r, c, i, plane;
int Address;          // bitova adresa v bitmape
const byte *p;        // ukazatel do bitmapy
const byte *PBitmap;  // zacatek roviny v bitmape
dword Tmp;            // pomocny buffer bitmapy
int Shift;            // posun v bitmape
int BHeight;          // bytova vyska
byte   Mask;
byte   BMask;         // maska neuplneho bytu
int StartRow;         // svisly posuv v bitech

   maxX = x + width  - 1;
   maxY = y + height - 1;
   if( maxX > G_MAX_X || maxY > G_MAX_Y){
      return;
   }
   SetMinX( x);
   SetMaxX( maxX);
   SetMinY( y);
   SetMaxY( maxY);
   
   StartRow = GetBit( y);                           // svisly posuv
   
   // prodlouzime sloupec o svisly posuv :
   BHeight  = GetRow( height + StartRow + 7) - 1;        // pocet zasazenych bytu (jeden je vzdy)
   BMask    = 0xFF >> (8 - GetBit( height + StartRow));  // maska nedokoncene osmice
   if( !BMask){
      BMask = 0xFF;                                 // shift o 8 prekodujeme na shift o 0
   }   
   PBitmap = (const byte *)Bitmap;                  // prvni rovina
   for( plane = 0; plane < 2; plane++){ 
      Address = 0;                                  // vychozi adresa v rovine
      for( c = x; c <= maxX; c++){
         // jeden sloupec
         r     = GetRow( y);
         p     = &PBitmap[ GetRow( Address)];       // zacatek sloupce v bitmape
         Shift = GetBit( Address);                  // bitovy ofset zacatku
         // priprav prvni byte :
         Tmp   = GetWord( p);                       // dvojice bytu
         Tmp >>= Shift;                             // eliminuj bitovy ofset
         Tmp <<= StartRow;                          // bitovy svisly posuv (dopln nula * StartRow)
         Mask  = 0xFF << StartRow;                  // spodni bity vynech
         // od druheho bytu nastav novy zdrojovy ofset :
         i = Address + 8 - StartRow;                // novy bitovy ofset
         p     = &PBitmap[ GetRow( i)];             // zacatek sloupce v bitmape
         Shift = GetBit( i);                        // bitovy ofset zacatku
         // uplne byty :
         for( i = 0; i < BHeight; i++){
            SaveByte( plane, c, r, Tmp, Mask);      // zapis pripraveny byte
            r++;                                    // dalsi radek
            Tmp   = GetWord( p);                    // dvojice bytu
            Tmp >>= Shift;                          // eliminuj bitovy ofset
            Mask  = 0xFF;                           // uplny byte
            p++;                                    // dalsi byte
         }
         // posledni neuplny byte :
         SaveByte( plane, c, r, Tmp & BMask, BMask & Mask); // jeden byte bude vzdy
         Address += height;                         // dalsi sloupec bitmapy
      } // for c
      PBitmap += (width * height + 7) / 8;          // velikost bitmapy v bytech
   } // for plane
} // BitCopy

//-----------------------------------------------------------------------------
// Ulozeni bytu
//-----------------------------------------------------------------------------

static void SaveByte( int plane, int x, int y, byte value, byte mask)
// Ulozi byte do bufferu
{
byte OldValue;
byte NewValue;

   OldValue = GBuffer.Buffer[ plane][y][x];
   switch( _Mode){
      case GMODE_REPLACE :
         NewValue = (OldValue & ~mask) | (value & mask);
         break;
      case GMODE_OR :
         NewValue = OldValue | (value & mask);
         break;
      case GMODE_XOR :
         NewValue = OldValue ^ (value & mask);
         break;
   }
   GBuffer.Buffer[ plane][ y][ x] = NewValue;
} // SaveByte
#endif // G_PLANES > 1
