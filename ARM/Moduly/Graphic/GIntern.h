//*****************************************************************************
//
//    GIntern.h     Graphic internals
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __GIntern_H__
   #define __GIntern_H__

//-----------------------------------------------------------------------------
// Graphic modules common
//-----------------------------------------------------------------------------

// common variables :

extern byte _Mode;
extern byte _Color;
extern word _Pattern;

void GBitBlt( int x, int y, int width, int height, const void *Bitmap);
// Copy bitmap by size <width,height> pixels at <x,y>

#endif
