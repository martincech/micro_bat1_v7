//*****************************************************************************
//
//   Spi.c     Programmed SPI interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "../../inc/Spi.h"
#include "../../inc/System.h"

// Porty:
#define SetSCK()  SPI_SET =  SPI_SCK
#define ClrSCK()  SPI_CLR =  SPI_SCK
#define SetSI()   SPI_SET =  SPI_MOSI
#define ClrSI()   SPI_CLR =  SPI_MOSI
#define GetSO()   (SPI_PIN & SPI_MISO)

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void SpiInit( void)
// Inicializace sbernice
{
   SPI_SET   = SPI_CS;                             // deselect
   ClrSCK();                                       // vychozi CLK
   SPI_DIR |= SPI_CS | SPI_MOSI | SPI_SCK;         // vystupy
   SPI_DIR &= ~SPI_MISO;                           // vstup
} // SpiInit

//-----------------------------------------------------------------------------
// Select
//-----------------------------------------------------------------------------

void SpiAttach( void)
// Nastaveni sbernice a select zarizeni
{
   SPI_CLR = SPI_CS;                   // CS select
   SysUDelay( 1);
} // SpiAttach

//-----------------------------------------------------------------------------
// Deselect
//-----------------------------------------------------------------------------

void SpiRelease( void)
// Uvolneni sbernice, deselect
{
   SysUDelay( 1);
   SPI_SET = SPI_CS;                   // CS deselect
} // SpiRelease

//-----------------------------------------------------------------------------
// Cteni dat
//-----------------------------------------------------------------------------

byte SpiReadByte( void)
// Precte byte
{
native Value;
native i = 8;

   Value = 0;
   do {
      SetSCK();
      SysUDelay( 1);
      Value <<= 1;
      if( GetSO()){
         Value |= 1;
      }
      ClrSCK();
      SysUDelay( 1);
   } while( --i);
   return( Value);
} // SpiReadByte

//-----------------------------------------------------------------------------
// Zapis dat
//-----------------------------------------------------------------------------

void SpiWriteByte( byte Value)
// Zapise byte
{
native i;

   i = 8;
   do {
      if( Value & 0x80) {
         SetSI();
      } else {
         ClrSI();
      }
      Value <<= 1;
      SysUDelay( 1);
      SetSCK();
      SysUDelay( 1);
      ClrSCK();
   } while( --i);
} // SpiWriteByte
