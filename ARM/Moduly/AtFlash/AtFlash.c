//*****************************************************************************
//
//   AtFlash.c      Flash memory AT45DBxxx, linear access
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#include "..\inc\AtFlash.h"
#include "..\inc\At45dbxx.h"  // Flash page access

#define INVALID_PAGE       0xFFFF         // neplatna stranka

// kontextova adresa pro zapis :
static word WritePage;
static word WriteOffset;

// kontextova adresa pro cteni :
static word ReadPage;
static word ReadOffset;

// Lokalni funkce :

static void SwapBuffer( word OldPage, word NewPage);
// Ulozi <OldPage>, nacte <NewPage>

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void FlashInit( void)
// Inicializace
{
   SpiInit();                          // vychozi stav SPI
   WritePage = INVALID_PAGE;
   ReadPage  = INVALID_PAGE;
} // FlashInit

//-----------------------------------------------------------------------------
// Zapis - start
//-----------------------------------------------------------------------------

void FlashWriteStart( dword Address)
// Start zapisu od <Address>
{
word NewPage;

   NewPage = MkPageAddress( Address);
   if( NewPage != WritePage){
      // zmena aktualni stranky
      SwapBuffer( WritePage, NewPage);
   } // else stejna stranka
   WritePage = NewPage;
   // zahajeni bloku zapisu :
   if( NewPage >= FLASH_PAGES){
      return;           // mimo rozsah = flush
   }
   WriteOffset = MkBufferAddress( Address);
   FlashWriteBufferStart( WriteOffset);
} // FlashWriteStart

//-----------------------------------------------------------------------------
// Zapis - data
//-----------------------------------------------------------------------------

void FlashWriteData( byte Data)
// Zapis na aktualni adresu a posun adresy
{
   FlashWriteBufferData( Data);        // zapis do bufferu
   WriteOffset++;                      // nasledujici ofset
   if( WriteOffset < FLASH_VIRTUAL_PAGE){
      return;                          // jen zapis do bufferu
   }
   // stranka naplnena
   FlashWriteBufferDone();             // ukonci blok
   WriteOffset = 0;                    // zacatek stranky
   SwapBuffer( WritePage, WritePage + 1);
   WritePage++;                        // nova stranka
   // zahajeni noveho bloku zapisu :
   FlashWriteBufferStart( 0);          // nova stranka zacina od 0
} // FlashWriteData

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte FlashReadByte( dword Address)
// Jednorazove cteni bytu
{
byte Value;

   FlashReadStart( Address);
   Value = FlashReadData();
   FlashReadDone();
   return( Value);
} // FlashReadByte

//-----------------------------------------------------------------------------
// Zapis bytu
//-----------------------------------------------------------------------------

void FlashWriteByte( dword Address, byte Value)
// Zapise byte vcetne Flush stranky
{
   FlashWriteStart( Address);
   FlashWriteData( Value);
   FlashWriteDone();
   FlashFlush();
} // FlashWriteByte

//-----------------------------------------------------------------------------
// Cteni - start
//-----------------------------------------------------------------------------

void FlashReadStart( dword Address)
// Start cteni od <Address>
{
   ReadPage   = MkPageAddress(   Address);
   ReadOffset = MkBufferAddress( Address);
   if( ReadPage == WritePage){
      // cteme z bufferu
      FlashBufferReadStart( ReadOffset);
   } else {
      // cteme z flash
      FlashBlockReadStart( ReadPage, ReadOffset);
   }
} // FlashReadStart

//-----------------------------------------------------------------------------
// Cteni - data
//-----------------------------------------------------------------------------

byte FlashReadData( void)
// Cteni z aktualni adresy a posun adresy
{
byte Data;

   Data = FlashCommonReadData();      // cteni stranky/bloku
   ReadOffset++;                      // nasledujici ofset
   if( ReadOffset < FLASH_VIRTUAL_PAGE){
      return( Data);                  // cteni ve strance
   }
   // dalsi stranka
   FlashCommonReadDone();             // ukonci cteni stranky/bloku
   ReadOffset = 0;                    // zacatek stranky
   ReadPage++;                        // nova stranka
   // zahajeni noveho bloku cteni :
   if( ReadPage == WritePage){
      // cteme z bufferu
      FlashBufferReadStart( ReadOffset);
   } else {
      // cteme z flash
      FlashBlockReadStart( ReadPage, ReadOffset);
   }
   return( Data);
} // FlashReadData

//-----------------------------------------------------------------------------
// Prepnuti bufferu
//-----------------------------------------------------------------------------

static void SwapBuffer( word OldPage, word NewPage)
// Ulozi <OldPage>, nacte <NewPage>
{
   if( OldPage < FLASH_PAGES){
      // v adresnim rozsahu, zapis
      FlashSaveBuffer( OldPage);
      FlashWaitForReady();
   }
   if( NewPage >= FLASH_PAGES){
      return;                          // mimo kapacitu
   }
   FlashLoadBuffer( NewPage);
   FlashWaitForReady();
} // SwapBuffer
