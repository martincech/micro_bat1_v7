//******************************************************************************
//
//   Weighing.h    Weighing utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Weighing_H__
   #define __Weighing_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

#ifndef __SdbDef_H__
   #include "SdbDef.h"       // sample flag only
#endif

typedef enum {
   WEIGHING_OK,              // weight in range
   WEIGHING_OVER,            // over weight
   WEIGHING_UNDER            // under weight
} TWeighingStatus;

//------------------------------------------------------------------------------
// Basic functions
//------------------------------------------------------------------------------

void WeighingInit( void);
// Initialize weighing

void WeighingSetGlobalParameters( void);
// Set/Update current file by global parameters

void WeighingCopyParameters( void);
// Unconditionaly Set/Update current file by global parameters

void WeighingStart( void);
// Power up start weighing

void WeighingStop( void);
// Power down stop weighing

void WeighingSuspend( void);
// Temporary stop weighing

void WeighingRelease( void);
// Start temporary stopped weighing

void WeighingManualMale( void);
// Save manual value button MALE

void WeighingManualFemale( void);
// Save manual value button FEMALE

void WeighingExecute( void);
// Execute automatic weighing

void WeighingDelete( void);
// Delete last value

void WeighingTara( void);
// Save tara

TRawWeight WeighingRaw( void);
// Get actual raw value

void WeighingRawTara( TRawWeight RawWeight);
// Set <RawWeight> as tara

//------------------------------------------------------------------------------
// Display data
//------------------------------------------------------------------------------

TWeight WeighingWeight( void);
// Returns actual weight

TWeighingStatus WeighingStatus( void);
// Returns actual weighing status

TWeight WeighingLastWeight( void);
// Returns last saved weight

TSampleFlag WeighingLastFlag( void);
// Returns last saved flag

int WeighingNumberOfBirds( void);
// Return number of birds (0 by disabled option)

//------------------------------------------------------------------------------
// Test
//------------------------------------------------------------------------------

void WeighingTestStart( void);
// Start simulation

void WeighingTestStop( void);
// Stop simulation

void WeighingTestClear( void);
// Clear simulation data

TYesNo WeighingTestActive( void);
// Returns YES on running test

#endif
