//*****************************************************************************
//
//    Kbd.c -  Keyboard services code template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../inc/System.h"             // "operacni system" - kvuli SysDelay()
#include "../inc/Cpu.h"                // WatchDog

word _kbd_counter = 0;                 // citac delky
word _kbd_status  = 0;                 // kontext
word _kbd_key     = K_RELEASED;        // kontextova klavesa (getch)

// horni 2 bity _kbd_counter jsou kontextova informace :
#define KBD_IDLE         0x0000        // cekame prvni dotek
#define KBD_FIRST_PRESS  0x0001        // ustaleni po prvnim doteku
#define KBD_WAIT_REPEAT  0x0002        // cekani na autorepeat
#define KBD_WAIT_RELEASE 0x0003        // zastaveny autorepeat, cekame na uvolneni

#define GetStatus()               _kbd_status
#define GetCounter()              _kbd_counter
#define MkStatus( Code, Timeout)  _kbd_status  = (Code);\
                                  _kbd_counter = ((Timeout) / TIMER_PERIOD)
#define MkIdle()                  _kbd_status  = KBD_IDLE

// Lokalni funkce :

static int ReadKey( void);
// Vrati kod prave stisknute klavesy

//-----------------------------------------------------------------------------
// Klavesa po zapnuti
//-----------------------------------------------------------------------------

int KbdPowerUpKey( void)
// Vrati klavesu, ktera je drzena po zapnuti nebo K_RELEASED
{
int key;

   key = ReadKey();
   SysDelay( 10);
   return( ReadKey());
} // KbdPowerUpKey

//-----------------------------------------------------------------------------
// Pusteni klavesy po zapnuti
//-----------------------------------------------------------------------------

void KbdPowerUpRelease( void)
// Ceka na pusteni klavesy, drzene po zapnuti
{
   while( ReadKey() != K_RELEASED){
      WatchDog();            // cekame na pusteni
   }
} // KbdPowerUpRelease

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

TYesNo kbhit( void)
// Vraci YES, bylo-li neco stisknuto
{
   _kbd_key = KbdGet();
   if( _kbd_key == K_IDLE){
      return( NO);            // nic neni stisknuto
   }
   return( YES);
} // kbhit

//-----------------------------------------------------------------------------
// Cekani na klavesu
//-----------------------------------------------------------------------------

int getch( void)
// Ceka na stisknuti klavesy, vrati ji
{
int key;

   if( _kbd_key == K_IDLE){
      // cekej na stisknuti
      while( !kbhit()){
         WatchDog();
      }
   }
   key = _kbd_key;                     // zapamatuj
   // cekej na pusteni :
   while( KbdGet() != K_RELEASED){
       WatchDog();
   }
   _kbd_key = K_IDLE;                  // uvolneni
   return( key);
} // getch

//-----------------------------------------------------------------------------
// Wait release
//-----------------------------------------------------------------------------

void KbdDisable( void)
// Zakaze generovani klaves, ceka na uvolneni
{
   MkStatus( KBD_WAIT_RELEASE, 0);     // novy stav automatu
} // KbdDisable

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

int KbdGet( void)
// Testuje stisknuti klavesy, vraci klavesu nebo
// K_IDLE neni-li stisknuto nic, volat periodicky
{
int key;

   key = ReadKey();
   switch( GetStatus()){
      case KBD_IDLE :
         if( key == K_RELEASED){
            return( K_IDLE);           // nic neni stisknuto
         }      
         MkStatus( KBD_FIRST_PRESS, KBD_DEBOUNCE);         // zaciname cekat na ustaleni
         return( K_IDLE);

      case KBD_FIRST_PRESS :
         if( GetCounter() != 0){
            return( K_IDLE);           // cekame po prvnim doteku
         }
         if( key == K_RELEASED){
            MkIdle();                  // cekame prvni dotek
            return( K_IDLE);           // odpadl po prvnim doteku
         }      
         MkStatus( KBD_WAIT_REPEAT, KBD_AUTOREPEAT_START); // zaciname cekat na autorepeat
         return( key);                 // platna klavesa

      case KBD_WAIT_REPEAT :
         if( key == K_RELEASED){
            MkIdle();                  // cekame prvni dotek
            return( K_RELEASED);       // uvolneni klavesy
         }      
         if( GetCounter() != 0){
            return( K_IDLE);           // cekame autorepeat
         }
         // zahajeni autorepeat
         MkStatus( KBD_WAIT_REPEAT, KBD_AUTOREPEAT_SPEED); // cekame na dalsi repeat
         return( key | K_REPEAT);      // platna klavesa

      case KBD_WAIT_RELEASE :
         if( key == K_RELEASED){
            MkIdle();                  // cekame prvni dotek
            return( K_RELEASED);       // uvolneni klavesy
         }
         return( K_IDLE);      
   }
   return( K_IDLE);                    // jen kompilator
} // KbdGet
